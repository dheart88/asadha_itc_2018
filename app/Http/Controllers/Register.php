<?php

namespace App\Http\Controllers;

use App\User;
use App\Glob as Glob;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Datetime;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use \stdClass;
<<<<<<< HEAD
// use \reflection;

//TODO: 
//1. add quota to each categores, ganpangnyaa buat sqatu funbgi s yang di opanggil dari tiap2 controller funtion katakan checkQuota("a") dimana kalau kuota telah terpenuhi maka serve halaman kuota teleh terpenuhi
//2. Refactor all views, jadikan semua menjadi View coponent masing2, misal emergencyContact, 
//3. Refactor all register* functions, make one core register function that serve recruiter and participant data
=======
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b


class Register extends Controller
{
<<<<<<< HEAD
	private $globReflection = null;

	public function __construct(){
		// parent::__construct();
		$this->globReflection = new \ReflectionClass('App\Glob');
	}

  public function index(){
      return view('frontend.registerIndex');
  }
  
  public function quickRegister(){
      return view('frontend.registerQuick');
  }
  
  public function quickPhoto(Request $r){
      $dt = new stdClass();
      if ($r->uuid)
      {
          $dt = DB::connection('mysql')->table('peserta')
              ->where('barcode', '=', $r->uuid)
              ->where('active', '=', 1)
              ->first();
      }

      return view('frontend.photoQuick')->with('data',$dt);;
  }

  private function registerHandler($r, $formType)
  {
    //check available spot based on quota - number of participants of certain 'form'
    if ($this->getCountOfGroup($formType) > $this->globReflection->getConstant('quota'.$formType))
      dd("Mohon maaf, jumlah kuota kami untuk peserta ".$formType." telah penuh, mohon mencoba form lainnya");


    $dt = new stdClass();
      if ($r->uuid)
      {
          $dt2 = DB::connection('mysql')->table('recruiter')
              ->where('uuid', '=', $r->uuid)
              ->first();
          if ($dt2)
          {
              $dt->uuid_recruiter     = $dt2->UUID;
              $dt->nama_recruiter     = $dt2->NAMA;
              $dt->cp_recruiter       = $dt2->CP;
              $dt->cp_recruiter2      = $dt2->CP2;
              $dt->hp_recruiter       = $dt2->HP;
              $dt->hp_recruiter2      = $dt2->HP2;
              $dt->email_recruiter    = $dt2->EMAIL;
          }
      }
      return view('frontend.register'.$formType)->with('data',$dt);
  }

  //As wrapper for registration form A, register Handler would actually do the task
  public function registerA(Request $r){
    return $this->registerHandler($r, 'A');
  }

  //As wrapper for registration form B, register Handler would actually do the task
  public function registerB(Request $r){
    return $this->registerHandler($r, 'B');
  }

  //As wrapper for registration form C, register Handler would actually do the task
  public function registerC(Request $r){
    return $this->registerHandler($r, 'C');
  }

  //As wrapper for registration form D, register Handler would actually do the task
  public function registerD(Request $r){
    return $this->registerHandler($r, 'D');
  }

  //As wrapper for registration form E, register Handler would actually do the task
  public function registerE(Request $r){
    return $this->registerHandler($r, 'E');
  }

  //As wrapper for registration form F, register Handler would actually do the task
  public function registerF(Request $r){
    return $this->registerHandler($r, 'F');
  }

  public function edit(Request $r)
  {
    if ($r->uuid)
    {
        $dt = DB::connection('mysql')->table('peserta')
            ->where('barcode', '=', $r->uuid)
            //->where('active', '=', 1)
            ->first();

        if ($dt)
        {
            //peserta sudah di hapus
            if ($dt->active == 0)
                dd("Peserta ini berstatus tidak aktif, kemungkinan karena membatalkan ikut acara.");
            
            $db_time = strtotime($dt->created_datetime) + Glob::timelimitedit + ($dt->add_days * 24 * 3600);
            $curtime = time();
            
            Log::useFiles(storage_path().'/logs/editRequest.log');
            Log::info("valid request ".json_encode($r->input())." redirect to : ".$dt->form);
            
            //min( $limittime, $db_time)
            $dt->can_edit   = (($curtime < $db_time) && ($dt->ctredit < 10)) ? 1 : 0;
            $dt->group_edit = false;
            
            return view('frontend.edit'.$dt->form)->with('data',$dt);
        }
        else 
            dd("Data ini tidak ditemukan, mungkin juga telah di hapus admin karena kembar. Hub Assaji 081269110181 via Whats app jika perlu verifikasi data secara manual.");
=======
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function index()
    {
        //return "Ada";
        return view('frontend.registerIndex');
    }

    public function registerA()
    {
        return view('frontend.registerA');
    }

    public function registerB()
    {
        return view('frontend.registerB');
    }

    public function registerC(Request $r)
    {
        $dt = new stdClass();
        
        if ($r->uuid)
        {
            $dt2 = DB::connection('mysql')->table('recruiter')
                ->where('uuid', '=', $r->uuid)
                ->first();
            if ($dt2)
            {
                $dt->uuid_recruiter     = $dt2->UUID;
                $dt->nama_recruiter     = $dt2->NAMA;
                $dt->cp_recruiter       = $dt2->CP;
                $dt->cp_recruiter2      = $dt2->CP2;
                $dt->hp_recruiter       = $dt2->HP;
                $dt->hp_recruiter2      = $dt2->HP2;
                $dt->email_recruiter    = $dt2->EMAIL;
            }
        }

        return view('frontend.registerC')->with('data',$dt);
    }

    public function registerD(Request $r)
    {
        $dt = new stdClass();
        $dt->getDCount  = $this->getCountOfD();
        $dt->getDQueue  = $this->getCountOfDQueue();
        $dt->kuota      = Glob::quotaD;

        if ($r->uuid)
        {
            $dt2 = DB::connection('mysql')->table('recruiter')
                ->where('uuid', '=', $r->uuid)
                ->first();
            if ($dt2)
            {
                $dt->uuid_recruiter     = $dt2->UUID;
                $dt->nama_recruiter     = $dt2->NAMA;
                $dt->cp_recruiter       = $dt2->CP;
                $dt->cp_recruiter2      = $dt2->CP2;
                $dt->hp_recruiter       = $dt2->HP;
                $dt->hp_recruiter2      = $dt2->HP2;
                $dt->email_recruiter    = $dt2->EMAIL;
            }
        }
          
        return view('frontend.registerD')->with('data',$dt);
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
    }
    else
        dd("Id yang dimasukkan tidak valid");
  }

<<<<<<< HEAD
  private function indokesql($str)
  {
    if ($str) {
        try {
            $date = Datetime::createFromFormat('d-m-Y', $str);            
        } catch (Exception $e) {
            return -1;
        }
        if ($date === false)
            return -1;
        return $date->format('Y-m-d');
=======
    public function edit(Request $r)
    {
        
        if ($r->uuid)
        {
            $dt = DB::connection('mysql')->table('peserta')
                ->where('barcode', '=', $r->uuid)
                ->first();


            if ($dt)
            {
                $db_time = strtotime($dt->datetime) + Glob::timelimitedit + ($dt->add_days * 24 * 3600);
                $curtime = time();
                $limittime = strtotime(Glob::registrationCloseDate);

                Log::useFiles(storage_path().'/logs/editRequest.log');
                Log::info("valid request ".json_encode($r->input())." redirect to : ".$dt->form);
                
                $dt->can_edit   = ($curtime < min($limittime, $db_time) && ($dt->ctredit < 3)) ? 1 : 0;
                $dt->group_edit = false;
                switch($dt->form)
                {
                    case 'A':
                        return view('frontend.editA')->with('data',$dt);
                    break;
                    case 'B':
                        return view('frontend.editB')->with('data',$dt);
                    break;
                    case 'C':
                        if ($dt->ID_REKRUT != null)
                        {
                            $dt2 = DB::connection('mysql')->table('recruiter')
                                ->where('ID_REKRUT', '=', $dt->ID_REKRUT)
                                ->first();
                            if ($dt2)
                            {
                                $dt->uuid_recruiter     = $dt2->UUID;
                                $dt->nama_recruiter     = $dt2->NAMA;
                                $dt->cp_recruiter       = $dt2->CP;
                                $dt->cp_recruiter2      = $dt2->CP2;
                                $dt->hp_recruiter       = $dt2->HP;
                                $dt->hp_recruiter2      = $dt2->HP2;
                                $dt->email_recruiter    = $dt2->EMAIL;
                            }
                        }
                        return view('frontend.editC')->with('data',$dt);
                    break;
                    case 'D':
                        if ($dt->ID_REKRUT != null)
                        {
                            $dt2 = DB::connection('mysql')->table('recruiter')->select('NAMA','HP','EMAIL','FORM')
                                    ->where('ID_REKRUT', '=', $dt->ID_REKRUT)
                                    ->first();
                            if ($dt2)
                            {
                                $dt->uuid_recruiter     = $dt2->UUID;
                                $dt->nama_recruiter     = $dt2->NAMA;
                                $dt->cp_recruiter       = $dt2->CP;
                                $dt->cp_recruiter2      = $dt2->CP2;
                                $dt->hp_recruiter       = $dt2->HP;
                                $dt->hp_recruiter2      = $dt2->HP2;
                                $dt->email_recruiter    = $dt2->EMAIL;
                            }
                        }
                        // $dt->getDCount  = $this->getCountOfD();
                        // $dt->kuota      = Glob::quotaD;
                        return view('frontend.editD')->with('data',$dt);
                    break;
                    default:
                        
                    break;
                }
            }
            else 
                dd("Data ini tidak ditemukan, mungkin juga telah di hapus admin karena kembar. Hub Assaji 081269110181 via Whats app jika perlu verifikasi data secara manual.");
        }
        else
            dd("Id yang dimasukkan tidak valid");
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
    }
    return -1;
  }
  
  public function cetakAll(Request $r)
  {
    $data = DB::connection('mysql')->
                table('peserta')->
                select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->
                where('photo_validated','=',1)->
                where('active','=',1)->
                get();
    
    return view('backend.preview')->with('data',$data);
  }
  
  public function cetak(Request $r)
  {
    $data = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->where('photo_validated','=',1)->where('photo_printed','=',0)->where('active','=',1)->get();
    
    return view('backend.preview')->with('data',$data);
  }

  public function cetakSTAB(Request $r)
  {
    $data = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->where('id_rekrut','=','27')->where('photo_validated','=',1)->where('active','=',1)->get();
      
    return view('backend.preview')->with('data',$data);
  }

  public function cetakSTI(Request $r)
  {
    $data = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->where('id_rekrut','=','30')->where('photo_validated','=',1)->where('active','=',1)->get();
      
    return view('backend.preview')->with('data',$data);
  }
  
  public function cetakSBY(Request $r)
  {
      $data = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->where('id_rekrut','=','38')->where('photo_validated','=',1)->where('active','=',1)->get();
      
      return view('backend.preview')->with('data',$data);
  }

  //buat display semua peserta dengan cepat.. hacky module
  public function quickAdmin(Request $r)
  {
      $dt = DB::connection('mysql')->
                  table('peserta')->
                  select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                  where('active','=',1)->
                  get();
      $dt->PESERTA = $dt;
      $dt->ADD_VALID = true;
      
      return view('frontend.quickAdmin')->with('data',$dt);
  }

  //buat display semua peserta dengan cepat.. hacky module
  public function quickBendahara(Request $r)
  {
      $dt = DB::connection('mysql')->
                  table('peserta')->
                  select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                  where('active','=',1)->
                  get();
      $dt->PESERTA = $dt;
      $dt->ADD_VALID = true;
      
      return view('frontend.quickAdmin')->with('data',$dt);
  }

  public function listA(Request $r)
  {
      $dt = DB::connection('mysql')->table('recruiter')
              ->where('id_rekrut', '=', 0)
              ->first();
      $dt -> FORM         = "A";
      $dt -> ADD_VALID    = true;
      $dt2 = DB::connection('mysql')->
                  table('peserta')->
                  select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                  where('active','=',1)->
                  where('FORM','=','A')->
                  get();
      $dt->PESERTA = $dt2;
      return view('frontend.registerGroup')->with('data',$dt);
  }
  
  public function listB(Request $r)
  {
		$dt = DB::connection('mysql')->table('recruiter')
						->where('id_rekrut', '=', 0)
						->first();
		$dt -> FORM         = "B";
		$dt -> ADD_VALID    = true;
		$dt2 = DB::connection('mysql')->
								table('peserta')->
								select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
								where('active','=',1)->
								where('FORM','=','B')->
								get();
		$dt->PESERTA = $dt2;
		return view('frontend.registerGroup')->with('data',$dt);
  }
  
  public function listC(Request $r)
  {
		$dt = DB::connection('mysql')->table('recruiter')
						->where('id_rekrut', '=', 0)
						->first();
		$dt -> FORM         = "C";
		$dt -> ADD_VALID    = true;
		$dt2 = DB::connection('mysql')->
								table('peserta')->
								select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
								where('active','=',1)->
								where('FORM','=','C')->
								get();
		$dt->PESERTA = $dt2;
		return view('frontend.registerGroup')->with('data',$dt);
  }
  
  public function listD(Request $r)
  {
      $dt = DB::connection('mysql')->table('recruiter')
              ->where('id_rekrut', '=', 0)
              ->first();
      $dt -> FORM         = "D";
      $dt -> ADD_VALID    = true;
      $dt2 = DB::connection('mysql')->
                  table('peserta')->
                  select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                  where('active','=',1)->
                  where('FORM','=','D')->
                  get();
      $dt->PESERTA = $dt2;
      return view('frontend.registerGroup')->with('data',$dt);
  }
  
  public function groupRegistration(Request $r)
  {
      Log::useFiles(storage_path().'/logs/registerGroup.log');
      Log::info(json_encode($r->input()));

      //dd($r->type);

      if ($r->uuid)
      {
          $dt = DB::connection('mysql')->table('recruiter')
              ->where('uuid', '=', $r->uuid)
              ->first();
              
          if ($dt->ID_REKRUT != "ASALHAPU") {
              $dt2 = DB::connection('mysql')->
                      table('peserta')->
                      select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                      where('id_rekrut', '=', $dt->ID_REKRUT)->
                      where('active','=',1)->
                      get();
          } else {
              $dt2 = DB::connection('mysql')->
                      table('peserta')->
                      select('ID_PESERTA','BARCODE','FORM','NAMA','PHONE','EMAIL')->
                      where('active','=',1)->
                      get();
          }
          $dt->PESERTA = $dt2;
          
          $addvalid = true;
          $db_time = strtotime($dt->VALID_UNTIL);
          $curtime = time();
          $reason  = "";
          
          if ($dt->VALID_UNTIL == NULL)
              $addvalid = true;
          else if ($curtime > $db_time)
              {$addvalid = false; $reason = "Batas waktu memasukkan data telah lewat pada ".$db_time." mohon hubungi panitia.\n";}
          
          if (($dt->QUOTA > 0) && (sizeof($dt->PESERTA) >= $dt->QUOTA))
              {$addvalid = false; $reason = "Jumlah peserta yang bisa anda daftarkan telah melebih batas ".$dt->QUOTA." mohon hubungi panitia";}
          
          //+ add
          if ($r->action == 'add')
          {
              if ($addvalid == true) {
                if (($r->type == 'A') && (!(strpos($dt->FORM,'A') === false)))
                  return $this->registerA($r);
                else if (($r->type == 'B') && (!(strpos($dt->FORM,'B') === false)))
                  return $this->registerB($r);
                else if (($r->type == 'C') && (!(strpos($dt->FORM,'C') === false)))
                  return $this->registerC($r);
                else if (($r->type == 'D') && (!(strpos($dt->FORM,'D') === false)))
                  return $this->registerD($r);
                else
          dd("form salah atau anda tidak punya akses menambahkan peserta dengan form ini");
              }
              else
                  dd($reason);
          }
          
          else if (($r->action == 'edit') && ($r->p))
          {
            $dt2 = DB::connection('mysql')->table('peserta')
                      ->where('BARCODE', '=', $r->p)
                      ->first();

            $dt2->group_edit         = true;
              $dt2->can_edit           = 1;
              $dt2->uuid_recruiter     = $dt->UUID;
              $dt2->nama_recruiter     = $dt->NAMA;
              $dt2->cp_recruiter       = $dt->CP;
              $dt2->cp_recruiter2      = $dt->CP2;
              $dt2->hp_recruiter       = $dt->HP;
              $dt2->hp_recruiter2      = $dt->HP2;
              $dt2->email_recruiter    = $dt->EMAIL;
              return view('frontend.edit'.$dt2->form)->with('data',$dt2);
          }

          //= view
          else
          {
              $dt->ADD_VALID = $addvalid;
              return view('frontend.registerGroup')->with('data',$dt);
          }
          
      }
      else
          dd("Id yang dimasukkan tidak valid");
  }

  public function groupEdit(Request $r)
  {
      
    if (Input::has('uuid') && Input::has('id_peserta'))
    {
<<<<<<< HEAD
      $dt = DB::connection('mysql')->table('recruiter')
          ->where('uuid', '=', $r->uuid)
          ->first();


      if ($dt)
      {
        $dt2 = DB::connection('mysql')->table('peserta')
                ->where('ID_PESERTA', '=', $r->id_peserta)
                ->first();
        if ($dt2)
        {
          $dt2->group_edit         = true;
          $dt2->can_edit           = 1;
          $dt2->uuid_recruiter     = $dt->UUID;
          $dt2->nama_recruiter     = $dt->NAMA;
          $dt2->cp_recruiter       = $dt->CP;
          $dt2->cp_recruiter2      = $dt->CP2;
          $dt2->hp_recruiter       = $dt->HP;
          $dt2->hp_recruiter2      = $dt->HP2;
          $dt2->email_recruiter    = $dt->EMAIL;
              
          switch ($dt2->form) {
              case 'C':
                  return view('frontend.editC')->with('data',$dt2);
                  break;
              
              default:
                  return view('frontend.editD')->with('data',$dt2);
                  break;
          }
                
        }
        else 
          dd("Invalid Request. code 3");    
      }
      else 
        dd("Invalid Request.  code 2");
    }
    else
      dd("Invalid Request.  code 1");
  }

  public function checkRegistration(Request $r)
  {
    $data = $r->json()->all();

    $retval = new stdClass();
      $retval->data = [];
    if (!empty($data))
    {
      $uuid = '';
      if (array_key_exists('uuid', $data))
        $uuid = $data['uuid'];
      
      if ($uuid!='')
      {
        $retval->data = DB::connection('mysql')->table('peserta')->
                            where('barcode', $uuid)->
                            orWhere('ktp', $uuid)->
                            orwhere('passport', $uuid)->
                            select('id_peserta', 'parent_id', 'barcode', 'nama', 'form', 'created_datetime', 'active')->get();

        $tempData = $retval->data;
        for ($i = 0; $i < count($tempData); $i++)
=======
        if ($str) {
            try {
                $date = Datetime::createFromFormat('d-m-Y', $str);            
            } catch (Exception $e) {
                return -1;
            }
            if ($date === false)
                return -1;
            return $date->format('Y-m-d');
        }
        return -1;
    }

    public function groupRegistration(Request $r)
    {
        Log::useFiles(storage_path().'/logs/registerGroup.log');
        Log::info(json_encode($r->input()));

        if ($r->uuid)
        {
            $dt = DB::connection('mysql')->table('recruiter')
                ->where('uuid', '=', $r->uuid)
                ->first();
            
            //dd($dt);
            //passing semua data pesertanya
            $dt2 = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','NAMA','PHONE','EMAIL')->where('id_rekrut', '=', $dt->ID_REKRUT)->get();

            $dt->PESERTA = $dt2;

            return view('frontend.registerGroup')->with('data',$dt);
        }
        else
            dd("Id yang dimasukkan tidak valid");
    }

    public function groupEdit(Request $r)
    {
        
        if (Input::has('uuid') && Input::has('id_peserta'))
        {
            $dt = DB::connection('mysql')->table('recruiter')
                ->where('uuid', '=', $r->uuid)
                ->first();


            if ($dt)
            {
                // Log::useFiles(storage_path().'/logs/editGroupRequest.log');
                // Log::info("valid request ".json_encode($r->input())." redirect to : ".$dt->form);

                $dt2 = DB::connection('mysql')->table('peserta')
                        ->where('ID_PESERTA', '=', $r->id_peserta)
                        ->first();
                if ($dt2)
                {
                    $dt2->group_edit         = true;
                    $dt2->can_edit           = 1;
                    $dt2->uuid_recruiter     = $dt->UUID;
                    $dt2->nama_recruiter     = $dt->NAMA;
                    $dt2->cp_recruiter       = $dt->CP;
                    $dt2->cp_recruiter2      = $dt->CP2;
                    $dt2->hp_recruiter       = $dt->HP;
                    $dt2->hp_recruiter2      = $dt->HP2;
                    $dt2->email_recruiter    = $dt->EMAIL;
                        
                    switch ($dt2->form) {
                        case 'C':
                            return view('frontend.editC')->with('data',$dt2);
                            break;
                        
                        default:
                            return view('frontend.editD')->with('data',$dt2);
                            break;
                    }
                        
                }
                else 
                    dd("Invalid Request. code 3");    
            }
            else 
                dd("Invalid Request.  code 2");
        }
        else
            dd("Invalid Request.  code 1");
    }

    public function postRegistration(Request $r)
    {
        $foto_nama          = '';
        $foto_ukuran        = 0;
        $foto_tipe          = '';
        $foto               = $r->file('gambar');
        $adafoto            = $foto && $foto->isValid();
        if ($adafoto)
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
        {
            $thisData = $tempData[$i]; 
            $thisData->children = DB::connection('mysql')->table('peserta')->
                                      where('parent_id', $thisData->id_peserta)->
                                      select('id_peserta', 'parent_id', 'barcode', 'nama', 'form', 'created_datetime', 'active')->get();
        }
      }
    }
    return json_encode($retval);
  }

<<<<<<< HEAD
  public function checkUniqueID(Request $r)
  {
      Log::useFiles(storage_path().'/logs/register.log');
      $data = $r->json()->all();

      $retval = new stdClass();
      $retval->result = 0;
      $retval->message = 'Data yang dikirim kosong / Empty payload';
      if (!empty($data))
      {
        $ktp              = '';//$data['ktp'];
        $passport         = '';//$data['passport'];
        $uuid             = '';

        if (array_key_exists('ktp', $data))
          $ktp      = $data['ktp'];
        if (array_key_exists('passport', $data))
          $passport = $data['passport'];
        if (array_key_exists('uuid', $data))
          $uuid     = $data['uuid'];
          
        if (($ktp=='') && ($passport==''))
=======
        Log::useFiles(storage_path().'/logs/register.log');
        Log::info(json_encode($r->input()).$foto_nama.' '.$foto_tipe.' '.$foto_ukuran);

        $time_arr = null;
        if (is_string($r->input('arrival_time')))
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
        {
          $retval->result = 0;
          $retval->message = 'Minimal salah satu data, KTP atau Passport harus diisi / At least passport or ktp must be filled.';
          return json_encode($retval);
        }

        if ($ktp!='')
        {
          $q = DB::connection('mysql')->
                  table('peserta')->
                  where(function($query) use ($ktp)
                  {
                    $query->where('barcode', $ktp);
                    $query->orWhere('ktp', $ktp);
                  });
          
          if ($uuid!='')
          {
            $q->where('barcode','!=',$uuid);
            $q->where('ktp', $uuid);
          }
            
                        
          $count = $q -> count();

        //   \DB::connection('mysql')->listen(function($sql, $bindings, $time) {
        //     Log::info($sql);
        //     Log::info($bindings);
        //     Log::info($time);
        //   });
          Log::info($q->toSql());

          if ($count > 0)
          {
            $retval->result = 0;
            $retval->message = 'Nomor KTP ini telah terdata sebelumnya, mohon periksa kembali registrasi anda di halaman utama registrasi. Mungkin anda telah terdaftar sebelumnya / This KTP has been registered before.';
            return json_encode($retval);
          }
        }

<<<<<<< HEAD
        if ($passport!='')
        {
          $q = DB::connection('mysql')->
                  table('peserta')->
                  where(function($query) use ($passport)
                  {
                    $query->where('barcode', $passport);
                    $query->orWhere('passport', $passport);
                  });
          
          if ($uuid!='')
          {
            $q->where('barcode','!=',$uuid);
            $q->where('passport', $uuid);
          }

          $count = $q -> count();

        //   \DB::connection('mysql')->listen(function($sql, $bindings, $time) {
        //     Log::info($sql);
        //     Log::info($bindings);
        //     Log::info($time);
        //   });
          Log::info($q->toSql());

          if ($count > 0)
          {
              $retval->result = 0;
              $retval->message = 'Nomor Passport ini telah terdata sebelumnya, mohon periksa kembali registrasi anda di halaman utama registrasi. Mungkin anda telah terdaftar sebelumnya. Kemungkinan lainnya adalah nomor passportnya kebetulan sama untuk  negara berbeda, silakan tambahkan angka 0 didepan / This Passport has been registered or there might be a case where there are other participants that has same passport number from issued from different country. please recheck your registration at main registration page or add 0 before your passport number.';
              return json_encode($retval);
          }
=======
        
        if (!$r->input('nama')) //dianggap nyasar
        {
            return "Request tidak valid, silakan ke halaman utama http://register.asalhapuja.or.id atau back ke halaman sebelumnya";
        }


        //validasi server side
        $tgl_lahir = $this->indokesql($r->input('tgl_lahir'));
        if ($tgl_lahir == -1)
            return "tgl lahir tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";

        $arrival_date = null;
        if (Input::has('arrival_date'))
        {
            $arrival_date = $this->indokesql($r->input('arrival_date'));
            if ($arrival_date == -1)
                return "tgl kedatangan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
        }

        $departure_date = null;
        if (Input::has('departure_date'))
        {
            $departure_date = $this->indokesql($r->input('departure_date'));
            if ($departure_date  == -1)
                return "tgl keberangkatan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
        }

        $kebangsaan = 'Indonesia';
        if (Input::has('kebangsaan'))
            $kebangsaan = $r->input('kebangsaan');

        $nama           = strtoupper($r->input('nama'));
        $organisasi     = ($r->input('organisasi'));
        $jabatan        = ($r->input('jabatan'));
        $alamat         = ($r->input('alamat'));
        $ktp            = ($r->input('ktp'));
        $form           = strtoupper($r->input('form'));


        //ensure unique UUID
        $hasUUID = false;
        for($i =0;$i<100;$i++)
        {
            $uuid = $this->uniqidReal(12);
            $et = DB::connection('mysql')->table('peserta')
                ->where('barcode', '=', $uuid)
                ->count();
            if ($et == 0)
            {
                $hasUUID = true;
                break;
            }
        }

        if ($hasUUID == false)
        {
            dd('internal error : no uuid');
        }

        $id_rekrut = null;
        if (Input::has('uuid_recruiter'))
        {
            $id_rekrut = DB::connection('mysql')->table('recruiter')
                ->where('uuid', '=', $r->uuid_recruiter)->limit(1)->pluck('ID_REKRUT')->first();
            //dd($id_rekrut);
        }


        $flagOverQuotaD = false;
        //flag over quota
        if (($form == 'D') && ($this->getCountOfD()>=Glob::quotaD) && ($id_rekrut == null))
            $flagOverQuotaD = true;

        // insertGetId
        DB::connection('mysql')->beginTransaction();
        $id = DB::connection('mysql')->table('peserta')->insertGetId([
            //'ID_PESERTA'    => $r->input('ID_PESERTA'), 
            'form'                  => $form,
            'nama'                  => $nama, 
            'umat'                  => $r->input('umat'),
            'organisasi'            => $organisasi,
            'jabatan'               => $jabatan,
            'tgl_lahir'             => $tgl_lahir,
            'kebangsaan'            => $kebangsaan,
            'ktp'                   => $ktp,
            'alamat'                => $alamat,
            'passport'              => $r->input('passport'),
            'meal'                  => $r->input('meal'),
            'foto_nama'             => $foto_nama,
            'foto_ukuran'           => $foto_ukuran,
            'foto_tipe'             => $foto_tipe,
            'jk'                    => $r->input('jk'),
            'phone'                 => $r->input('phone'),
            'email'                 => $r->input('email'),
            'barcode'               => $uuid,
            'program'               => $r->input('program'),
            'paket'                 => $r->input('paket'),
            'arrival_transport'     => $r->input('arrival_transport'),
            'arrival_from'          => $r->input('arrival_from'),
            'arrival_date'          => $arrival_date,
            'arrival_time'          => $time_arr,
            'departure_transport'   => $r->input('departure_transport'),
            'departure_to'          => $r->input('departure_to'),
            'departure_date'        => $departure_date,
            'departure_time'        => $time_dept,
            'emergency_cp'          => $r->input('emergency_cp'),
            'emergency_phone'       => $r->input('emergency_phone'),
            'emergency_email'       => $r->input('emergency_email'),
            'photo_validated'       => 0,
            'id_rekrut'             => $id_rekrut,
            'waiting_list'          => $flagOverQuotaD ? 1:0,
        ]);
        DB::connection('mysql')->commit();

        if ($id) //pastinya valid!
        {
            if ($adafoto)
                $this->savePhoto($foto,$id);

            if ($r->input('email'))
            {
                $curtime = time() + Glob::timelimitedit;
                $expiredDate = date('d-m-Y H:i:s',  $curtime);
                $lin = "http://register.asalhapuja.or.id/edit?uuid=".$uuid;
                $msg = "Terima kasih ".$r->input('nama').", telah melakukan pendaftaran untuk acara Asadha ITC . \n";
                if ($flagOverQuotaD)
                    $msg .= "Status pendaftaran anda sedang dalam antrian waiting list, kami akan menghubungi anda kembali sebelum 15 Juni 2018\n"; 
                $msg .= "Detail anda telah kami terima, untuk mengubah detail data anda dapat dilakukan dari / Your registration entry for Asadha ITC has been processed, to change entry detail please follow this link : \n\n";
                $msg .= $lin;
                $msg .= "\n\nLink tersebut expired hingga ".$expiredDate." UTC time / Link above will be active until ".$expiredDate;
                $msg = wordwrap($msg,150);
                $headers = "From: donotreply@asalhapuja.or.id";
                
                mail($r->input('email'), "Pendaftaran/Registration for Asadha 2562 ITC 2018", $msg, $headers);
                
                /*
                DB::connection('mysql')->table('email_queue')->insert([
                    'mail_from'     => 'donotreply@asalhapuja.or.id', 
                    'mail_to'       => $r->input('email'),
                    'mail_title'    => htmlspecialchars("Pendaftaran/Registration for Asadha 2562 ITC 2018",ENT_QUOTES),
                    'mail_content'  => htmlspecialchars($msg,ENT_QUOTES),
                ]);
                */


            }

            $phone = $r->input('phone');
            if ($phone)
            {
                if ( ((substr($phone,0,1) == '0') || (substr($phone,0,3) == '+62')) && (strlen($phone)>8) )
                {
                    $message = 'Data pendaftaran anda pada Asadha ITC 2018 dapat dilihat di http://register.asalhapuja.or.id/edit?uuid='.$uuid;
                    if ($flagOverQuotaD)
                        $message = 'Data anda dalam waiting list Asadha ITC 2018 dan dapat dilihat di http://register.asalhapuja.or.id/edit?uuid='.$uuid;
                    DB::connection('mysql')->table('sms_queue')->insert([
                        'ID_PESERTA'    => $id, 
                        'HP'            => $phone,
                        'MESSAGE'       => $message,
                    ]);
                }

            }

            if ($id_rekrut)
                return redirect()->route('registerGroup', array('uuid' => $r->uuid_recruiter));

            return redirect()->route('success', array('uuid' => $uuid));
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
        }

<<<<<<< HEAD
        //sepertinya tidak ada masalah
        $retval->result   = 1;
        $retval->message  = 'Valid';
        return json_encode($retval); 
      }
      return json_encode($retval);
  }

  public function postRegistration(Request $r)
  {
      Log::useFiles(storage_path().'/logs/register.log');

    $form           = strtoupper($r->input('form'));
      //check available spot based on quota - number of participants of certain 'form'
    if ($this->getCountOfGroup($form) > $this->globReflection->getConstant('quota'.$form))
      dd("Mohon maaf, jumlah kuota kami untuk peserta ".$form." telah penuh, mohon mencoba form lainnya");
=======
    //fungsi buat bantu kirim lagi email.. ckckck
    public function sendEmailChangeTrigger($uuid)
    {
    //     $uuid = $r->uuid;
        if ($uuid)
        {
            $data = DB::connection('mysql')->table('peserta')->where('barcode', $uuid)->get()->first();
            $limit = $this->getEditLimit($uuid);

            $lin = "http://register.asalhapuja.or.id/edit?uuid=".$data->barcode;
            $msg = $data->nama.", anda telah melakukan pengubahan alamat email ke email ini / You just changed you email address to this address. Untuk melihat data anda dapat dilakukan melalui link yang sama dengan sebelumnya yaitu pada / To View your entry please follow this link : \n\n";
            $msg .= $lin;
            $msg .= "\n\nLink tersebut expired hingga ".$limit->expiredDate." UTC time / Link above will be active until ".$limit->expiredDate;
            $msg = wordwrap($msg,150);
            $headers = "From: donotreply@asalhapuja.or.id";
            
            //$data->email = 'assajitjahjadi@gmail.com';
            mail($data->email, "Perubahan data Asadha 2562 ITC 2018", $msg, $headers);
        }
    }


    //fungsi buat mengirim email ke semua buat bantu mengecek data mereka sendiri
    public function sendFormValidationInvitationtoAll()
    {
        $data = DB::connection('mysql')->table('peserta')->get();
        foreach ($data as $dt) {
            if ($dt->email)
            {
                $lin = "http://register.asalhapuja.or.id/edit?uuid=".$dt->barcode;
                $title = "Validasi Data Peserta Asadha 2562 ITC 2018";
                $msg = "Kepada ".$dt->nama.",\n\n"; 
                $msg .= "Terima kasih kembali untuk telah mendaftar untuk acara Asadha ITC, sehubungan dengan banyaknya data peserta yang tidak valid terutama bagian foto maka dengan ini kami menghimbau kembali ";
                $msg .= "peserta untuk mengecek datanya masing-masing. Format foto yang diharapkan adalah close-up yang mana wajah-nya nampak jelas dan bukan KTP/\n";
                $msg .= "Thank you for enrolling Asadha ITC 2018, we want to inform you to re-check your data because we have received many invalid data from participants particularly photos taken from ID cards.\n\n ";
                $msg .= "Untuk melihat data kembali dapat melalui tautan berikut / to view your entry please follow this link : \n\n";
                $msg .= $lin;  
                $msg .= "\n\nAtas Perhatiannya kami ucapkan terima kasih / Thank you for your attention";
                $msg = wordwrap($msg,150);
                $from = 'donotreply@asalhapuja.or.id';
                $to = $dt->email;
                // $headers = "From: donotreply@asalhapuja.or.id";
                // echo $msg;
                //$dt->email
                //mail('assajitjahjadi@gmail.com', 'a', 'b', 'c');
                //echo $dt->email."</br>";

                DB::connection('mysql')->table('email_queue')->insert([
                    'mail_from'     => $from, 
                    'mail_to'       => $title,
                    'mail_title'    => $to,
                    'mail_content'  => $msg,
                ]);
            }
        }    
    }

    public function postEdit(Request $r)
    {
        $foto_nama          = '';
        $foto_ukuran        = 0;
        $foto_tipe          = '';
        $foto               = $r->file('gambar');
        $adafoto            = $foto && $foto->isValid();
        if ($adafoto)
        {
            $foto_nama      = $foto->getClientOriginalName();
            $foto_ukuran    = $foto->getClientSize();
            $foto_tipe      = $foto->getClientMimeType();
        }

        Log::useFiles(storage_path().'/logs/edit.log');
        Log::info(json_encode($r->input()));

        $time_arr = null;
        if (is_string($r->input('arrival_time')))
        {
            $temp = explode(':', $r->input('arrival_time'));
            if (sizeof($temp) == 2)
                $time_arr = $r->input('arrival_time');
                // $time_arr = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
        }
        
        $time_dept = null;
        if (is_string($r->input('departure_time')))
        {
            $temp = explode(':', $r->input('departure_time'));
            if (sizeof($temp)==2)
                $time_dept = $r->input('departure_time');
                //$time_dept = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
        }

        if (!$r->input('nama'))
        {   
            return "Request tidak valid, silakan ke halaman utama http://register.asalhapuja.or.id atau back ke halaman sebelumnya";
        }

        $dt = DB::connection('mysql')->table('peserta')->where('barcode', $r->uuid)->select('ID_PESERTA','ctredit','datetime','add_days')->first();
        
        $db_time = strtotime($dt->datetime) + Glob::timelimitedit + ($dt->add_days * 24 * 3600);
        $curtime = time();
        $limittime = strtotime(Glob::registrationCloseDate);

        if ( (!Input::has('uuid_recruiter') && (($curtime < min($limittime, $db_time) && ($dt->ctredit < 3)) == false) ))
            dd("Batas waktu ataupun kesempatan mengubah data telah lewat");

        $ctredit = $dt->ctredit;
        if (!Input::has('uuid_recruiter'))
            $ctredit += 1;

        //validasi server side
        $tgl_lahir = $this->indokesql($r->input('tgl_lahir'));
        if ($tgl_lahir == -1)
            return "tgl lahir tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";

        $arrival_date = null;
        if (Input::has('arrival_date'))
        {
            $arrival_date = $this->indokesql($r->input('arrival_date'));
            if ($arrival_date == -1)
                return "tgl kedatangan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
        }

        $departure_date = null;
        if (Input::has('departure_date'))
        {
            $departure_date = $this->indokesql($r->input('departure_date'));
            if ($departure_date  == -1)
                return "tgl keberangkatan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
        }
        $nama           = strtoupper($r->input('nama'));

        $res = DB::connection('mysql')->table('peserta')
            ->where('barcode', $r->uuid)->limit(1)->update([
            'nama'                  => $nama, 
            'umat'                  => $r->input('umat'),
            'organisasi'            => $r->input('organisasi'),
            'jabatan'               => $r->input('jabatan'),
            'tgl_lahir'             => $tgl_lahir,
            'kebangsaan'            => $r->input('kebangsaan'),
            'ktp'                   => $r->input('ktp'),
            'alamat'                => $r->input('alamat'),
            'passport'              => $r->input('passport'),
            'meal'                  => $r->input('meal'),
            'jk'                    => $r->input('jk'),
            'phone'                 => $r->input('phone'),
            'email'                 => $r->input('email'),
            // 'paket'                 => $r->input('paket'),
            'arrival_transport'     => $r->input('arrival_transport'),
            'arrival_from'          => $r->input('arrival_from'),
            'arrival_date'          => $arrival_date,
            'arrival_time'          => $time_arr,
            'departure_transport'   => $r->input('departure_transport'),
            'departure_to'          => $r->input('departure_to'),
            'departure_date'        => $departure_date,
            'departure_time'        => $time_dept,
            'emergency_cp'          => $r->input('emergency_cp'),
            'emergency_phone'       => $r->input('emergency_phone'),
            'emergency_email'       => $r->input('emergency_email'),
            'ctredit'               => $ctredit,
        ]);
        
        if ($adafoto)
        {
            $this->savePhoto($foto, $dt->ID_PESERTA);
            $res = DB::connection('mysql')->table('peserta')->where('barcode', $r->uuid)->limit(1)->update([
                'photo_validated'   => 0,
            ]);
        }
        // if ($res)
        // {
        //     return redirect()->route('successEdit', array('uuid' => $r->uuid));
        // }
        // else
        //     return back()->withInput();

        if (Input::has('uuid_recruiter'))
        {
            $cnt = DB::connection('mysql')->table('recruiter')->where('uuid', '=', $r->uuid_recruiter)->count();
            if ($cnt>0)
                 return redirect()->route('registerGroup', array('uuid' => $r->uuid_recruiter));
        }


        return redirect()->route('successEdit', array('uuid' => $r->uuid));
    }

    private function savePhoto($foto,$id)
    {
        $rawPath = 'ImageRaw/';
        if (!file_exists(public_path($rawPath))) {
            mkdir(public_path($rawPath), 0775, true);
        }

        // $imagePath = 'Image/';
        // if (!file_exists(public_path($imagePath))) {
        //     mkdir(public_path($imagePath), 0775, true);
        // }

        $thumbnailPath = 'ImageThumb/';
        if (!file_exists(public_path($thumbnailPath))) {
            mkdir(public_path($thumbnailPath), 0775, true);
        }

        //raw
        $imageName  = $id.'.'.$foto->getClientOriginalExtension();
        $res        = $foto->move(public_path($rawPath), $imageName);

        //create backup
        // $tempImage  = Image::make($res)->orientate();
        // $maxSize    = 2000; 
        // if ($tempImage->width()>$tempImage->height())
        //     $tempImage->widen($maxSize, function ($constraint) {$constraint->upsize();});
        // else
        //     $tempImage->heighten($maxSize, function ($constraint) {$constraint->upsize();});
        // $tempImage->save(public_path($imagePath).$id.'.jpg', 100);

        //create thumb
        $tempImage2  = Image::make($res)->orientate();
        $maxSize2    = 400; 
        if ($tempImage2->width()>$tempImage2->height())
            $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
        else
            $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});
        $tempImage2->save(public_path($thumbnailPath).$id.'.jpg', 60); 
    }

    public function resizeImage(Request $r)
    {

        $rawPath = 'ImageRaw/';
        if (!file_exists(public_path($rawPath))) {
            mkdir(public_path($rawPath), 0775, true);
        }

        $imagePath = 'Image/';
        if (!file_exists(public_path($imagePath))) {
            mkdir(public_path($imagePath), 0775, true);
        }

        $thumbnailPath = 'ImageThumb/';
        if (!file_exists(public_path($thumbnailPath))) {
            mkdir(public_path($thumbnailPath), 0775, true);
        }

        //.{jpg,png,gif}
        $files = glob(public_path($rawPath).'/*', GLOB_BRACE);
        foreach($files as $file) {
            echo $file."</br>";
            $path_parts = pathinfo($file);

            //create backup
            $tempImage  = Image::make($file)->orientate();
            $maxSize    = 2000; 
            if ($tempImage->width()>$tempImage->height())
                $tempImage->widen($maxSize, function ($constraint) {$constraint->upsize();});
            else
                $tempImage->heighten($maxSize, function ($constraint) {$constraint->upsize();});
            $tempImage->save(public_path($imagePath).$path_parts['filename'].'.jpg', 100);

            //create thumb
            $tempImage2  = Image::make($file)->orientate();
            $maxSize2    = 400; 
            if ($tempImage2->width()>$tempImage2->height())
                $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
            else
                $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});  
            $tempImage2->save(public_path($thumbnailPath).$path_parts['filename'].'.jpg', 60);
        }
    }

    public function success(Request $r)
    {
        $uuid = $r->input('uuid');
        $data = $this->getEditLimit($uuid);
        // dd($data);
        if ($data)
            return view('frontend.success')->with('data',$data);
        else
            dd("uuid ".$uuid." is not valid.");
    }

    public function successEdit(Request $r)
    {
        $uuid = $r->input('uuid');
        $data = $this->getEditLimit($uuid);
        if ($data)
            return view('frontend.successEdit')->with('data',$data);
        else
            dd("uuid ".$uuid." is not valid.");
    }

    private function getEditLimit($uuid)
    {
        $data = DB::connection('mysql')->table('peserta')->where('barcode', $uuid)->select('datetime','ctredit','add_days')->first();
        if ($data && $data->datetime)
        {
            $db_time = strtotime($data->datetime) + Glob::timelimitedit + ($data->add_days * 24 * 3600);
            $limittime = strtotime(Glob::registrationCloseDate);
            $limittime = min($limittime, $db_time);
            $expiredDate = date('d-m-Y H:i:s',  $limittime);
            return (object)array("uuid" => $uuid, "ctredit" => $data->ctredit, "expiredDate" => $expiredDate);
        }
        else
            return 0;
    }

    public function test()
    {
        // Mail::raw('content', function($message) {
        //     $message->to('assajitjahjadi@gmail.com');
        //     $message->subject('E-Mail Example');

        //     $message->from($address, $name = null);
        //     $message->sender($address, $name = null);
        //     $message->to($address, $name = null);
        // });

        // Mail::send('email.test', ['name' => "nama", 'isi' => "Isi"],  function($message) {
        //     $message->from('noreply@asalhapuja.or.id', 'asalhapuja');
        //     $message->to('assajitjahjadi@gmail.com');
        //     $message->subject('E-Mail Example');
        // });
        
        echo $this->getCountOfD();

        //$this->sendEmailChangeTrigger('1a3ce822678d');
        // echo "nothing here";
    }

    public function sendQueuedEmail()
    {
        Log::useFiles(storage_path().'/logs/mailSent.log');
        

        $dt = DB::connection('mysql')->table('email_queue')->whereNull('mail_sent')->first();
        
        if ($dt)
        {
            dd($dt);
            $headers = "From: ".$dt->mail_from;
            if (mail($dt->mail_to, htmlspecialchars($dt->mail_title,ENT_QUOTES), htmlspecialchars($dt->mail_content,ENT_QUOTES), $headers))
            {
                DB::connection('mysql')->table('email_queue')
                        ->where('mail_id','=', $dt->mail_id)
                        ->limit(1)
                        ->update(['mail_sent' => DB::raw('NOW()')]);

                Log::info("Mail sent : ".$dt->mail_to." ".time());
            }

            
        }
        return $dt;

    }
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b

      $foto_nama          = '';
      $foto_ukuran        = 0;
      $foto_tipe          = '';
      $foto               = $r->file('gambar');
      $adafoto            = $foto && $foto->isValid();
      if ($adafoto)
      {
          $foto_nama      = $foto->getClientOriginalName();
          $foto_ukuran    = $foto->getClientSize();
          $foto_tipe      = $foto->getClientMimeType();
      }

      
      Log::info(json_encode($r->input()).$foto_nama.' '.$foto_tipe.' '.$foto_ukuran);

      $time_arr = null;
      if (is_string($r->input('arrival_time')))
      {
          $temp = explode(':', $r->input('arrival_time'));
          if (sizeof($temp) == 2)
              $time_arr = $r->input('arrival_time');
              // $time_arr = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
      }
      
      $time_dept = null;
      if (is_string($r->input('departure_time')))
      {
          $temp = explode(':', $r->input('departure_time'));
          if (sizeof($temp)==2)
              $time_dept = $r->input('departure_time');
              //$time_dept = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
      }

      if (!$r->input('nama')) //dianggap nyasar
      {
          return "Request tidak valid, silakan ke halaman utama http://register.asalhapuja.or.id atau back ke halaman sebelumnya";
      }

      //validasi server side
      $tgl_lahir = NULL;
      if ($r->input('tgl_lahir'))
      {
          $tgl_lahir = $this->indokesql($r->input('tgl_lahir'));
          if ($tgl_lahir == -1)
              return "tgl lahir tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }
      
      $arrival_date = null;
      if (Input::has('arrival_date'))
      {
          $arrival_date = $this->indokesql($r->input('arrival_date'));
          if ($arrival_date == -1)
              return "tgl kedatangan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }

      $departure_date = null;
      if (Input::has('departure_date'))
      {
          $departure_date = $this->indokesql($r->input('departure_date'));
          if ($departure_date  == -1)
              return "tgl keberangkatan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }

      $kebangsaan = 'Indonesia';
      if (Input::has('kebangsaan'))
          $kebangsaan = $r->input('kebangsaan');

      $nama           = strtoupper($r->input('nama'));
      $organisasi     = ($r->input('organisasi'));
      $jabatan        = ($r->input('jabatan'));
      $alamat         = ($r->input('alamat'));
      $ktp            = ($r->input('ktp'));
      $passport       = ($r->input('passport'));
      
      //ensure unique UUID
      // $hasUUID = false;
      // for($i =0;$i<100;$i++)
      // {
      //     $uuid = $this->randomString(5);
      //     $et = DB::connection('mysql')->table('peserta')
      //         ->where('barcode', '=', $uuid)
      //         ->count();
      //     if ($et == 0)
      //     {
      //         $hasUUID = true;
      //         break;
      //     }
      // }

      // if ($hasUUID == false)
      // {
      //     dd('internal error : no uuid');
      // }

      $ktp            = ($r->input('ktp'));
      $passport       = ($r->input('passport'));

      $uuid = '';
      if ($ktp)
          $uuid = $ktp;
      else if ($passport)
          $uuid = $passport;


      if ($uuid == '')
      {
          dd('internal error : no uuid, please input KTP or Passport');
      }
      else
      {
          $et = DB::connection('mysql')->table('peserta')
              ->where('barcode', '=', $uuid)
              ->count();
          if ($et > 0)
          {
              dd('internal error : UUID duplicated, please check KTP or Passport, it should be unique');       
          }
      }


      $id_rekrut = null;
      if (Input::has('uuid_recruiter'))
      {
          if (($r->uuid_recruiter) != "ASALHAPU") //quick by pass
              $id_rekrut = DB::connection('mysql')->table('recruiter')
                  ->where('uuid', '=', $r->uuid_recruiter)->limit(1)->pluck('ID_REKRUT')->first();
          //dd($id_rekrut);
      }

      $printed_name = null;
      if (Input::has('printed_name'))
          $printed_name = strtoupper($r->input('printed_name'));
          
      // insertGetId
      DB::connection('mysql')->beginTransaction();
      $id = DB::connection('mysql')->table('peserta')->insertGetId([
          'form'                  => $form,
          'nama'                  => $nama, 
          'printed_name'          => $printed_name, 
          'umat'                  => $r->input('umat'),
          'organisasi'            => $organisasi,
          'jabatan'               => $jabatan,
          'tgl_lahir'             => $tgl_lahir,
          'kebangsaan'            => $kebangsaan,
          'ktp'                   => $ktp,
          'alamat'                => $alamat,
          'passport'              => $passport,
          'meal'                  => $r->input('meal'),
          'foto_nama'             => $foto_nama,
          'foto_ukuran'           => $foto_ukuran,
          'foto_tipe'             => $foto_tipe,
          'photo_upload_datetime' => DB::raw('NOW()'),
          'jk'                    => $r->input('jk'),
          'phone'                 => $r->input('phone'),
          'email'                 => $r->input('email'),
          'barcode'               => $uuid,
          'program'               => $r->input('program'),
          'paket'                 => $r->input('paket'),
          'arrival_transport'     => $r->input('arrival_transport'),
          'arrival_from'          => $r->input('arrival_from'),
          'arrival_date'          => $arrival_date,
          'arrival_time'          => $time_arr,
          'departure_transport'   => $r->input('departure_transport'),
          'departure_to'          => $r->input('departure_to'),
          'departure_date'        => $departure_date,
          'departure_time'        => $time_dept,
          'emergency_cp'          => $r->input('emergency_cp'),
          'emergency_phone'       => $r->input('emergency_phone'),
          'emergency_email'       => $r->input('emergency_email'),
          'photo_validated'       => 0,
          'id_rekrut'             => $id_rekrut,
          'waiting_list'          => 0,
      ]);
      DB::connection('mysql')->commit();

      if ($id) //pastinya valid!
      {
          if ($adafoto)
              $this->savePhoto($foto,$id);

          /*
          if ($r->input('email'))
          {
              $curtime = time() + Glob::timelimitedit;
              $expiredDate = date('d-m-Y H:i:s',  $curtime);
              $lin = "http://register.asalhapuja.or.id/edit?uuid=".$uuid;
              $msg = "Terima kasih ".$r->input('nama').", telah melakukan pendaftaran untuk acara Asadha ITC . \n";
              if ($flagOverQuotaD)
                  $msg .= "Status pendaftaran anda sedang dalam antrian waiting list, kami akan menghubungi anda kembali sebelum 15 Juni 2018\n"; 
              $msg .= "Detail anda telah kami terima, untuk mengubah detail data anda dapat dilakukan dari / Your registration entry for Asadha ITC has been processed, to change entry detail please follow this link : \n\n";
              $msg .= $lin;
              $msg .= "\n\nLink tersebut expired hingga ".$expiredDate." UTC time / Link above will be active until ".$expiredDate;
              $msg = wordwrap($msg,150);
              $headers = "From: donotreply@asalhapuja.or.id";
              
              mail($r->input('email'), "Pendaftaran/Registration for Asadha 2562 ITC 2018", $msg, $headers);
          }
          */
          
          /*
          DB::connection('mysql')->table('email_queue')->insert([
              'mail_from'     => 'donotreply@asalhapuja.or.id', 
              'mail_to'       => $r->input('email'),
              'mail_title'    => htmlspecialchars("Pendaftaran/Registration for Asadha 2562 ITC 2018",ENT_QUOTES),
              'mail_content'  => htmlspecialchars($msg,ENT_QUOTES),
          ]);
          */
  
          /*
          $phone = $r->input('phone');
          if ($phone)
          {
              if ( ((substr($phone,0,1) == '0') || (substr($phone,0,3) == '+62')) && (strlen($phone)>8) )
              {
                  $message = 'Data pendaftaran anda pada Asadha ITC 2018 dapat dilihat di http://register.asalhapuja.or.id/edit?uuid='.$uuid;
                  if ($flagOverQuotaD)
                      $message = 'Data anda dalam waiting list Asadha ITC 2018 dan dapat dilihat di http://register.asalhapuja.or.id/edit?uuid='.$uuid;
                  DB::connection('mysql')->table('sms_queue')->insert([
                      'ID_PESERTA'    => $id, 
                      'HP'            => $phone,
                      'MESSAGE'       => $message,
                  ]);
              }
          }
          */

          if ($id_rekrut)
              return redirect()->route('registerGroup', array('uuid' => $r->uuid_recruiter));

          return redirect()->route('success', array('uuid' => $uuid));
      }
      else
          return back()->withInput(Input::all());
  }
  
  public function postQuickRegistration(Request $r)
  {
      $foto_nama          = '';
      $foto_ukuran        = 0;
      $foto_tipe          = '';
      $foto               = $r->file('gambar');
      $adafoto            = $foto && $foto->isValid();
      if ($adafoto)
      {
          $foto_nama      = $foto->getClientOriginalName();
          $foto_ukuran    = $foto->getClientSize();
          $foto_tipe      = $foto->getClientMimeType();
      }

      Log::useFiles(storage_path().'/logs/register.log');
      Log::info(json_encode($r->input()).$foto_nama.' '.$foto_tipe.' '.$foto_ukuran);

      // if (!$r->input('nama')) //dianggap nyasar
      // {
      //     return "Request tidak valid, silakan ke halaman utama http://register.asalhapuja.or.id atau back ke halaman sebelumnya";
      // }
      
      $form           = strtoupper($r->input('form'));
      $alamat         = ($r->input('alamat'));

      //ensure unique UUID
      $hasUUID = false;
      for($i =0;$i<100;$i++)
      {
          $uuid = $this->randomString(5);
          $et = DB::connection('mysql')->table('peserta')
              ->where('barcode', '=', $uuid)
              ->count();
          if ($et == 0)
          {
              $hasUUID = true;
              break;
          }
      }
      
      $printed_name = null;
      if (Input::has('printed_name'))
          $printed_name = strtoupper($r->input('printed_name'));
          
      $nama           = $printed_name;
      
      // insertGetId
      DB::connection('mysql')->beginTransaction();
      $id = DB::connection('mysql')->table('peserta')->insertGetId([
          'form'                  => $form,
          'nama'                  => $nama, 
          'printed_name'          => $printed_name,
          'alamat'                => $alamat,
          'foto_nama'             => $foto_nama,
          'foto_ukuran'           => $foto_ukuran,
          'foto_tipe'             => $foto_tipe,
          'photo_upload_datetime' => DB::raw('NOW()'),
          'barcode'               => $uuid,
          'quick_flag'            => 1,
          'photo_validated'       => 1,
      ]);
      DB::connection('mysql')->commit();

      if ($adafoto)
          $this->savePhotoAutoValidate($foto,$id);
          
      return redirect()->route('success', array('uuid' => $uuid));
  }
  
  

  //fungsi buat bantu kirim lagi email.. ckckck
  public function sendEmailChangeTrigger($uuid)
  {
      //$uuid = $r->uuid;
      if ($uuid)
      {
          $data = DB::connection('mysql')->table('peserta')->where('barcode', $uuid)->get()->first();
          $limit = $this->getEditLimit($uuid);

          $lin = "http://register.asalhapuja.or.id/edit?uuid=".$data->barcode;
          $msg = $data->nama.", anda telah melakukan pengubahan alamat email ke email ini / You just changed you email address to this address. Untuk melihat data anda dapat dilakukan melalui link yang sama dengan sebelumnya yaitu pada / To View your entry please follow this link : \n\n";
          $msg .= $lin;
          $msg .= "\n\nLink tersebut expired hingga ".$limit->expiredDate." UTC time / Link above will be active until ".$limit->expiredDate;
          $msg = wordwrap($msg,150);
          $headers = "From: donotreply@asalhapuja.or.id";
          
          //$data->email = 'assajitjahjadi@gmail.com';
          mail($data->email, "Perubahan data Asadha 2562 ITC 2018", $msg, $headers);
      }
  }


  //fungsi buat mengirim email ke semua buat bantu mengecek data mereka sendiri
  public function sendFormValidationInvitationtoAll()
  {
      $data = DB::connection('mysql')->table('peserta')->get();
      foreach ($data as $dt) {
          if ($dt->email)
          {
              $lin = "http://register.asalhapuja.or.id/edit?uuid=".$dt->barcode;
              $title = "Validasi Data Peserta Asadha 2562 ITC 2018";
              $msg = "Kepada ".$dt->nama.",\n\n"; 
              $msg .= "Terima kasih kembali untuk telah mendaftar untuk acara Asadha ITC, sehubungan dengan banyaknya data peserta yang tidak valid terutama bagian foto maka dengan ini kami menghimbau kembali ";
              $msg .= "peserta untuk mengecek datanya masing-masing. Format foto yang diharapkan adalah close-up yang mana wajah-nya nampak jelas dan bukan KTP/\n";
              $msg .= "Thank you for enrolling Asadha ITC 2018, we want to inform you to re-check your data because we have received many invalid data from participants particularly photos taken from ID cards.\n\n ";
              $msg .= "Untuk melihat data kembali dapat melalui tautan berikut / to view your entry please follow this link : \n\n";
              $msg .= $lin;  
              $msg .= "\n\nAtas Perhatiannya kami ucapkan terima kasih / Thank you for your attention";
              $msg = wordwrap($msg,150);
              $from = 'donotreply@asalhapuja.or.id';
              $to = $dt->email;
              // $headers = "From: donotreply@asalhapuja.or.id";
              // echo $msg;
              //$dt->email
              //mail('assajitjahjadi@gmail.com', 'a', 'b', 'c');
              //echo $dt->email."</br>";

              DB::connection('mysql')->table('email_queue')->insert([
                  'mail_from'     => $from, 
                  'mail_to'       => $title,
                  'mail_title'    => $to,
                  'mail_content'  => $msg,
              ]);
          }
      }    
  }
  
  public function postQuickPhoto(Request $r)
  {
      $foto_nama          = '';
      $foto_ukuran        = 0;
      $foto_tipe          = '';
      $foto               = $r->file('gambar');
      $adafoto            = $foto && $foto->isValid();
      if ($adafoto)
      {
          $foto_nama      = $foto->getClientOriginalName();
          $foto_ukuran    = $foto->getClientSize();
          $foto_tipe      = $foto->getClientMimeType();
      }

      Log::useFiles(storage_path().'/logs/edit.log');
      Log::info(json_encode($r->input()));

      $nama = strtoupper($r->input('nama'));
      
      $printed_name = null;
      if (Input::has('printed_name'))
          $printed_name = strtoupper($r->input('printed_name'));

      $res = DB::connection('mysql')->table('peserta')
          ->where('barcode', $r->uuid)->limit(1)->update([
          'nama'                  => $nama, 
          'printed_name'          => $printed_name, 
      ]);
      
      $dt = DB::connection('mysql')->table('peserta')->
          where('barcode', $r->uuid)->
          select('ID_PESERTA')->first();
      
      if ($adafoto)
      {
          $this->savePhotoAutoValidate($foto, $dt->ID_PESERTA);
          $res = DB::connection('mysql')->table('peserta')->where('barcode', $r->uuid)->limit(1)->update([
              'photo_validated'   => 1,
              'photo_upload_datetime' => DB::raw('NOW()'),
          ]);
      }
      // if ($res)
      // {
      //     return redirect()->route('successEdit', array('uuid' => $r->uuid));
      // }
      // else
      //     return back()->withInput();

      if (Input::has('uuid_recruiter'))
      {
          $cnt = DB::connection('mysql')->table('recruiter')->where('uuid', '=', $r->uuid_recruiter)->count();
          if ($cnt>0)
                return redirect()->route('registerGroup', array('uuid' => $r->uuid_recruiter));
      }


      return redirect()->route('successEdit', array('uuid' => $r->uuid));
  }


  public function postEdit(Request $r)
  {
      $foto_nama          = '';
      $foto_ukuran        = 0;
      $foto_tipe          = '';

      $foto               = $r->file('gambar');
      $adafoto            = $foto && $foto->isValid();
      if ($adafoto)
      {
          $foto_nama      = $foto->getClientOriginalName();
          $foto_ukuran    = $foto->getClientSize();
          $foto_tipe      = $foto->getClientMimeType();
      }
            
      Log::useFiles(storage_path().'/logs/edit.log');
      Log::info(json_encode($r->input()));

      $time_arr = null;
      if (is_string($r->input('arrival_time')))
      {
          $temp = explode(':', $r->input('arrival_time'));
          if (sizeof($temp) == 2)
              $time_arr = $r->input('arrival_time');
              // $time_arr = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
      }
      
      $time_dept = null;
      if (is_string($r->input('departure_time')))
      {
          $temp = explode(':', $r->input('departure_time'));
          if (sizeof($temp)==2)
              $time_dept = $r->input('departure_time');
              //$time_dept = date('Y-d-m H:i:s', mktime($temp[0],$temp[1],0));
      }

      if (!$r->input('nama'))
      {   
          return "Request tidak valid, silakan ke halaman utama http://register.asalhapuja.or.id atau back ke halaman sebelumnya";
      }

      $dt = DB::connection('mysql')->table('peserta')->where('barcode', $r->uuid)->select('ID_PESERTA','ctredit','created_datetime','add_days')->first();
      
      $db_time = strtotime($dt->created_datetime) + Glob::timelimitedit + ($dt->add_days * 24 * 3600);
      $curtime = time();
      $limittime = strtotime(Glob::registrationCloseDate);

      if ( (!Input::has('uuid_recruiter') && (($curtime < min($limittime, $db_time) && ($dt->ctredit < 10)) == false) ))
          dd("Batas waktu ataupun kesempatan mengubah data telah lewat");

      $ctredit = $dt->ctredit;
      if (!Input::has('uuid_recruiter'))
          $ctredit += 1;

      //validasi server side
      $tgl_lahir = NULL;
      if ($r->input('tgl_lahir'))
      {
          $tgl_lahir = $this->indokesql($r->input('tgl_lahir'));
          if ($tgl_lahir == -1)
              return "tgl lahir tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }

      $arrival_date = null;
      if (Input::has('arrival_date'))
      {
          $arrival_date = $this->indokesql($r->input('arrival_date'));
          if ($arrival_date == -1)
              return "tgl kedatangan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }

      $departure_date = null;
      if (Input::has('departure_date'))
      {
          $departure_date = $this->indokesql($r->input('departure_date'));
          if ($departure_date  == -1)
              return "tgl keberangkatan tidak valid, gunakan format dd-mm-yyyy semisal 31-12-1950 silahkan kembali dengan menekan tombol back, sebaiknya pergunakan chrome/firefox/safari agar validasi tanggal berjalan dengan baik / Invalid date";
      }
      $nama = strtoupper($r->input('nama'));
      
      $printed_name = null;
      if (Input::has('printed_name'))
          $printed_name = strtoupper($r->input('printed_name'));

      $ktp            = ($r->input('ktp'));
      $passport       = ($r->input('passport'));

      $uuid = '';
      if ($ktp)
          $uuid = $ktp;
      else if ($passport)
          $uuid = $passport;

      if ($uuid == '')
      {
          dd('internal error : no uuid, please input KTP or Passport');
      }

      $res = DB::connection('mysql')->table('peserta')
          ->where('barcode', $r->uuid)->limit(1)->update([
          'nama'                  => $nama, 
          'printed_name'          => $printed_name, 
          'umat'                  => $r->input('umat'),
          'organisasi'            => $r->input('organisasi'),
          'jabatan'               => $r->input('jabatan'),
          'tgl_lahir'             => $tgl_lahir,
          'kebangsaan'            => $r->input('kebangsaan'),
          'ktp'                   => $ktp,
          'alamat'                => $r->input('alamat'),
          'passport'              => $passport,
          'meal'                  => $r->input('meal'),
          'jk'                    => $r->input('jk'),
          'phone'                 => $r->input('phone'),
          'email'                 => $r->input('email'),
          'updated_datetime'      => DB::raw('NOW()'),
          'barcode'               => $uuid,
          // 'paket'                 => $r->input('paket'),
          'arrival_transport'     => $r->input('arrival_transport'),
          'arrival_from'          => $r->input('arrival_from'),
          'arrival_date'          => $arrival_date,
          'arrival_time'          => $time_arr,
          'departure_transport'   => $r->input('departure_transport'),
          'departure_to'          => $r->input('departure_to'),
          'departure_date'        => $departure_date,
          'departure_time'        => $time_dept,
          'emergency_cp'          => $r->input('emergency_cp'),
          'emergency_phone'       => $r->input('emergency_phone'),
          'emergency_email'       => $r->input('emergency_email'),
          'ctredit'               => $ctredit,
      ]);
      
      if ($adafoto)
      {
          $this->savePhoto($foto, $dt->ID_PESERTA);
          $res = DB::connection('mysql')->table('peserta')->where('barcode', $r->uuid)->limit(1)->update([
              'photo_validated'   => 0,
              'photo_upload_datetime' => DB::raw('NOW()'),
          ]);
      }

      $receipt = $r->file('receipt');
      if ($receipt && $receipt->isValid())
      {
        $this->saveReceipt($receipt, $dt->ID_PESERTA);
      }

      // if ($res)
      // {
      //     return redirect()->route('successEdit', array('uuid' => $r->uuid));
      // }
      // else
      //     return back()->withInput();

      if (Input::has('uuid_recruiter'))
      {
          $cnt = DB::connection('mysql')->table('recruiter')->where('uuid', '=', $r->uuid_recruiter)->count();
          if ($cnt>0)
                return redirect()->route('registerGroup', array('uuid' => $r->uuid_recruiter));
      }


      return redirect()->route('successEdit', array('uuid' => $uuid));
  }

  private function saveReceipt($foto,$id)
  {
    $path = 'receipt/';
    if (!file_exists(public_path($path))) {
        mkdir(public_path($path), 0775, true);
    }

<<<<<<< HEAD
    $tempImage2  = Image::make($foto)->orientate();
    $maxSize2    = 800; 
    if ($tempImage2->width()>$tempImage2->height())
        $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
    else
        $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});
    $tempImage2->save(public_path($path).$id.'.jpg', 60); 
  }

  private function savePhoto($foto,$id)
  {
      $rawPath = 'ImageRaw/';
      if (!file_exists(public_path($rawPath))) {
          mkdir(public_path($rawPath), 0775, true);
      }

      $thumbnailPath = 'ImageThumb/';
      if (!file_exists(public_path($thumbnailPath))) {
          mkdir(public_path($thumbnailPath), 0775, true);
      }

      //raw
      $imageName  = $id.'.'.$foto->getClientOriginalExtension();
      $res        = $foto->move(public_path($rawPath), $imageName);

      //create thumb
      $tempImage2  = Image::make($res)->orientate();
      $maxSize2    = 200; 
      if ($tempImage2->width()>$tempImage2->height())
          $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
      else
          $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});
      $tempImage2->save(public_path($thumbnailPath).$id.'.jpg', 60); 
  }
  
  private function savePhotoAutoValidate($foto,$id)
  {
      $rawPath        = 'ImageRaw/';
      if (!file_exists(public_path($rawPath))) {
          mkdir(public_path($rawPath), 0775, true);
      }
      
      $validatedPath  = 'validated/';
      if (!file_exists(public_path($validatedPath))) {
          mkdir(public_path($validatedPath), 0775, true);
      }

      $thumbnailPath  = 'ImageThumb/';
      if (!file_exists(public_path($thumbnailPath))) {
          mkdir(public_path($thumbnailPath), 0775, true);
      }

      //raw
      $imageName  = $id.'.'.$foto->getClientOriginalExtension();
      $res        = $foto->move(public_path($rawPath), $imageName);

      //create validated
      $tempImage2  = Image::make($res)->orientate();
      $maxSize2    = 600; 
      if ($tempImage2->width()>$tempImage2->height())
          $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
      else
          $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});
      $tempImage2->save(public_path($validatedPath).$id.'.jpg', 60);
      
      //create thumb
      $tempImage2  = Image::make($res)->orientate();
      $maxSize2    = 400; 
      if ($tempImage2->width()>$tempImage2->height())
          $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
      else
          $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});
      $tempImage2->save(public_path($thumbnailPath).$id.'.jpg', 60); 
  }

  // public function resizeImage(Request $r)
  // {

  //     $rawPath = 'ImageRaw/';
  //     if (!file_exists(public_path($rawPath))) {
  //         mkdir(public_path($rawPath), 0775, true);
  //     }

  //     $imagePath = 'Image/';
  //     if (!file_exists(public_path($imagePath))) {
  //         mkdir(public_path($imagePath), 0775, true);
  //     }

  //     $thumbnailPath = 'ImageThumb/';
  //     if (!file_exists(public_path($thumbnailPath))) {
  //         mkdir(public_path($thumbnailPath), 0775, true);
  //     }

  //     //.{jpg,png,gif}
  //     $files = glob(public_path($rawPath).'/*', GLOB_BRACE);
  //     foreach($files as $file) {
  //         echo $file."</br>";
  //         $path_parts = pathinfo($file);

  //         //create backup
  //         $tempImage  = Image::make($file)->orientate();
  //         $maxSize    = 2000; 
  //         if ($tempImage->width()>$tempImage->height())
  //             $tempImage->widen($maxSize, function ($constraint) {$constraint->upsize();});
  //         else
  //             $tempImage->heighten($maxSize, function ($constraint) {$constraint->upsize();});
  //         $tempImage->save(public_path($imagePath).$path_parts['filename'].'.jpg', 100);

  //         //create thumb
  //         $tempImage2  = Image::make($file)->orientate();
  //         $maxSize2    = 400; 
  //         if ($tempImage2->width()>$tempImage2->height())
  //             $tempImage2->widen($maxSize2, function ($constraint) {$constraint->upsize();});
  //         else
  //             $tempImage2->heighten($maxSize2, function ($constraint) {$constraint->upsize();});  
  //         $tempImage2->save(public_path($thumbnailPath).$path_parts['filename'].'.jpg', 60);
  //     }
  // }

  public function success(Request $r)
  {
      $uuid = $r->input('uuid');
      $data = $this->getEditLimit($uuid);
      // dd($data);
      if ($data)
          return view('frontend.success')->with('data',$data);
      else
          dd("uuid ".$uuid." is not valid.");
  }

  public function successEdit(Request $r)
  {
      $uuid = $r->input('uuid');
      $data = $this->getEditLimit($uuid);
      if ($data)
          return view('frontend.successEdit')->with('data',$data);
      else
          dd("uuid ".$uuid." is not valid.");
  }

  private function getEditLimit($uuid)
  {
      $data = DB::connection('mysql')->table('peserta')->where('barcode', $uuid)->select('created_datetime','ctredit','add_days')->first();
      if ($data && $data->created_datetime)
      {
          $db_time = strtotime($data->created_datetime) + Glob::timelimitedit + ($data->add_days * 24 * 3600);
          $limittime = strtotime(Glob::registrationCloseDate);
          $limittime = min($limittime, $db_time);
          $expiredDate = date('d-m-Y H:i:s',  $limittime);
          return (object)array("uuid" => $uuid, "ctredit" => $data->ctredit, "expiredDate" => $expiredDate);
      }
      else
          return 0;
  }

  public function test()
  {
      // Mail::raw('content', function($message) {
      //     $message->to('assajitjahjadi@gmail.com');
      //     $message->subject('E-Mail Example');

      //     $message->from($address, $name = null);
      //     $message->sender($address, $name = null);
      //     $message->to($address, $name = null);
      // });

      // Mail::send('email.test', ['name' => "nama", 'isi' => "Isi"],  function($message) {
      //     $message->from('noreply@asalhapuja.or.id', 'asalhapuja');
      //     $message->to('assajitjahjadi@gmail.com');
      //     $message->subject('E-Mail Example');
      // });
      
      //echo $this->getCountOfD();

      //$data = DB::connection('mysql')->table('peserta')->select('ID_PESERTA','NAMA','BARCODE','FORM','KEBANGSAAN','MEAL')->where('photo_validated','=',1)->get();
      
      //echo $data;
      
//       $res 	= 	DB::connection('mysql')->select(
  // 				"select NAMA, barcode, kebangsaan, PHONE, email from peserta where peserta.form='B'"
  // 				,[]
  // 		    );
  
  // foreach ($res as $p)
  // {
  //     $wa_phone = str_replace(array('-','+','(',')'), '', $p->PHONE);
  //     if (substr($p->PHONE, 0, 1)=="0")
// 				$wa_phone = "62".substr($wa_phone,1);
// 			echo $p->NAMA.'<BR>';
// 			echo 'wa https://web.whatsapp.com/send?phone='.$wa_phone.'<BR>';
// 			echo 'link http://register.asalhapuja.or.id/edit?uuid='.$p->barcode.'<BR>';
// 			echo $p->kebangsaan.'<BR>';
// 			echo $p->email.'<BR><BR>';
  // }
  
  	//return  $res;//array("DataUnit" => $res);
  
    //$this->sendEmailChangeTrigger('1a3ce822678d');
		echo "Counting </BR>";
		$total = 0;
		$cnt = 0;
		for($i = 0; $i<6; $i++)
		{
				$char = chr(65+$i);
				$cnt = $this->getCountOfGroup(chr(65+$i));
				$total += $cnt;
				echo $char." : ".$cnt."<BR/>";
		}
		echo "total ".$total;

    // return view('frontend.registerIndex2');
  }

  public function sendQueuedEmail()
  {
      Log::useFiles(storage_path().'/logs/mailSent.log');
      

      $dt = DB::connection('mysql')->table('email_queue')->whereNull('mail_sent')->first();
      
      if ($dt)
      {
          dd($dt);
          $headers = "From: ".$dt->mail_from;
          if (mail($dt->mail_to, htmlspecialchars($dt->mail_title,ENT_QUOTES), htmlspecialchars($dt->mail_content,ENT_QUOTES), $headers))
          {
              DB::connection('mysql')->table('email_queue')
                      ->where('mail_id','=', $dt->mail_id)
                      ->limit(1)
                      ->update(['mail_sent' => DB::raw('NOW()')]);

              Log::info("Mail sent : ".$dt->mail_to." ".time());
          }

          
      }
      return $dt;

  }

  private function randomString($length = 16)
  {
      $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

      return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
  }

  //depricated function
  // private function uniqidReal($len = 13) {
  //     // uniqid gives 13 chars, but you could adjust it to your needs.
  //     if (function_exists("random_bytes")) {
  //         $bytes = random_bytes(ceil($len / 2));
  //     } elseif (function_exists("openssl_random_pseudo_bytes")) {
  //         $bytes = openssl_random_pseudo_bytes(ceil($len / 2));
  //     } else {
  //         throw new Exception("no cryptographically secure random function available");
  //     }
  //     return substr(bin2hex($bytes), 0, $len);
  // }

  private function getCountOfGroup($tipe)
  {
      return $dt = DB::connection('mysql')->table('peserta')->where('form', '=', $tipe)->where('active', '=', '1')->where('waiting_list', '=', '0')->count();
  }

  private function getCountOfQueue($tipe)
  {
      return $dt = DB::connection('mysql')->table('peserta')->where('form', '=', $tipe)->where('active', '=', '1')->where('waiting_list', '=', '1')->count();
  }
=======
    private function getCountOfD()
    {
        return $dt = DB::connection('mysql')->table('peserta')->where('form', '=', 'D')->whereNull('ID_REKRUT')->count();
    }

    private function getCountOfDQueue()
    {
        return $dt = DB::connection('mysql')->table('peserta')->where('waiting_list', '=', '1')->count();
    }

    // public function RegisterA()
    // {
    //     return "B";
    // }
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
}