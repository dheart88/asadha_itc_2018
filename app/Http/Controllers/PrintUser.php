<?php

namespace App\Http\Controllers;

use App\User;
use App\Glob as Glob;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Datetime;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use \stdClass;


class PrintUser extends Controller
{
    public function listPrint(Request $r)
    {
        return view('backend.listPrint');
    }
    
    //TODO:pindahkan ke backend
    public function listPrintData(Request $r)
    {
        $dt = DB::connection('mysql')
                ->table('peserta')
                ->leftJoin('recruiter', 'peserta.id_rekrut', '=', 'recruiter.id_rekrut')
                ->select('ID_PESERTA','BARCODE','peserta.NAMA','peserta.FORM','PRINTED_NAME','PHOTO_VALIDATED','PHOTO_PRINTED',DB::raw("ifnull(recruiter.NAMA,'-') as GRUP"))
                ->where('active', '=', 1)
                ->where('photo_validated', '=', 1);
            
        if ((Input::has('name_lookup') && ($r->name_lookup)))
            $dt = $dt->where('peserta.nama', 'like', '%'.$r->name_lookup. '%')
                    ->orWhere('peserta.printed_name', 'like', '%'.$r->name_lookup. '%');
          
        if (Input::has('printed_status') && ($r->printed_status > -1))
            $dt = $dt->where('photo_printed', '=', $r->printed_status);
            
        if (Input::has('form') && ($r->form != ""))
            $dt = $dt->where('peserta.form', '=', $r->form);
            
        if (Input::has('order_by'))
        {
            switch($r->order_by)
            {
                case 0: 
                    $dt = $dt->orderBy('nama', 'asc');
                break;
                
                case 1: 
                    $dt = $dt->orderBy('printed_name', 'asc');
                break;
                
                case 2: 
                    $dt = $dt->orderBy('peserta.form', 'asc');
                break;
                
                case 3: 
                    $dt = $dt->orderBy('peserta.id_rekrut', 'asc');
                break;
                
                case 4: 
                    $dt = $dt->orderBy('peserta.id_peserta', 'asc');
                break;
            }
        }
                
        $dt = $dt->get();
        //return $dt;
        return view('backend.listPrintData')->with('data',$dt);
    }
    
    public function postPrintList(Request $r)
    {
        $data = [];
        
        if($r->list_id)
        {
            $data = DB::connection('mysql')->
                        table('peserta')->
                        select('ID_PESERTA','PRINTED_NAME','BARCODE','FORM','KEBANGSAAN','MEAL')->
                        whereIn('ID_PESERTA', $r->list_id)->
                        where('active','=',1)->
                        get();
        }
        
        return view('backend.preview')->with('data',$data);
    }
    
    public function postPrintSetBelumCetak(Request $r)
    {
        if($r->list_id)
        {
            $data = DB::connection('mysql')->
                        table('peserta')->
                        whereIn('ID_PESERTA', $r->list_id)->
                        where('active','=',1)->
                        update([
                            'photo_printed'   => 0,
                        ]);
        }
        return redirect()->route('listPrint');
    }
    
    public function postPrintSetSudahCetak(Request $r)
    {
        if($r->list_id)
        {
            $data = DB::connection('mysql')->
                        table('peserta')->
                        whereIn('ID_PESERTA', $r->list_id)->
                        where('active','=',1)->
                        update([
                            'photo_printed'   => 1,
                        ]);
        }
        return redirect()->route('listPrint');
    }
}