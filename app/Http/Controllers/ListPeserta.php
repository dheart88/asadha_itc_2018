<?php

namespace App\Http\Controllers;

use App\User;
use App\Glob as Glob;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Datetime;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use \stdClass;


class ListPeserta extends Controller
{
    public function listPeserta(Request $r)
    {
        return view('backend.listPeserta');
    }
    
    //TODO:pindahkan ke backend
    public function listPesertaData(Request $r)
    {
        $dt = DB::connection('mysql')
                ->table('peserta')
                ->leftJoin('recruiter', 'peserta.id_rekrut', '=', 'recruiter.id_rekrut')
                ->leftJoin('data_makan', 'peserta.id_peserta', '=', 'data_makan.id_peserta')
                ->select('peserta.ID_PESERTA','peserta.MEAL','BARCODE','peserta.NAMA','peserta.FORM','PRINTED_NAME','PHOTO_VALIDATED',
                        'PHOTO_PRINTED',DB::raw("ifnull(recruiter.NAMA,'-') as GRUP"),
                        'field_1','field_2','field_3','field_4','field_5','field_6','field_7'
                )
                ->where('active', '=', 1);
            
        if ((Input::has('name_lookup') && ($r->name_lookup)))
            $dt = $dt->where('peserta.nama', 'like', '%'.$r->name_lookup. '%')
                    ->orWhere('peserta.printed_name', 'like', '%'.$r->name_lookup. '%');
          
        // if (Input::has('printed_status') && ($r->printed_status > -1))
        //     $dt = $dt->where('photo_printed', '=', $r->printed_status);
            
        if (Input::has('form') && ($r->form != ""))
            $dt = $dt->where('peserta.form', '=', $r->form);
            
        if (Input::has('order_by'))
        {
            switch($r->order_by)
            {
                case 0: 
                    $dt = $dt->orderBy('nama', 'asc');
                break;
                
                case 1: 
                    $dt = $dt->orderBy('printed_name', 'asc');
                break;
                
                case 2: 
                    $dt = $dt->orderBy('peserta.form', 'asc');
                break;
                
                case 3: 
                    $dt = $dt->orderBy('peserta.id_rekrut', 'asc');
                break;
                
                case 4: 
                    $dt = $dt->orderBy('peserta.id_peserta', 'asc');
                break;
            }
        }
                
        $dt = $dt->get();
        
        
        
        //return $dt;
        return view('backend.listPesertaData')->with('data',$dt);
    }
    
}