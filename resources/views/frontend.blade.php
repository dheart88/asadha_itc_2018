
<!DOCTYPE html>


<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<title>@yield('title')</title>
		
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=6.0, minimum-scale=.25, user-scalable=yes" name="viewport" />
<<<<<<< HEAD:resources/views/frontend.blade.php
=======

		@yield('meta')

>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b:resources/views/frontend.blade.php
		
		<!-- reference color : #555299 -->
		<meta name="theme-color" content="#4a4785">


		@yield('meta')

		<!-- jQuery 3 -->
		<script src="{{asset('AdminLTE/bower_components/jquery/dist/jquery.min.js')}}"></script>

		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
		
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/font-awesome/css/font-awesome.min.css')}}">
		
		<!-- Ionicons -->
		<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/Ionicons/css/ionicons.min.css')}}">
		
		@yield('css')

		<!-- Theme style -->
		<link rel="stylesheet" href="{{asset('AdminLTE/dist/css/AdminLTE.css')}}">

		<!-- AdminLTE Skins. Usa Admin LTE Blue SKin -->
		@section('adminlte_skin')
<<<<<<< HEAD:resources/views/frontend.blade.php
		<link rel="stylesheet" href="{{asset('AdminLTE/dist/css/skins/skin-purple.min.css')}}">
		@show

=======
		<link rel="stylesheet" href="{{asset('AdminLTE/dist/css/skins/skin-blue.min.css')}}">
		@show
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b:resources/views/frontend.blade.php
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Google Font -->
		<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
	</head>


	<body class="hold-transition skin-purple layout-top-nav" style="height: auto; min-height: 100%;" onload()>
		<div class="wrapper">
		    
		  @section('header')
			<header class="main-header">
				<nav class="navbar navbar-fixed-top">
				  	<div class="container">
				  		<div class="navbar-header">
				  			<span class="navbar-brand">
				  				<div class="visible-md visible-lg">
<<<<<<< HEAD:resources/views/frontend.blade.php
				  					Indonesia Tipitaka Chanting 2563 / Asalha Mahapuja 2019
				  				</div>
				  				<div class="visible-sm visible-xs" style="width:100%;">
				  					<span style="float:left;">ITC 2563/2019</span> | 
				  					<span style="float:right;"><small>12-14 Juli 2019</small></span>

=======
				  					Indonesia Tipitaka Chanting 2562 <span class="hidden-xs"> / Asalha Mahapuja 2018</span>
				  				</div>
				  				<div class="visible-sm visible-xs">
				  					ITC 2562 / Asalha Mahapuja 2018
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b:resources/views/frontend.blade.php
				  				</div>
				  				
				  			</span>
				  		</div>
				  		<div class="navbar-custom-menu pull-right">
							<span class="navbar-brand hidden-sm hidden-xs"><small>Borobudur, 12-14 Juli 2019</small></span>
						</div>
				  	</div>
				</nav>
			</header>
			@show

			<!-- Full Width Column -->
			<div class="content-wrapper" style="padding-top: 50px; padding-bottom: 50px;">
				<div class="container">
					@yield('content-header')
					@yield('content')
				</div>
			<!-- /.container -->
			</div>

      
      @section('footer')
			<footer class="main-footer">
				<div class="container" style="width: 100%;">
					<span class="pull-left" style="font-size:small; font-weight: bold;">
						Sekretariat: Pusdiklat Buddhis Sikkhadama Santibhumi
						<p style="font-weight: normal;">
							BSD City Sektor VII Blok C No. 6<br>
							Tangerang Selatan, Banten – 15321
						</p>
					</span>
					<span class="pull-right">
						<small>(+62 21) 5316-7061</small>&nbsp;<span class="glyphicon glyphicon-earphone"></span>
						<small>info@asalhapuja.or.id</small>&nbsp;<span class="glyphicon glyphicon-envelope"></span>
					</span>
				</div>
			</footer>
      @show
            
      <!-- <div style="position: fixed; bottom: 20px; right: 25px; width: 40px; height: 40px; color: rgb(238, 238, 238); line-height: 40px; text-align: center; background-color: rgb(34, 45, 50); cursor: pointer; border-radius: 5px; z-index: 99999; opacity: 0.7; display: none;"><i class="fa fa-chevron-up"></i></div> -->

		</div>
	
	

	<!-- jQuery UI 1.11.4 -->
	<script src="{{asset('AdminLTE/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
	
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

	<!-- dipakai apabila ada js script tambahan -->
	@yield('script_body')

	<!-- FastClick -->
	<script src="{{asset('AdminLTE/bower_components/fastclick/lib/fastclick.js')}}"></script>

	<!-- AdminLTE App -->
	<script src="{{asset('AdminLTE/dist/js/adminlte.min.js')}}"></script>

	<!-- script utama yang after body - load() -->
	@yield('script_body_main')

	</body>
</html>