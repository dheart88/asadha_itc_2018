@extends('frontend')

@section('title', 'Registration Page')

@section('css')
	<style type="text/css">
		 @media (max-width: 768px) {
      .member-info{
         text-align: center;
      } 
    }
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi/ <i style="color: gray">Registration</i>
			</h3>
		</div>

		<ol class="breadcrumb">
	    <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
	    <li><a class="active">Registration</a></li>
	  </ol>
	</section>
@endsection

@section('content')
	<section class="content">

		<!-- Modal -->
		<div id="modal_cek_peserta" class="modal fade" role="dialog">
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

			</div>
		</div>

		<div class="row">
			<!-- Bagian untuk cek kepesertaan tahun 2019 -->
			<!-- col-md-6 -->
			<div class="col-xs-12"> 

				<div id='panel-check-membership' class="box box-primary">

		      <div class="box-header with-border bg-blue" style="padding:3px;  display:flex; flex-direction: row;">
						<div style="display:block; width:30px; line-height:30px; background-color:rgba(0,0,0,0.2); text-align:center;"> <i class="fa fa-search  fa-lg"></i>
						</div>
		        <div class="box-title" style="font-size:18px; flex:1; line-height:30px; margin:0 3px;">
		        	Cek Kepesertaan Tahun 2019
		        </div>
		      </div>

		      <div class="box-body">
		      	<div class="row">
			    		<!-- NAMA -->
			    		<form method="POST" action="javascript:void(0);" onSubmit="checkRegistration();">
			    		<div class="col-xs-12 col-sm-8">
	      					<label for="uuid2019">Kode Registrasi atau Passport atau KTP (<i>Registration code or Passport or KTP</i>)</label>
	      					<div style="display:flex; flex-direction: row;">
	      						<input type="text" class="form-control" id="uuid2019" name="uuid2019" placeholder="Masukkan kode registrasi / passport / ktp." maxlength="16" style="flex:1;">
	      						<input type="button" class="btn btn-primary" value="Cek" onclick="checkRegistration();">
	      					</div>
	  					</div>
	  				</form>
	  				</div>
		      </div>

		      <!-- border:1px solid silver; -->
		      <div class="box-body member-panel" style="display:none;">
		      	<hr style="margin:0px;">
						<div id="member-panel-container">
						</div>
					</div>

					<div class="overlay" style="display:none;">
              <i class="fa fa-refresh fa-spin"></i>
          </div>

		    </div>

		  </div>

			<!-- Bagian untuk cek kepesertaan tahun 2018 -->
			<div class="col-xs-12" style="display:none;"> 

				<div id='panel-prev-membership' class="box box-primary">
		      <div class="box-header with-border bg-blue" style="padding:3px; line-height:30px;">
						<div style="display:inline-block; height:30px; width:30px; background-color:rgba(0,0,0,0.2); text-align:center;"> <i class="fa fa-pencil-square  fa-lg"></i>
						</div>
		        <h3 class="box-title">Pendaftaran kembali (Under live development)</h3>
		      </div>

		      <div class="box-body">
		      	<p align="justify">*Akan siap pada 10 Maret 2019, tunggu wa/sms dari kami, ketentuan undangan first come first serve, dimana jika memang kuota form tersebut penuh, maka undangan tersebut tidak valid lagi.*
		      	Bagian ini diperuntukkan untuk peserta B, C, dan D yang telah mengikuti ITC Tipitaka chanting pada tahun 2018 (hanya ada 4 kategori tahun tersebut A sd D), dengan no KTP yang anda masukkan tahun sebelumnya atau kode yang kami berikan via wa/sms maka data anda dari tahun sebelumnya otomatis masuk kedalam form pendaftaran baru. Tujuan bagian ini hanya untuk menyalin data registrasi tahun lalu untuk mengurangi pekerjaan memasukkan data/foto secara manual untuk peserta 2018 yang belum terdaftar pada 2019. <u>Undangan yang kami berikan via WA/sms tidak menjamin ketersediaan registrasi, semua bergantung kuota masing-masing paket </u>.
		      	<p>
		      </div><!-- /.box-body -->
		      <div class="box-body">
		      	<hr style="margin:0px;">
		      	<div class="row">
			    		<!-- NAMA -->
			    		<form method="POST" action="javascript:void(0);" onSubmit="checkInvitation();">
			    		<div class="col-xs-12">
	      					<label for="invitationCode">Kode Undangan, nomor KTP atau passport/ <i>Invitation code, ID or passport</i></label>
	      					<input type="text" class="form-control" id="invitationCode" name="invitationCode" placeholder="Masukkan 5 digit kode registrasi" maxlength="5">
	      					<input type="button"class="btn btn-primary" value="Cek" onclick="checkInvitation();">
	  					</div>
	  				</form>
	  				</div>
		      </div>
		    </div>

		  </div>

		  <div class="col-xs-12"> 

				<div class="box box-info">
		      <div class="box-header with-border bg-blue" style="padding:3px;  display:flex; flex-direction: row;">
						<div style="display:block; width:30px; line-height:30px; background-color:rgba(0,0,0,0.2); text-align:center;"> <i class="fa fa-list-alt  fa-lg"></i>
						</div>
		        <div class="box-title" style="font-size:18px; flex:1; line-height:30px; margin:0 3px;">
		        	Info Pendaftaran / <i>General Information</i>
		        </div>
		      </div>
		      <div class="box-body">
		      	<div class="row col-md-10"> 
			      	<ul>

			      		<lI align="justify">
			      			<b><i class="fa fa-exclamation-triangle" style="color:tomato;"></i> Bagi peserta yang telah terdaftar. Kami akan mengubah seluruh kode 5 digit registrasi mengikuti nomor <u>KTP</u> atau Nomor NIK yang dilingkari merah (bukan nomor induk kk) seperti yang tertera pada contoh KK dibawah untuk anak yang belum memiliki KTP, ataupun <u>Passport</u> bagi WNA untuk perapian data. Kami mohon maaf sebelumnya atas perubahan ini. Jika terdapat pertanyaan, seperti data yang tidak dapat ditemukan dan sebagainya bisa menghubungi Bagian Registrasi. Data yang telah masuk sebelumnya tidak akan hilang, hanya link ataupun kode 5 digit akan berubah mengikuti kode baru nantinya. Perubahan akan dilakukan selambat-lambatnya 17 Maret 2019.</b>

			      			<span>
			      				<a target="_blank" href="https://api.whatsapp.com/send?phone=6281269110181" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span>Chat Via WA</a>
			      			</span>

			      			<div>			    			 
		        				<img class="img-responsive member-image" crossorigin="anonymous" style="display: block; max-width: 100%; max-height: 250px; margin: auto;" src="/KTPGuide.jpg">
		    					</div>
			      		</lI>

			      		<li align="justify">Pendaftaran secara umum buka hingga 30 Mei 2019. Kuota pendaftaran untuk setiap paket terbatas, apabila pendaftaran untuk paket tertentu telah penuh, sistem akan secara otomatis menutup pendaftaran lebih dini daripada tanggal yang tertera / <i>Registration will open until May 30th 2019. There are limits for each package, if the allocated quota is fulfilled, system will automatically close the registration links.</i></li>
			      		<li align="justify">Pembayaran untuk paket B dan C hingga 2 Juni 2019. <i>Payment for package B and C until June 2nd 2019</i>. 
									Kirim bukti pembayaran kepada saudara Assaji via WhatsApp di +62812-6911-0181 <i style="color: gray"> / Please send photo or screenshot of receipt via whatsapp to Assaji +62812-6911-0181</i>
									<span><a target="_blank" href="https://api.whatsapp.com/send?phone=6281269110181" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span>Chat Via WA</a></span>
			      		</li>
			      		<li align="justify">Updated: Sebaiknya pergunakan KTP atau Passport Untuk mengecek kembali pendaftaran anda pada bagian Cek Kepesertaan diatas</li>
			      		<li align="justify">Informasi lebih lanjut seperti tempat berkumpul untuk registrasi ulang di Magelang, perlengkapan dan hal-hal lebih teknis lainnya akan kami infokan dikemudian hari via sms/wa sekitar 1 bulan sebelum acara dimulai. </li>
			      		<li align="justify">Khusus untuk peserta B, mohon mengisi jam keberangkatan, nomor penerbangan atau nomor kode kereta api yang dipergunakan setelah iternary perjalanan dipersiapkan agar transportasi antar jemput dari bandara bisa kami aturkan.</li>
			      		<li align="justify">Khusus untuk perubahan paket, misalnya telah terdaftar pada paket E dan ingin berubah, semisal ke paket C mohon di infokan ke nomor diatas, mohon untuk tidak memasukkan data kembali</li>
			      		<li align="justify">Segala kelengkapan data seperti foto yang diupload, kontak darurat akan di cek kembali oleh panitia. Kami akan menginfokan kembali jika ada data yang kurang lengkap.</li>

			      		

			      	</ul>
		      	</div>
		      </div><!-- /.box-body -->
		    </div>

		  </div>

			<div class="col-xs-12">
				<div class="box box-primary">
			    <div class="bg-blue" style="min-height:90px;">
			        <span class="info-box-icon">
								<i class="fa fa-file-text-o"></i>
			        </span>

			        <div class="info-box-content ">
			          <span class="info-box-number">Pendaftaran Peserta Baru 2019 / <i>Registration for new participants</i></span>
			        </div>
			    </div>
			    <!-- Add the bg color to the header using any of the bg-* classes -->        
          <div class="box-body col-md-10">
            <ul class="nav nav-stacked">
              
							
              <li align="justify">
              	<a href="registerB">
									<span>
	       						<strong>Paket B</strong><br>
										Untuk umum dengan fasilitas hotel bintang 4, Antar jemput dari bandara, antar jemput premium dari hotel ke candi (pp).
										Besarnya biaya perorang sebesar Rp 4.000.000,- untuk kamar sharing berdua dengan peserta lain. Biaya tambahan sebesar Rp 1.500.000,- untuk kamar sendiri.
									</span>
              		<span class="pull-right badge bg-green">
              			Daftar <i class="fa fa-arrow-right" aria-hidden="true"></i>
              		</span>
									<div style="clear: both;"></div>
              	</a>
              </li>

              <li align="justify">
              	<a href="registerC">
									<span>
	       						<strong>Paket C</strong><br>
										Untuk peserta umum dengan fasilitas hotel bintang 2 dan antar jemput dari umum dari hotel ke candi (pp). Besarnya biaya perorang sebesar Rp 1.200.000,- untuk kamar sharing berdua dengan peserta lain. Biaya tambahan sebesar Rp 800.000,- untuk kamar sendiri.
									</span>
              		<span class="pull-right badge bg-green">
              			Daftar <i class="fa fa-arrow-right" aria-hidden="true"></i>
              		</span>
									<div style="clear: both;"></div>
              	</a>
              </li>

              <li align="justify">
              	<a href="registerD">
									<span>
	       						<strong>Paket D</strong><br>
										Untuk peserta dengan rekomendasi dari vihara. Peserta akan mendapatkan fasilitas penginapan yang disediakan oleh panitia dan antar jemput dari umum dari hotel ke candi (pp).
									</span>
              		<span class="pull-right badge bg-green">
              			Daftar <i class="fa fa-arrow-right" aria-hidden="true"></i>
              		</span>
									<div style="clear: both;"></div>
              	</a>
              </li>

              <li align="justify">
              	<a href="registerE">
									<span>
	       						<strong>Paket E</strong><br>
										Untuk peserta umum yang mengatur sendiri transportasi dan penginapannya.
									</span>
              		<span class="pull-right badge bg-green">
              			Daftar <i class="fa fa-arrow-right" aria-hidden="true"></i>
              		</span>
									<div style="clear: both;"></div>
              	</a>
              </li>

              <li align="justify">
              	<a>
								<span>
       						<strong>Paket F</strong> <br>
									Untuk peserta yang berasal dari jalur rekrumen, pendaftarannya akan diatur oleh tim atthasila. Peserta umum tidak dapat mendaftar melalui bagian ini
              		<span class="pull-right badge bg-gray">
              			<i class="fa fa-lock" aria-hidden="true"></i>
              		</span>
									<div style="clear: both;"></div>
              	</a>
              </li>

            </ul>
          </div>
        </div>
			</div>
		</div>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
@endsection

@section('script_body_main')
	<script type="text/javascript">
		var debug = null;

		function checkRegistration()
		{
   		var _uuid = $('#uuid2019').val().trim();

   		if (!_uuid)
   		{
   			modalShow('Error', 'Masukkan data yang dicari');
   			$('#uuid2019').focus();
   			return;
   		}

   		$('#panel-check-membership .overlay').show();
   		$('#panel-check-membership .member-panel').hide();

   		$('#member-panel-container').html('');

		  $.ajax({
					url: '/checkRegistration',
					type: 'POST',
					headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
					contentType: 'application/json',
					data: JSON.stringify({uuid:_uuid}),
					success: function(data) {
						$('#panel-check-membership .overlay').hide();
						debug = JSON.parse(data);

						var res = JSON.parse(data);
						var dt = res.data; 
						let counter = 1;
						if (dt.length > 0)
						{
							var headerMessage = `<div>
																			<h3>Data Anda / <i>Your Data</i></h3>
																		</div>`;
							$('#member-panel-container').append(headerMessage);

							for(var i = 0; i < dt.length; i++)
							{
								let detail = dt[i];
								let container = `	<hr style="margin:0px;">
																	<div class="row member-panel-content">
											    					<div class="col-xs-12 col-sm-4">			    			 
											        				<img class="img-responsive member-image" crossorigin="anonymous" style="display: block; max-width: 150px; max-height: 150px; margin: auto;" src="/ImageThumb/${detail.id_peserta}.jpg?${Math.floor(Math.random()*10000)}">
											    					</div>
											    					<div class="member-info col-xs-12 col-sm-8">
																			
																			<div class="member-name" style="border-bottom: 3px gray double; width: 12em; display:inline-block; font-weight:bold; font-size:large;">
																				${counter}. ${detail.nama}
																			</div><br>
																			
																			<div class='member-form' style="border-bottom: 1px gray solid; width: 12em; display:inline-block;">
																				Paket ${detail.form}
																			</div><br>
																			
																			<div class='member-registration-date' style="border-bottom: 1px gray solid; width: 12em; display:inline-block;">
																				<i class='fa fa-calander'> ${detail.created_datetime}</i>
																			</div><br>

																			<button class="btn btn-md btn-primary member-action" 
																				onclick="document.location.href='/edit?uuid=${detail.barcode}',true;">
																				Click here to view data&nbsp;<i class="fa fa-arrow-right"></i>
																			</button>
											    					</div>

											    				</div>`;
								counter++;

								$('#member-panel-container').append(container);
							}
							
							var duplicateMessage = `<div style="color:red; font-weight:bold;">
																				Sistem mendeteksi adanya data kembar. Data peserta yang di-affiliasi-kan beserta peserta ini tidak akan ditampilkan. Data yang kembar akan dihapus.
																			</div>
																			`
							if (dt.length > 1)
								$('#member-panel-container').append(duplicateMessage);
							else
							{
								let affiliates = [];
								
								if (dt.length >= 1)
									affiliates = dt[0].children;

								if (affiliates.length >= 1)
								{
									let affiliateMessage = `<div>
																				<h4>Data Orang Lain yang beraffiliasi / <i>affilates Data</i></h4>
																			</div>`;
									$('#member-panel-container').append(affiliateMessage);
								}

								for(var j = 0; j < affiliates.length; j++)
								{
									console.log('here');
									let detail = affiliates[j];
									let container = `	<hr style="margin:0px;">
																		<div class="row member-panel-content">
												    					<div class="col-xs-12 col-sm-4">			    			 
												        				<img class="img-responsive member-image" crossorigin="anonymous" style="display: block; max-width: 150px; max-height: 150px; margin: auto;" src="/ImageThumb/${detail.id_peserta}.jpg?${Math.floor(Math.random()*10000)}">
												    					</div>
												    					<div class="member-info col-xs-12 col-sm-8">
																				
																				<div class="member-name" style="border-bottom: 3px gray double; width: 12em; display:inline-block; font-weight:bold; font-size:large;">
																					${counter}. ${detail.nama}
																				</div><br>
																				
																				<div class='member-form' style="border-bottom: 1px gray solid; width: 12em; display:inline-block;">
																					Paket ${detail.form}
																				</div><br>
																				
																				<div class='member-registration-date' style="border-bottom: 1px gray solid; width: 12em; display:inline-block;">
																					<i class='fa fa-calander'> ${detail.created_datetime}</i>
																				</div><br>

																				<button class="btn btn-md btn-primary member-action" 
																					onclick="document.location.href='/edit?uuid=${detail.barcode}',true;">
																					Click here to view data&nbsp;<i class="fa fa-arrow-right"></i>
																				</button>
												    					</div>

												    	
												    				</div>`;
									counter++;
									$('#member-panel-container').append(container);
								}

							}

							$('#panel-check-membership .member-panel').show();
						}
						else
						{
							modalShow('Data Tidak ada / Data not found', 'Cek kembali kode registrasi yang anda terima, jika masih tidak berhasil, mohon hubungi +6281269110181 via wa/call/ Invalid registration code, Please check your code, if you think there is a technical problem please contact the number above via whatsapp.');
						}

					},
					error: function() {
						$('#panel-check-membership .overlay').hide();
						modalShow('Error', 'Terjadi kesalahan ketika mengambil data, mgkn token anda expired, mohon refresh kembali halaman ini');
					},
			
			});

		}

		function modalShow(titleHeader,message)
		{
			$('#modal_cek_peserta').modal('show');
			$("#modal_cek_peserta .modal-title").text(titleHeader);
			$("#modal_cek_peserta .modal-body").text(message); 
		}

	</script>`
@endsection
