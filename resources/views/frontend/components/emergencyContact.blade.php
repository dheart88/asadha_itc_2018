<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Kontak Darurat/<i style="color: gray">Emergency Contact</i></h3><br>
    <small class="row col-xs-12 col-sm-11 col-md-10 col-lg-9" style="text-align:justify;">
      <i class="fa fa-exclamation-triangle" style="color:tomato; "></i>
      Mohon diisi nama orang lain selain anda, jika terjadi sesuatu yang darurat kami akan menghubungi kontak ini/ Please fill it with emergency contact
    </small>
  </div>

  <div class="box-body" style="margin-left: 10px;">
    <div class="form-group row">
    <!--  -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <label for="emergency_cp">Nama / <i style="color: gray">Name</i></label>
        <input type="text" value="{{$emergency_cp}}" class="form-control" id="emergency_cp" name="emergency_cp"  maxlength="40" size="40" placeholder="In case of emergency please contact">
      </div>
  </div>

  <div class="form-group row">
    <!-- Emergency phone -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
          <label for="emergency_phone">No Telp / <i style="color: gray">Phone Number</i></label>
          <input type="text" value="{{$emergency_phone}}" class="form-control" id="emergency_phone" name="emergency_phone" maxlength="35" size="35" placeholder="required">
      </div>
    </div>

    <div class="form-group row">
      <!-- Emergency Email -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
          <label for="emergency_email">Email / <i style="color: gray">Email</i></label>
          <input type="email" value="{{$emergency_email}}" class="form-control" id="emergency_email" name="emergency_email" maxlength="60" size="60" placeholder="boleh kosong / Optional">
      </div>
    </div>
  </div>
</div>