<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Ketentuan dan Detil Acara untuk Grup D</h3>
  </div>

  <div class="box-body" style="margin-left: 10px;">
    <div class="form-group row">
      <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
        <p>Registrasi ini diperuntukkan untuk peserta yang mengikuti keseluruhan acara dan di-rekomendasikan oleh vihara setempat ataupun organisasi bernaung. Mohon untuk menunjukkan surat rekomendasi atau kartu keanggotaan vihara/ organisasi. Dan Peserta di wajibkan <strong> mengikuti keseluruhan acara dari 12-14 Juli 2019</strong> Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja. Bagi Umat yang hanya akan mengikuti Asalha Mahapuja harap datang langsung ke Candi Mendut pada tgl 14-7-2019 pkl. 14.00 untuk mengikuti prosesi Asalha Mahapuja dan tidak perlu melakukan registrasi ini</p>
        <strong>Dengan mengikuti seluruh acara Panitia akan menyediakan</strong>
          <ul>
            <li>Tiket masuk Borobudur sesuai jadwal acara</li>
            <li>penginapan selama acara. Check-in dari tanggal 11 dan check-out pada tanggal 15 sebelum tengah hari </li>
          <li>Makan 2x sehari sesuai jadwal (sarapan di penginapan, makan siang di area ITC)</li>
          <li>Air minum di lokasi ITC</li>
          <li>Transportasi dengan bus AC dari hotel ke Borobudur PP.</li>
        </ul>
      </div>
    </div>
  </div>

</div>