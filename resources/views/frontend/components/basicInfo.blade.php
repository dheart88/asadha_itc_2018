{{-- This component relies on jquery loaded first due its nature to inject values --}}
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Data Diri / <i style="color: gray">Identity</i></h3>
  </div>

  <div class="box-body" style="margin-left: 10px;">
    <div class="form-group row">
      <!-- Foto -->
      <div class="col-xs-12 col-sm-12 col-lg-9">
        
        @if($photoValidated != 1)
        <label for="gambar" class="cameraButton">Take a picture
          <input type="file" name="gambar" id="gambar" accept="image/*;capture=camera" onchange="readURL(this);">
        </label>
        @endif
        
        <div class="panel panel-default">
          @if($photoExist) 
            <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{URL::to('/').'/ImageThumb'.'/'.$id_peserta.'.jpg?'.time()}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
          @else
            <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
          @endif
        </div>
      </div>

    </div>

    <div class="form-group row">
      <!-- NAMA -->
      <div class="col-xs-12 col-sm-12 col-lg-9">

          <label for="nama">Nama / <i style="color: gray">Name</i></label>
          <input type="text" value="{{$name}}" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama / Enter Your Name" maxlength="50" size="50">
      </div>
    </div>

    <div class="form-group row">
      <!-- PRINTED NAME -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <label for="printed_name">Nama Tercetak/ <i style="color: gray">Printed Name</i></label>
        <input type="text" value="{{$printed_name}}" class="form-control" id="printed_name" name="printed_name" placeholder="Maksimum 20 karakter / 20 chars max" 
            maxlength="20" size="20" style="text-transform: uppercase;">
      </div>
    </div>

    <div class="form-group row">
        <!-- UMAT -->
        <div class="col-xs-12 col-sm-6 col-lg-5">
          <label for="umat">Saya adalah / <i style="color: gray">I'm a</i></label>

          <div class="radio">
            <label><input type="radio" class="umat-option" name="umat" value="0">Bhikkhu / <i style="color: gray">Monk</i></label>
          </div>
          <div class="radio">
            <label><input type="radio" class="umat-option" name="umat" value="1">Samanera / Novice</label>
          </div>
          <div class="radio">
            <label><input type="radio" class="umat-option" name="umat" value="2">Anagarini / <i style="color: gray">Atthasilani</i></label>
          </div>
          <div class="radio">
            <label><input type="radio" class="umat-option" name="umat" value="3">Umat / <i style="color: gray">Lay Person</i></label>
          </div>
          
        </div>

      <!-- JENIS KELAMIN -->
      <div class="col-xs-12 col-sm-6 col-lg-4">
        <label for="jk">Jenis Kelamin / <i style="color: gray">Gender</i></label>
        <div class="radio">
          <label><input type="radio" class='jk-option' name="jk" value="M" >Pria / <i style="color: gray">Male</i></label>
        </div>

        <div class="radio">
          <label><input type="radio" class='jk-option' name="jk" value="F">Wanita / <i style="color: gray">Female</i></label>
        </div>
      </div>

    </div>

    <hr>

    <div class="form-group row">
      <!-- Organisasi -->
        <div class="col-xs-12 col-sm-6 col-lg-5">
            <label for="organisasi">Organisasi / <i style="color: gray">Organization</i></label>
            <input type="text" value="{{$organisasi}}" class="form-control" name="organisasi" id="organisasi" placeholder="boleh kosong" maxlength="40" size="40" style="text-transform:uppercase">
        </div>

      <!-- Jabatan -->
        <div class="col-xs-12 col-sm-6 col-lg-4">
            <label for="jabatan">Jabatan / <i style="color: gray">Designation</i></label>
            <input type="text" value="{{$jabatan}}" class="form-control" name="jabatan" id="jabatan" placeholder="boleh kosong" maxlength="40" size="40" style="text-transform:uppercase">
        </div>
    </div>

    <!-- Tanggal lahir  -->
    <div class="form-group row">
      
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <label for="tgl_lahir">Tanggal Lahir / <i style="color: gray">Date Of Birth (dd-mm-yyyy)</i></label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="{{$tgl_lahir_dmy}}">
          </div> 
        </div>
    </div>

    <div class="form-group row">

      @if ($indonesian_only == false)
        <!-- Asal Negara -->
        <div class="col-xs-12 col-sm-6 col-lg-5">
          <label for="kebangsaan">Asal Negara / <i style="color: gray">Nationality</i></label>
          <select class="form-control" name="kebangsaan" id="kebangsaan">
            <option></option>
          </select>
        </div>
        
        <!-- KTP -->
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <label for="ktp">KTP (<i style="color: gray">Indonesian Only</i>)</label>
          <input value="{{$ktp}}" type="number" pattern="[0-9]*" min="0" max="9999999999999999" inputmode="numeric" class="form-control" name="ktp" id="ktp" maxlength="16" size="16" placeholder="kosong jika tidak memiliki KTP">
        </div>

        <!-- Passport -->
        <div class="col-xs-12 col-sm-6 col-sm-offset-6 col-lg-4 col-lg-offset-5">
          <label for="passport">Passport / <i style="color: gray">Passport</i></label>
          <input value="{{$passport}}"type="text" class="form-control" name="passport" id="passport"  maxlength="16" size="16" placeholder="boleh kosong">
        </div>
      @else
        <!-- KTP -->
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <label for="ktp">KTP (<i style="color: gray">Indonesian Only</i>)</label>
          <input value="{{$ktp}}" type="number" pattern="[0-9]*" min="0" max="9999999999999999" inputmode="numeric" class="form-control" name="ktp" id="ktp" maxlength="16" size="16" placeholder="kosong jika tidak memiliki KTP">
        </div>
      @endif
    </div>
 
    <div class="form-group row">
      <!-- Alamat -->
        <div class="col-xs-12 col-lg-9">
            <label for="alamat">Alamat / <i style="color: gray">Full Address</i></label>
            <textarea id="alamat" name="alamat" class="form-control" rows="4" data-limit-rows="true" maxlength="120" placeholder="Enter..." style="resize:none;width: 100%;">{{$alamat}}</textarea>
        </div>
    </div>

    <div class="form-group row">
      <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6">
        <label for="phone">No Telp/ HP <i style="color: gray">Phone / Mobile Phone <BR>
          Aktif WhatsApp <span class="fa fa-whatsapp" aria-hidden="true"></span> jika memungkinkan/ using WhatsApp if possible</i>
        </label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="glyphicon glyphicon-phone"></i>
          </div>
              
          <input type="tel" value="{{$phone}}" inputmode="tel" name="phone" id="phone" class="form-control" maxlength="20" size="20" placeholder="contoh/ex : +62812345678 / 021-1111111">
        </div>
      </div>
    </div>

    <!-- Email  -->
    <div class="form-group row">
      <div class="col-xs-12 col-lg-9">
        <label for="email">Email / <i style="color: gray">Email</i></label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="glyphicon glyphicon-envelope"></i>
          </div>
          <input type="email" value="{{$email}}" inputmode="email" class="form-control" id="email" name="email" placeholder="Boleh kosong" maxlength="60" size="60">
        </div>
      </div>
    </div>

    <!-- Meal -->
    <div class="form-group row">
      <div class="col-xs-12 col-sm-6 col-lg-4">
        <label for="meal">Makanan / <i style="color: gray">Meal</i></label>
        <div class="radio">
          <label><input type="radio" class="meal-option" name="meal" value="0">Vegetarian</label>
        </div>

        <div class="radio">
          <label><input type="radio" class="meal-option" name="meal" value="1">Non Vegetarian</label>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- Script to inject values by jquery --}}
<script>
  var kebangsaan = null;
  var photoExist = {{$photoExist?'true':'false'}};
  $(function(){
    $('.umat-option[value={{$umat}}]:eq(0)').attr('checked','checked');
    $('.jk-option[value={{$jk}}]:eq(0)').attr('checked','checked');
    $('.meal-option[value={{$meal}}]:eq(0)').attr('checked','checked');
    
    if ({{$indonesian_only?'true':'false'}})
    {
      formRules.ktp.required = 'required';
      formMessages.ktp.required = 'KTP harus di-isi. Jika tidak memiliki KTP, lihat 16 angka disamping nama yang tertera pada kartu keluarga';
    }

    $.ajax({
      url: "/country.json",
      cache: false,
      dataType: 'json',
      success: function(json){
        var dt = [];
        if( Object.prototype.toString.call( json ) === '[object Array]' ) {
          for (var i = 0; i < json.length; i++) {
            var obj = {};
            var name = json[i];
            obj['id'] = name;
            obj['text'] = name;
            dt.push(obj);
          }

          $('#kebangsaan').select2({
              data:dt, 
              width: '100%', 
              placeholder:"Pilih / Please Select",
              allowClear:true,
          });
          
          $("#kebangsaan").val(`{{$kebangsaan}}`);
          $("#kebangsaan").trigger('change');//.done(function(){
          $('#kebangsaan').change(function() {
            var ktpEnabled = ($('#kebangsaan').val().toLowerCase() == 'indonesia')
            $("#ktp").val('').prop('disabled', !ktpEnabled);
          });
          //});

          

        }
      }
    });

  });

  
</script>