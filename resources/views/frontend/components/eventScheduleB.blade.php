<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Paket yang dipilih/<i style="color: gray">Package</i></small>
      </h3>
    </div>

      <div class="box-body" style="margin-left: 10px;">

        <div class="form-group row">
          <!-- UMAT -->
          <div class="col-xs-12 col-lg-9">
            <label for="program">Acara Yang akan diikuti / <i style="color: gray">I will attend</i> </label>
            <p>12 - 14 Juli 2019 Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja</p>
          </div>
        </div>

        <div class="form-group row">
          <!-- KAMAR -->
          <div class="col-xs-12 col-lg-9">
            <label for="paket">Jenis Kamar / <i style="color: gray">kamar</i></label>

          <div class="radio">
            <label>
              <input type="radio" name="paket" value="0" {{$package==0?'checked':''}}  {{$enableOption?'':'disabled'}} } >
              Twin Package : Rp. 4.000.000 (IDR) 4 malam di hotel berbintang empat, kamar twin sharing berdua 
              <i>(4 nights at four stars hotel, twin bed &amp; sharing room)</i>
            </label>
          </div>
          
          <div class="radio">
            <label>
              <input type="radio" name="paket" value="1" {{$package==1?'checked':''}}  {{$enableOption?'':'disabled'}}>
              Single Package : Rp. 5.500.000 (IDR) 4 malam di hotel berbintang empat, kamar sendiri<i>(4 nights at four stars hotel, private room)</i>
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
          <strong>Harga paket sudah termasuk / The price include :</strong>
          <ul>
            <li>Antar jemput di Bandara Adi Sucipto Yogyakarta. <i style="color: gray">/Transportation airport - hotel</i></li>
            <li>Penginapan 4 malam di Hotel yang disediakan panitia di Magelang (Check-in 11 Juli 2019 setelah Jam 14.00 dan Check-out 15 Juli 2019 sebelum Jam 12.00 siang) <i style="color: gray"> / Hotel on July 11th-15th, 2019</i></li>
            <li>Tiket masuk Borobudur sesuai jadwal acara. <i style="color: gray"> / Entry Fee for Borobudur</i></li>
            <li>Makan 2x sehari sesuai jadwal (sarapan di hotel, makan siang di area ITC )<i style="color: gray"> / Breakfast at Hotel &amp; lunch at ITC venue </i></li>
            <li>Air minum di lokasi ITC. <i style="color: gray">/ Drinking water at ITC Venue</i></li>
            <li>Transportasi dengan bus AC dari hotel ke Borobudur PP. <i style="color: gray">/ Bus with AC from hotel - Borobudur, vice versa</i></li>
          </ul>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
          <strong>Harga paket tidak termasuk<i style="color: gray">/ The price does not include</i> :</strong>
          <ul>
              <li>Transportasi di luar jadwal acara<i style="color: gray"> / Transportation by your own request</i></li>
            <li>Pengeluaran pribadi lainnya <i style="color: gray">/ Other personal expenses</i></li>
          </ul>
        </div>
      </div>
      
      <div class="form-group row">
        <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
          <strong>Payment transfer detail :</strong>
          <ul>
            <li>Pembayaran harga paket ditransfer ke rekening Bank BCA Cabang serpong no. rek 497-878-5555 a.n Yayasan Sangha Theravada Indonesia, paling lambat tanggal 30 Mei 2019 
              <i style="color: gray"> / (Transfer from overseas please transfer to Bank Name : BCA, account number : 497-878-5555, account name : Yayasan Sangha Theravada Indonesia, Swift Code: CENAIDJA before June 12th 2019. Please contact us via whatsapp  Mr. Daudy +62818-0844-1767 if you need help)</i>
              <span>
                <a target="_blank" href="https://api.whatsapp.com/send?phone=6281808441767" class="btn btn-success btn-xs" role="button">
                  <span class="glyphicon glyphicon-phone"></span>chat using WA
                </a>
              </span>
            </li>
            <li>Upload bukti pembayaran setelah registrasi berhasil dengan mengedit kembali data anda. Jika ada kendala ataupun perubahan paket mohon hubungi nomor disamping<i style="color: gray"> / Please upload photo or screenshot of receipt on the section above, or if there is any problem or room package change please contact Assaji via WA +62812-6911-0181</i>
              <span>
                <a target="_blank" href="https://api.whatsapp.com/send?phone=6281269110181" class="btn btn-success btn-xs" role="button">
                  <span class="glyphicon glyphicon-phone"></span>
                  Click using WA
                </a>
              </span>
            </li>
          </ul>
        </div>
      </div>

      </div>
  </div>