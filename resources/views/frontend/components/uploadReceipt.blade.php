<div class="box box-default">

  <div class="box-header with-border">
    <h3 class="box-title">Bukti Pembayaran / <i style="color: gray">Bank transfer receipt </i></h3>
  </div>

  <div class="box-body" style="margin-left: 10px;">

    <div class="form-group row">
      <div class="col-xs-12 col-sm-12 col-lg-9">
        <label for="receipt" class="uploadButton">Upload
          <input type="file" name="receipt" id="receipt" accept="image/*;capture=camera" onchange="previewReceipt(this);">
        </label>

        <div class="panel panel-default">
          @if($hasReceipt) 
          <img id="receiptImage" class="img-responsive" crossorigin="anonymous" src="{{'/receipt'.'/'.$id_peserta.'.jpg?'.time()}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
          @else
          <img id="receiptImage" class="img-responsive" crossorigin="anonymous" src="{{asset('receipt.png')}}" style="display: block; max-height: 300px;"/>
          @endif
        </div>
        <!-- <p style="text-align:center"><small>rotasi gambar akan diperbaiki ketika gambar diupload / Image will be rotate when uploaded</small></p> -->
        <p style="text-align:center"><small>data hanya akan tersimpan ketika anda menekan save change dibawah / image will be uploaded when you press Save Changes button</small></p>
      </div>
      
    </div>
  </div>
</div>