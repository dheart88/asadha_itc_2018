<div class="box box-default">

  <div class="box-header with-border">
    <h3 class="box-title">Pernyataan/<i style="color: gray">Agreement</i></small>
    </h3>
  </div>

  <div class="box-body" style="margin-left: 10px;">
    @foreach ($agreements as $item)
      <div class="form-group row">
        <div class="col-xs-12 col-lg-9">
          <label style="font-weight: normal;">
            <input class="accepted" type="checkbox">{!! $item !!}</label>
        </div>
      </div>
    @endforeach
  
    <div class="row" style="text-align:center;">
      <div class="col-xs-12">
        <input type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Register" disabled>
      </div>
    </div>

  </div>
</div>