{{-- Not Implemented in production --}}
<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Data Kepala Kelompok</h3>
    </div>

      <div class="box-body" style="margin-left: 10px;">
     
        <div class="form-group row">
          <input type="hidden" name="uuid_recruiter" value="{{$data->uuid_recruiter}}">
          
          <div class="col-xs-3 col-sm-3 col-lg-2">
            <label>KELOMPOK</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
          </div>
          <div class="col-xs-9 col-sm-9 col-sm-10">
            <p>{{$data->nama_recruiter}}<p>
          </div>

          <div class="col-xs-3 col-sm-3 col-lg-2">
            <label>KEPALA</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
          </div>
          <div class="col-xs-9 col-sm-9 col-sm-10">
            <p>{{$data->cp_recruiter}} - {{$data->hp_recruiter}}</p>
            <span><a href="tel:{{$data->hp_recruiter}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> Call</a></span>
            <span><a href="sms:{{$data->hp_recruiter}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> SMS</a></span>
            <span><a href="https://api.whatsapp.com/send?phone={{substr($data->hp_recruiter,1)}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>
          </div>

          @if($data->cp_recruiter2!=null)
          <div class="col-xs-3 col-sm-3 col-lg-2">
            <label>KEPALA2</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
          </div>
          <div class="col-xs-9 col-sm-9 col-sm-10">
            <p>{{$data->cp_recruiter2}} - {{$data->hp_recruiter2}}</p>
            <span><a href="tel:{{$data->hp_recruiter2}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> Call</a></span>
            <span><a href="sms:{{$data->hp_recruiter2}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> SMS</a></span>
            <span><a href="https://api.whatsapp.com/send?phone={{substr($data->hp_recruiter2,1)}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>
          </div>
          @endif

          @if($data->email_recruiter!=null)
          <div class="col-xs-3 col-sm-3 col-lg-2">
            <label>EMAIL</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
          </div>
          <div class="col-xs-9 col-sm-9 col-sm-10">
            <p>{{$data->email_recruiter}}</p>
          </div>
          @endif
        </div>	
      </div>
  </div>