<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Detil Acara untuk grup E/<i style="color: gray">Event Schedule for Registration E</i></small>
    </h3>
  </div>

  <div class="box-body" style="margin-left: 10px;">
    <div class="form-group row">
      <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
        <p style="text-align:justify;">Registrasi ini diperuntukkan untuk peserta mandiri yang mengatur seluruh jadwal perjalanannya sendiri dan Peserta di wajibkan <strong> untuk mengikuti keseluruhan acara dari 12-14 Juli 2019</strong> Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja. Peserta wajib mengatur sendiri akomodasi penginapan dan segala transportasinya selama acara. Registrasi ini tidak dipungut biaya apapun. Bagi Umat yang hanya akan mengikuti Asalha Mahapuja harap datang langsung ke Candi Mendut pada tgl 14-7-2019 pkl. 14.00 untuk mengikuti prosesi Asalha Mahapuja dan tidak perlu melakukan registrasi ini</p>
        <strong style="text-align:justify;">Dengan mengikuti seluruh acara Panitia akan menyediakan</strong>
        <ul>
          <li style="text-align:justify;">Tiket masuk Borobudur sesuai jadwal acara</li>
          <li style="text-align:justify;">Makan 2x sehari sesuai jadwal (sarapan di penginapan, makan siang di area ITC)</li>
          <li style="text-align:justify;">Air minum di lokasi ITC</li>
        </ul>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
        <p style="text-align:justify;">This registration is intended for independent participants, therefore please arrange your transportation and lodging. Event is held on July 12th-14th 2019</strong> Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja. There is no fee for this registration.</p>
        <strong style="text-align:justify;">The organizer will provide these :</strong>
        <ul>
          <li style="text-align:justify;">Borobudur Entry ticket</li>
          <li style="text-align:justify;">Breakfast and lunch. Dinner won't be provided</li>
          <li style="text-align:justify;">Drinking water</li>
        </ul>
      </div>
    </div>
  </div>
</div>