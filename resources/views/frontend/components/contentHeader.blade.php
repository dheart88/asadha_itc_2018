<section class="content-header">
		<div>
			<h3 style="text-align:center">{!!$headerTitle!!}
			</h3>
		</div>

		<ol class="breadcrumb">
			<li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
			<li><a href="{{ url('/') }}">Registration</a></li>
			<li><a class="active">{!!$currentPage!!}</a></li>
		</ol>
	</section>