<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Kedatangan/<i style="color: gray">Arrival</i></small>
    </h3>
  </div>

  {{-- ARRIVAL --}}
  <div class="box-body" style="margin-left: 10px;">

    <div class="form-group row">
      <!-- Arrival Transportation -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <label for="arrival_transport">No Pesawat/Kereta/Bus <i style="color: gray">Flight/Train/Bus No</i></label>
        <input type="text" name="arrival_transport" id="arrival_transport" value="{{$arrival_transport}}" class="form-control" maxlength="40" size="40" placeholder="contoh/ex : AirAsia A123, KA Pasudan">
      </div>

      <!-- Arrival Origin -->
      <div class="col-xs-12 col-sm-6 col-lg-4">
        <label for="arrival_from">Asal Kota <i style="color: gray">Origin</i></label>
        <input type="text" name="arrival_from" id="arrival_from" value="{{$arrival_from}}" class="form-control" maxlength="40" size="40" placeholder="">
      </div>

      <div class="clearfix"></div>

      <!-- Arrival Date -->
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <label for="arrival_date">Tanggal Tiba / <i style="color: gray">Arrival Date</i></label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="arrival_date" class="form-control datepicker" id="arrival_date">
        </div>
      </div>

      <!-- Arrival Time -->
      <div class="col-xs-12 col-sm-6 col-lg-4">
        <label for="arrival_time">Perkiraan Jam Tiba(<i style="color: gray">Estimated Arrival Time</i>)</label>
        <div class="bootstrap-timepicker">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-clock-o"></i>
            </div>
            <input type="text" name="arrival_time" value="{{$arrival_time}}" class="form-control timepicker" id="arrival_time">
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

{{-- DEPARTURE --}}
<div class="box box-default">

  <div class="box-header with-border">
    <h3 class="box-title">Keberangkatan / <i style="color: gray">Departure</i></small>
    </h3>
  </div>

    <div class="box-body" style="margin-left: 10px;">

    <div class="form-group row">
      <!-- Kedatangan -->
      <!-- Nama kereta atau pesawat / Flight or Train detail -->
        <div class="col-xs-12 col-sm-6 col-lg-5">
            <label for="departure_transport">No Pesawat/Kereta/Bus <i style="color: gray">Flight/Train/Bus No</i></label>
        <input type="text" name="departure_transport" id="departure_transport" value="{{$departure_transport}}" class="form-control" maxlength="40" size="40" placeholder="contoh/ex : AirAsia A123, KA Pasudan">
        <!-- <select class="js-data-example-ajax"></select> -->
        </div>

      
        <div class="col-xs-12 col-sm-6 col-lg-4">
            <label for="departure_to">Kembali Ke Kota <i style="color: gray">Departure to</i></label>
        <input type="text" name="departure_to" id="departure_to" value="{{$departure_to}}" class="form-control" maxlength="40" size="40" placeholder="">
        <!-- <select class="js-data-example-ajax"></select> -->
        </div>

        <div class="clearfix"></div>

      <!-- Arrival Date -->
        <div class="col-xs-12 col-sm-6 col-lg-5">
            <label for="departure_date">Tanggal Berangkat / <i style="color: gray">Departure Date</i></label>
            <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control datepicker" id="departure_date" name="departure_date">
        </div>
        </div>

      <!-- Arrival Date -->
        <div class="col-xs-12 col-sm-6 col-lg-4">
            <label for="departure_time">Jam Berangkat(<i style="color: gray">Departure Time</i>)</label>
            <div class="bootstrap-timepicker">
                <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-clock-o"></i>
            </div>
            <input type="text" class="form-control timepicker" value="{{$departure_time}}" id="departure_time" name="departure_time">
          </div>
        </div>
        </div>
    </div>

    </div>
</div>