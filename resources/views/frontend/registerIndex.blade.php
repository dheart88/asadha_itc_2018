@extends('frontend')

@section('title', 'Registration Page')

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi/ <i style="color: gray">Registration</i>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a class="active">Registration</a></li>
        </ol>
	</section>
@endsection

@section('content')
	<section class="content">
		<div class="box box-default">
		  	<div class="box-body" style="margin-left: 10px;">
		  		<div>
		  			<img scr="http://asalhapuja.or.id/wp-content/uploads/2017/04/logo.jpg" style="display: inline-block;">
		  		</div>
		  		<h5>
		  			Registrasi di kategorikan menjadi 4 sebagai berikut
		  		</h5>
				<ul>
					<li><a href="#">Bagian Registrasi Untuk Undangan Khusus / <i>Special Invitation</i></a>*</li>
					<li><a href="registration_with_accomodation">Bagian Registrasi Untuk Umum 4 Hari / <i>4 Nights Package</i></a></li>
					<li><a href="registration_without_accomodation">Bagian Registrasi Untuk Umum Tanpa Akomodasi / <i>No Accomodation Participant</i></a></li>
					<li><a href="upasakaupasika-atthasila">Bagian Registrasi Untuk Upasaka Upasika Atthasila (Rombongan)</a></li>
				</ul>
				<p>*Apabila lupa atau informasi link yang diberikan untuk undangan sebelumnya hilang mohon untuk menghubungi panitia, informasi lebih lanjut dapat dilihat di situs utama</p>
		  	</div>
		  <!-- /.box-body -->
		</div>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
@endsection

@section('script_body_main')
@endsection
