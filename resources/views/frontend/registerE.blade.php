@extends('frontend.registerBase')

@section('title', 'Registration Page - Paket E')

@section('content-header')
	{{-- Content Header for Registration E --}}
	@include('frontend.components.contentHeader',[
		'headerTitle' => 'Registrasi E/ <i style="color: gray">Registration E</i>',
		'currentPage' => 'Registrasi E'
	])
@endsection

@section('content')
	<section class="content">
		@parent

		<!-- novalidate="novalidate" -->
		<form class="form" name="formRegister" id="formRegister" method="post" action="{{ url('/') }}/postRegistration" enctype="multipart/form-data" autocomplete="off">
			{{ csrf_field() }}
			<input type="hidden" name="form" id="form" value="E">
			<input type="hidden" name="program" value="0"> 

			{{-- Basic Info --}}
			@include('frontend.components.basicInfo',[
				'id_peserta'     			=> null,
				'name' 								=> '',
				'printed_name' 				=> '',
				'umat' 								=> -1,
				'jk' 									=> -1,
				'organisasi' 					=> '',
				'jabatan' 						=> '',
				'tgl_lahir_dmy'   		=> '',
				'indonesian_only' 		=> false,
				'kebangsaan' 					=> 'Indonesia',
				'ktp' 								=> '',
				'passport' 						=> '',
				'alamat' 							=> '',
				'meal'   							=> -1,
				'phone' 							=> '',
				'email' 							=> '',
				'photoExist' 					=> false,
				'photoValidated'  		=> false,
			])

			<!-- Event Schedule -->
			@include('frontend.components.eventScheduleE')

			{{-- Emergency Contact --}}
			@include('frontend.components.emergencyContact',[
				'emergency_cp' 		=> '',
				'emergency_phone' => '',
				'emergency_email' => '',
			])

			{{-- Agreement --}}
			@include('frontend.components.agreement',[
				'agreements' => [
					'Berusia lebih dari atau sama dengan 12 Tahun / <i>At least 12 years Old</i>',
					'Mengikuti Tipitaka Chanting dan Asalha Maha Puja sesuai jadwal acara. <i>/ Follow Tipitaka Chanting and Asalha Maha Puja Schedule</i>',
					'Pakaian putih (atas) hitam (bawah) atau putih-putih selama acara <i>/ Wear black and white or all white</i>',
					'Telah membaca dan sepakat dengan ketentuan paket <i>/ I have read and understand to package term and condition above</i>',
				]
			])

		</form>
	</section>

@endsection


