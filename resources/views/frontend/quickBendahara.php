<?php
    //echo var_dump($data);
    //return 0;
?>
@extends('frontend')

@section('title', 'Registration Page')

@section('css')
	<style>
	.row-striped:nth-of-type(odd){
	  background-color: #efefef;
	}

	.row-striped:nth-of-type(even){
	  background-color: #ffffff;
	}
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Menu Admin Sementara</i>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="https://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a class="active">Messy Admin page by aji.. sorry!</a></li>
        </ol>
	</section>
@endsection

@section('content')

	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
    			<!-- Modal content-->
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal">&times;</button>
    					<h4 class="modal-title">Modal Header</h4>
    				</div>
    				<div class="modal-body">
    					<p>Some text in the modal.</p>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				</div>
    			</div>
			</div>
		</div>

		<div class="box box-default">
			
		  	<div class="box-body" style="margin-left: 10px; margin-right: 10px;">

		  		<?php $ct = 1;?>
		  		@foreach ($data->PESERTA as $peserta)
		  		<?php
  					$wa_phone = str_replace(array('-','+','(',')'), '', $peserta->PHONE);
  					if (substr($peserta->PHONE, 0, 1)=="0")
  						$wa_phone = "62".substr($wa_phone,1);
  				?>
		  		<div class="row row-striped">	
		  				<div class="hidden-xs hidden-sm hidden-md col-xs-12  col-lg-1">
		  					<label>{{$ct}}</label>
		  					<span class="pull-right"><span class="glyphicon glyphicon-file" style="color:gray;"></span><strong>{{$peserta->FORM }}</strong></span>
		  				</div>
		  				<div class="col-xs-8 col-lg-3">
		  					<p><span class="glyphicon glyphicon-user" style="margin-right: 5px"></span>{{$peserta->NAMA }}</p>
		  				</div>

		  				<div class="col-xs-2 hidden-lg" style="color:gray;">
		  					<span class="pull-right">
		  						<span class="glyphicon glyphicon-file"></span>
		  						<strong>{{$peserta->FORM }}</strong>
		  					</span>	  					
		  				</div>

		  				<div class="col-xs-2 hidden-lg">
		  						<label class="pull-right ">{{$ct}}</label>
		  				</div>

		  				<div class="col-xs-6 col-lg-2" >
		  					<p><span class="glyphicon glyphicon-phone" style="margin-right: 5px"></span>{{ $peserta->PHONE }}</p>
		  				</div>
		  				<div class="col-xs-6  visible-xs visible-sm visible-md">

		  					<span class="pull-right">
				  				<span><a href="tel:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span></a></span>
				    			<span><a href="sms:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-envelope"></span></a></span>
				    			<span><a target="_blank" href="https://api.whatsapp.com/send?phone={{$wa_phone}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>	
				    			
							</span>
		  					
		  				</div>
		  				<div class="col-xs-10 col-lg-4">
		  					<p><span class="glyphicon glyphicon-envelope" style="margin-right: 5px"></span>{{$peserta->EMAIL }}</p>
		  				</div>

		  				<div class="col-xs-2 col-lg-2">
		  					<span class="visible-lg pull-left">
				  				<span><a href="tel:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span></a></span>
				    			<span><a href="sms:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-envelope"></span></a></span>
				    			<span><a target="_blank" href="https://api.whatsapp.com/send?phone={{$wa_phone}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>	
							</span>
		  					<span class="pull-right"><a target="_blank" href="{{ url('/') }}/edit?uuid={{$peserta->BARCODE}}" class="btn btn-primary btn-xs" role="button">
		  						<span class="glyphicon glyphicon-pencil"></span> Edit</a>
		  					</span>
		  				</div>
					
		    	</div>
		    	<?php $ct++;?>
		    	@endforeach
		  	</div>
		  <!-- /.box-body -->
		</div>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
@endsection

@section('script_body_main')
@endsection
