<?php
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
?>

@extends('frontend')

@section('css')
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">

	<!-- Select 2 -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">

	<link rel="stylesheet" href="/css/register.css">

	<script type="text/javascript">

		var formRules = {
			// gambar: "required",
			nama: {
				required: true,
				minlength: 4
			},
			printed_name: {
				required: true,
				maxlength: 20
			},
			ktp : {
				minlength: 16,
				maxlength: 16
			},
			email: {email: true},
			umat: "required",
			jk: "required",
			tgl_lahir: "required",
			alamat: "required",
			meal: "required",
			phone: {
				required: true,
				minlength: 6,
				// pattern: "[0-9\-\(\)\s]+"
			},
			emergency_cp: "required",
			emergency_phone: "required",
		};

		var formMessages = {
			// gambar: "Please take picture or upload image file",
			nama : {
				required: "Nama harus diisi / Please enter your complete name",
				minlength: "Nama harus setidaknya terdiri dari 4 karakter / Your name must consist of at least 4 characters",
				// pattern: "Terdiri atas, consist of: 0-0, (), +/-"
			},
			printed_name: {
				required: "Nama tercetak harus disi / Printed name must be filled",
				maxlength: "Nama tercetak maksimal 20 karakter / 20 Characters Max"
			},
			ktp : {
				minlength: "KTP harus terdiri atas 16 angka",
				maxlength: "KTP harus terdiri atas 16 angka"
			},
			email: "Please enter a valid email address",
			umat: "Pilih salah satu / Please pick one of the options",
			jk: "Pilih salah satu / Please pick one of the options",
			tgl_lahir: "Wajib di isi/ Required",
			alamat: "Wajib di isi/ Required",
			meal: "Pilih salah satu / Please pick one of the options",
			phone : {
				required: "harus diisi / required",
				minlength: "Minimal 6 digit / should be at least 6 digits"
			},
			emergency_cp: "Wajib di isi/ Required",
			emergency_phone: "Wajib di isi/ Required",
		};
	</script>
@endsection


@section('content')
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
@endsection

@section('script_body')
	<!-- datepicker -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	
	<!-- bootstrap time picker -->
	<script type="text/javascript" src="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

	<!-- Select2 -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/select2/dist/js/select2.min.js')}}"></script>

	<!-- jquery validation 1.17 -->
	<script type="text/javascript" src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>

	<!-- <script type="text/javascript" src="js/exif.js"></script> -->
	<script type="text/javascript" src="js/register.js"></script>
@endsection

@section('script_body_main')
	<script type="text/javascript">
		// //alert('b_'+kode)
		$(function(){
		  $('form').validate( {
				ignore:[],
				rules: formRules,
				messages: formMessages,
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox") {
						error.insertAfter( element.parent( "label" ) );
					} 
					else if (element.prop( "type" ) === "radio"){
						error.insertAfter( element.parents('.col-xs-12').find("label").first() );
					}
					else if ((element.hasClass( "datepicker")) || ((element.prop( "type" ) === "email")) || (element.attr('name') === "phone") || (element.attr('name') === "gambar") ){
						error.appendTo(element.parents('.col-xs-12'));
					}
					else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-xs-12" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-xs-12" ).removeClass( "has-error" );
				},
				submitHandler: function(form) {
	    		if ((!photoExist)  && $('#gambar')[0].files.length == 0)
					{
						modalShow("Submitting form","Bagian foto perlu dimasukkan / Please take a picture or upload from file");
					}
					else if ($('#kebangsaan').val() == "")
					{
						modalShow("Submitting form","Asal Negara wajib diisi / Pleaase fill Nationality");
					}
					else if ( ($('#tgl_lahir').val()!= "") && ( isNaN(parseDMY(($('#tgl_lahir').val())))  ) )
					{
						modalShow("Submitting form","Format Tanggal lahir salah / Wrong date of birth format");
					}
					else if ( ($('#ktp').val() == "")  &&  ($('#passport').val() == "") )
					{
						modalShow("Submitting form","KTP atau passport wajib disi / Please fill KTP or Passport");
					}
					else
					{
		    		checkUniqueID().done(function(data) {
							var dt = JSON.parse(data);
							if (dt.result == 1)
							{
								modalShow("Submitting form","Silakan tunggu sebentar / Please wait a moment");
								form.submit();
							}
							else
								modalShow("Error",dt.message);
						}).fail(function(data) {
							modalShow("Error","Gagal mengvalidasi apakah KTP atau passport sudah diperugnakan/  Fail to verify whether KTP and/or Passport has not been used before");
						});
				  }
				},
			});
		});
	</script>
@endsection