@extends('frontend.registerBase')

@section('title', 'Edit Registration E')

@section('content-header')
	{{-- Content Header for Registration E --}}
	@include('frontend.components.contentHeader',[
		'headerTitle' => 'Edit Registrasi/ <i style="color: gray">Registration Edit E</i>',
		'currentPage' => 'Edit E'
	])
@endsection

@section('content')
	<section class="content">
		@parent

		<!-- novalidate="novalidate" -->
		<form class="form" method="post" action="postEdit"  enctype="multipart/form-data" autocomplete="off">
			{{ csrf_field() }}
			<input type="hidden" id="uuid" name="uuid" value="{{$data->barcode}}">

			{{-- Basic Info --}}
			@include('frontend.components.basicInfo',[
				'id_peserta'     			=> $data->ID_PESERTA,
				'name' 								=> $data->nama,
				'printed_name' 				=> $data->printed_name,
				'umat' 								=> $data->umat,
				'jk' 									=> $data->jk,
				'organisasi' 					=> $data->organisasi,
				'jabatan' 						=> $data->jabatan,
				'tgl_lahir_dmy'   		=> date('d-m-Y', strtotime($data->tgl_lahir)),
				'indonesian_only' 		=> false,
				'kebangsaan' 					=> $data->kebangsaan,
				'ktp' 								=> $data->ktp,
				'passport' 						=> $data->passport,
				'alamat' 							=> $data->alamat,
				'meal'   							=> $data->meal,
				'phone' 							=> $data->phone,
				'email' 							=> $data->email,
				'photoExist'					=> file_exists(public_path('ImageThumb/').'/'.$data->ID_PESERTA.'.jpg'),
				'photoValidated'  		=> $data->photo_validated,
			])

			<!-- Event Schedule -->
			@include('frontend.components.eventScheduleE')

			{{-- Emergency Contact --}}
			@include('frontend.components.emergencyContact',[
				'emergency_cp' 		=> $data->emergency_cp,
				'emergency_phone' => $data->emergency_phone,
				'emergency_email' => $data->emergency_email,
			])

			{{-- Submit Changes --}}
			@if ($data->can_edit == 1)
				<div class="row" style="text-align:center; height: 100px; ">
					<div class="col-xs-12">
						<input type="submit" id="id_complete" class="btn btn-primary btn-lg" style="width: 200px;" value="Save Changes">
					</div>
				</div>
			@else
				<div class="row" style="text-align:center; height: 100px; ">
					<p>Batas waktu ataupun batas kesempatan mengubah data telah lewat. Nametag akan dicetak sesuai data yang tertera diatas / Time limit exceed or edit chance is reaching max, Name tag will be printed with the data your provided above </p>
				</div>
			@endif

		</form>

	</section>
@endsection