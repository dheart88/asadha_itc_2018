<?php
	$recruiter = false;
	if (property_exists ($data, 'uuid_recruiter' ))
		$recruiter = true;
		
	//dd($data);
?>

@extends('frontend')

@section('title', 'Registration Page - Upasika Atthasila</i>')

@section('css')
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">

	<!-- Select 2 -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">

	<link rel="stylesheet" href="/css/register.css">
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi
			  <small>Khusus Untuk Upasaka Upasika Atthasila</i></small>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          @if($recruiter)
          <li><a href="{{ url('/') }}/registerGroup?uuid={{$data->uuid_recruiter}}">Group Registration</a></li>
          @else
          <li><a href="{{ url('/') }}">Registration</a></li>
          @endif
          <li><a class="active">Form D</a></li>
        </ol>
	</section>
@endsection



@section('content')
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

			</div>
		</div>

		<!-- novalidate="novalidate" -->
		<form class="form" name="formRegister" id="formRegister" method="post" action="{{ url('/') }}/postRegistration" enctype="multipart/form-data" autocomplete="off">
		{{ csrf_field() }}
		<input type="hidden" name="form" id="form" value="D">
		<input type="hidden" name="program" value="0"> 
		<input type="hidden" name="kebangsaan" value="Indonesia">
		
		@if($recruiter)
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Data Kepala Kelompok</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		   
		    	<div class="form-group row">
		    		<input type="hidden" name="uuid_recruiter" value="{{$data->uuid_recruiter}}">
		    		
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KELOMPOK</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->nama_recruiter}}<p>
		    		</div>

		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KEPALA</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->cp_recruiter}} - {{$data->hp_recruiter}}</p>
		    			<span><a href="tel:{{$data->hp_recruiter}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> Call</a></span>
		    			<span><a href="sms:{{$data->hp_recruiter}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> SMS</a></span>
		    			<span><a href="https://api.whatsapp.com/send?phone={{substr($data->hp_recruiter,1)}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>
		    		</div>

		    		@if($data->cp_recruiter2!=null)
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KEPALA2</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->cp_recruiter2}} - {{$data->hp_recruiter2}}</p>
		    			<span><a href="tel:{{$data->hp_recruiter2}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> Call</a></span>
		    			<span><a href="sms:{{$data->hp_recruiter2}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> SMS</a></span>
		    			<span><a href="https://api.whatsapp.com/send?phone={{substr($data->hp_recruiter2,1)}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>
		    		</div>
		    		@endif

		    		@if($data->email_recruiter!=null)
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>EMAIL</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->email_recruiter}}</p>
		    		</div>
		    		@endif
		    	</div>	
		    </div>
		@endif
		
		<div class="box box-default">
			<div class="box-header with-border">
				@if(($data->getDCount >= $data->kuota) && ($recruiter == false))
				<div class="alert alert-warning alert-dismissible">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <h4><i class="icon fa fa-warning"></i> Perhatian</h4>
	                Saat ini jumlah peserta sudah mencapai jumlah maksimum. Data Anda masuk ke Daftar Tunggu (Waiting List) dan kami akan menginformasikan ketersediaan tempat tanggal 15 Juni nanti.
              	</div>
              	@endif

				<h3 class="box-title">Data Diri</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		    	<div class="form-group row">
		    		<!-- Foto -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">
		    			<label for="gambar" class="cameraButton">Take a picture
					    	<input type="file" name="gambar" id="gambar" accept="image/*;capture=camera" onchange="readURL(this);">
					 	</label>

					 	<div class="panel panel-default">
							<img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
			    			<!-- <canvas id="myCanvas">No Canvas Support</canvas> -->
			    		</div>
		    		</div>
		    	</div>

		    	<div class="form-group row">
		    		<!-- NAMA -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">

      					<label for="nama" class="control-label">Nama Lengkap</label>
      					<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama / Enter Your Name" maxlength="50" size="50">
  					</div>
  				</div>

                <div class="form-group row">
  				    <!-- PRINTED NAME -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="printed_name">Nama Tercetak/ <i style="color: gray">Printed Name</i></label>
      					<input type="text" class="form-control" id="printed_name" name="printed_name" placeholder="Maksimum 20 karakter / 20 chars max" maxlength="20" size="20" style="text-transform: uppercase;">
  					</div>
  				</div>
  				
		    	<!-- <div class="clearfix"></div> -->
				<div class="form-group row">
		    		<!-- UMAT -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
		    			<label for="umat">Saya adalah / <i style="color: gray">I'm a</i></label>

							<div class="radio">
								<label><input type="radio" name="umat" value="0" >Bhikkhu / <i style="color: gray">Monk</i></label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="1">Samanera</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="2">Anagarini / <i style="color: gray">Atthasilani</i></label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="3">Umat / <i style="color: gray">Lay People</i></label>
							</div>
						</div>

					<!-- JENIS KELAMIN -->
					<div class="col-xs-12 col-sm-6 col-lg-4">
		    			<label for="jk">Jenis Kelamin</label>
        				<div class="radio">
							<label><input type="radio" name="jk" value="M" >Pria</label>
						</div>

						<div class="radio">
							<label><input type="radio" name="jk" value="F">Wanita</label>
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group row">
					<!-- Organisasi -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="organisasi">Organisasi / Vihara</label>
      					<input type="text" class="form-control" name="organisasi" id="organisasi" placeholder="wajib diisi - organisasi/vihara" maxlength="40" size="40">
  					</div>

					<!-- Jabatan -->
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="jabatan">Jabatan</label>
      					<input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="boleh kosong" maxlength="40" size="40" >
  					</div>
				</div>

				<div class="form-group row">
					<!-- tanggal lahir  -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="tgl_lahir">Tanggal Lahir</label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" >
						</div> 

  					</div>
				</div>

				<div class="form-group row">
					<!-- KTP -->
					<div class="col-xs-12 col-sm-6 col-lg-4">
						<label for="ktp">KTP</label>
						<input type="number" pattern="[0-9]*" min="0" max="9999999999999999" inputmode="numeric" class="form-control" name="ktp" id="ktp" maxlength="16" size="16" placeholder="kosong jika tidak memiliki KTP">
					</div>
				</div>

				<div class="form-group row">
					<!-- Alamat -->
		    		<div class="col-xs-12 col-lg-9">
      					<label for="alamat">Alamat</label>
      					<textarea id="alamat" name="alamat" class="form-control" rows="4" data-limit-rows="true" maxlength="120" placeholder="Enter..." style="resize:none;width: 100%;"></textarea>
  					</div>
				</div>

				<div class="form-group row">
					<div class="col-xs-12 col-sm-6 col-lg-5">
						<label for="phone">No Telp/ HP</label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="glyphicon glyphicon-phone"></i>
							</div>
	      					
							<input type="text" name="phone" id="phone" class="form-control" maxlength="20" size="20" placeholder="contoh/ex : +62812345678 / 021-1111111">
						</div>
  					</div>
  				</div>

				<div class="form-group row">
					<!-- Email  -->
		    		<div class="col-xs-12 col-lg-9">
      					<label for="email" class="control-label">Email</label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="glyphicon glyphicon-envelope"></i>
							</div>
							<input type="email" class="form-control" id="email" name="email" placeholder="Boleh kosong" maxlength="60" size="60">
						</div>
  					</div>
				</div>

				<!-- JENIS KELAMIN -->
				<div class="form-group row">
					<div class="col-xs-12 col-sm-6 col-lg-4">
		    			<label for="meal">Makanan</label>
	    				<div class="radio">
							<label><input type="radio" name="meal" value="0">Vegetarian</label>
						</div>

						<div class="radio">
							<label><input type="radio" name="meal" value="1">Non Vegetarian</label> </div>
					</div>
				</div>
		  	</div>
		  <!-- /.box-body -->
		</div>


		@if(!$recruiter)
		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Detil Acara</small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
				<div class="form-group row">
					<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<p>Registrasi ini diperuntukkan untuk peserta yang mengikut keseluruhan acara, sehingga peserta di wajibkan <strong> mengikuti keseluruhan 20-22 Juli 2018</strong> Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja, peserta yang hanya akan mengikuti Asalha Mahapuja harap datang langsung ke Candi Mendut pada tgl 22-7-2018 pkl. 14.00 dan tidak perlu melakukan registrasi ini</p>
						<strong>Dengan mengikuti seluruh acara Panitia akan menyediakan</strong>
					  	<ul>
					  		<li>Tiket masuk Borobudur sesuai jadwal acara</li>
							<li>Makan 2x sehari sesuai jadwal (sarapan di penginapan, makan siang di area ITC)</li>
							<li>Air minum di lokasi ITC</li>
							<li>Transportasi dengan bus AC dari hotel ke Borobudur PP.</li>
						</ul>
					</div>
				</div>
		  	</div>

		</div>
		@endif

		
		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Kontak Darurat/<i style="color: gray">Emergency Contact</i><br>
					<small> <i class="fa fa-exclamation-triangle" style="color:tomato;"></i> Mohon diisi nama orang lain selain anda, jika terjadi sesuatu yang darurat kami akan menghubungi kontak ini/ Please fill it with emergency contact</small>
				</h3>
			</div>

	  	<div class="box-body" style="margin-left: 10px;">
	  		<div class="form-group row">
				<!--  -->
	    		<div class="col-xs-12 col-sm-6 col-lg-5">
    					<label for="emergency_cp">Nama</label>
    					<input type="text" class="form-control" id="emergency_cp" name="emergency_cp"  maxlength="40" size="40" placeholder="In case of emergency please contact">
    				</div>
			</div>

			<div class="form-group row">
				<!-- KTP -->
	    		<div class="col-xs-12 col-sm-6 col-lg-5">
    					<label for="emergency_phone">No Telp/HP</label>
    					<input type="text" class="form-control" id="emergency_phone" name="emergency_phone" maxlength="35" size="35" placeholder="required">
					</div>
				</div>

				<div class="form-group row">
					<!-- Passport -->
	    		<div class="col-xs-12 col-sm-6 col-lg-5">
    					<label for="emergency_email">Email</label>
    					<input type="email" class="form-control" id="emergency_email" name="emergency_email" maxlength="60" size="60" placeholder="boleh kosong / Optional">
					</div>
				</div>
	  	</div>
		</div>

		@if(!$recruiter)
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Pernyataan/<i style="color: gray">Agreement</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  		<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted1" style="font-weight: normal;"><input id="accepted1" type="checkbox">Berusia lebih dari 15 Tahun</label>
      				</div>
				</div>
				<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted2" style="font-weight: normal;"><input id="accepted2" type="checkbox">Mengikuti Tipitaka Chanting dan Asalha Puja sesuai jadwal acara.</label>
      				</div>
				</div>
				<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted3" style="font-weight: normal;"><input id="accepted3" type="checkbox">Pakaian putih (atas) hitam (bawah) atau putih-putih selama acara</label>
      				</div>
				</div>
				<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        <label for="accepted4" style="font-weight: normal;"><input id="accepted4" type="checkbox">Wajib melaksanakan atthasila pada tgl 20-22 Juli 2018.</label>
      				</div>
				</div>
				<div class="row" style="text-align:center;">
					<div class="col-xs-12">
						<input type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Register" disabled>
					</div>
				</div>

		  	</div>
		</div>
		@endif

		@if($recruiter)
		<center><input style="margin-bottom: 50px;" type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Register"></center>
		@endif
		</form>

	<!-- /.box -->
	</section>


	
@endsection

@section('script_body')
	<!-- datepicker -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	
	<!-- bootstrap time picker -->
	<script type="text/javascript" src="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

	<!-- Select2 -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/select2/dist/js/select2.min.js')}}"></script>

	<!-- jquery validation 1.17 -->
	<script type="text/javascript" src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>

	<!-- <script type="text/javascript" src="js/exif.js"></script> -->
@endsection

@section('script_body_main')
	<script type="text/javascript">

		// //alert('b_'+kode)
		$(function(){
		    $(".select_placeholder").select2({ width: '100%', placeholder:"Pilih / Please Select" });

			$.ajax({
				url: "{{ url('/') }}/country.json",
				cache: false,
				dataType: 'json',
				success: function(json){
					var dt = [];
					if( Object.prototype.toString.call( json ) === '[object Array]' ) {
						for (var i = 0; i < json.length; i++) {
							var obj = {};
							var name = json[i];
							obj['id'] = name;
							obj['text'] = name;
							dt.push(obj);
						}

						$('#kebangsaan').select2({
						  	data:dt, 
						  	width: '100%', 
						  	placeholder:"Pilih / Please Select",
						  	allowClear:true,
						});
					}
				}
			});

			$('.js-data-example-ajax').select2({
				ajax: {
					url: 'https://api.github.com/search/repositories',
					dataType: 'json'
				},
			});

			$('.timepicker').timepicker({
		    	showInputs: false,
		    	maxHours:24,
		    	showMeridian:false,
		    	defaultTime:false,
		    });

			$(".datepicker").datepicker().on('changeDate', function(ev) {
	            $(this).valid();  
	            // triggers the validation test
	            // '$(this)' refers to '$("#datepicker")'
	        });
			
			@if($recruiter)
			var showQuota = {{$data->getDCount >= $data->kuota ?'true':'false'}};
			if (showQuota)
				modalShow("Pengumuman","Pendaftaran Khusus Untuk Upasaka Upasika Atthasila Nasional sementara dibatasi, saat ini peserta kategori ini telah penuh dan pendaftaran anda akan masuk dalam list cadangan. Panitia akan menghubungi paling lambat 15 Juni untuk kepastian tempat.");
		    @endif
		});

		$( document ).ready( function () {
			$('form').validate( {
				ignore:[],
				rules: {
					//gambar: "required",
					nama: {
						required: true,
						minlength: 4
					},
					printed_name: {
						required: true,
						maxlength: 20
					},
					email: {email: true},
					umat: "required",
					jk: "required",
					organisasi: "required",
					tgl_lahir: "required",
					alamat: "required",
					meal: "required",
					phone: {
						required: true,
						minlength: 6,
					},
					emergency_cp: "required",
					emergency_phone: {
						required: true,
						minlength: 6,
					},
				},
				messages: {
					//gambar: "Please take picture or upload image file",
					nama : {
						required: "Nama harus diisi / Please enter your complete name",
						minlength: "Nama harus setidaknya terdiri dari 4 karakter / Your name must consist of at least 4 characters"
					},
					printed_name: {
						required: "Nama tercetak harus disi / Printed name must be filled",
						maxlength: "Nama tercetak maksimal 20 karakter / 20 Characters Max"
					},
					email: "Please enter a valid email address",
					umat: "Pilih salah satu / Please pick one of the options",
					jk: "Pilih salah satu / Please pick one of the options",
					organisasi: "Wajib di isi/ Required",
					tgl_lahir: "Wajib di isi/ Required",
					alamat: "Wajib di isi/ Required",
					meal: "Pilih salah satu / Please pick one of the options",
					phone : {
						required: "harus diisi / required",
						minlength: "Minimal 6 digit / should be at least 6 digits"
					},
					emergency_cp: "Wajib di isi/ Required",
					emergency_phone : {
						required: "harus diisi / required",
						minlength: "Minimal 6 digit / should be at least 6 digits"
					},
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox") {
						error.insertAfter( element.parent( "label" ) );
					} 
					else if (element.prop( "type" ) === "radio"){
						error.insertAfter( element.parents('.col-xs-12').find("label").first() );
					}
					else if ((element.hasClass( "datepicker")) || ((element.prop( "type" ) === "email")) || (element.attr('name') === "phone") || (element.attr('name') === "gambar")){
						error.appendTo(element.parents('.col-xs-12'));
					}
					else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-xs-12" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-xs-12" ).removeClass( "has-error" );
				},
				submitHandler: function(form) {
				    var ph = $('#phone').val();
				    var e_ph = $('#emergency_phone').val();
				    
				    var filter = /^[0-9-+]+$/;
                    if (!filter.test(ph)) {
                        modalShow("Submitting form","No Telp atau HP harus angka, karakter + atau - tanpa spasi / Phone number consists only digit,'+' and '-'");
                        // focusTo('#phone');
                    }
                    else if (!filter.test(e_ph)) {
                        modalShow("Submitting form","No Telp atau HP darurat harus angka, karakter + atau - tanpa spasi / Emergency phone number consists only digit,'+' and '-'");
                    }
					else if ($('#gambar')[0].files.length == 0)
					{
						modalShow("Submitting form","Bagian foto perlu dimasukkan / Please take a picture or upload from file");
					}
					else if ($('#kebangsaan').val() == "")
					{
						modalShow("Submitting form","Asal Negara wajib diisi / Pleaase fill Nationality");
					}
					else if ( ($('#tgl_lahir').val()!= "") && ( isNaN(parseDMY(($('#tgl_lahir').val())))  ) )
					{
						modalShow("Submitting form","Format Tanggal lahir salah / Wrong date of birth format");
					}
					else
					{
					    @if(!$recruiter)
		    			var overQuota = {{$data->getDCount >= $data->kuota  ?'true':'false'}}
						if (overQuota)
							modalShow("Submitting form","Pendaftaran ini akan masuk dalam list cadangan, silakan tunggu sebentar");
						else
		    				modalShow("Submitting form","Silakan tunggu sebentar / Please wait a moment");
					    @endif
					    form.submit();
				    }
				},
			});
		});
		
// 		function focusTo(toElement)
// 		{
//     		$('html, body').animate({
//                 scrollTop: $(toElement).offset().top
//             }, $speed);
            
//             if (toElement) 
//                 $(toElement).focus();
//         }
        
		function parseDMY(value) {
		    var dt = value.split("-");
		    var d = parseInt(dt[0], 10),
		        m = parseInt(dt[1], 10),
		        y = parseInt(dt[2], 10);
		    return new Date(y, m - 1, d);
		}

		$('#tgl_lahir').datepicker({
			autoclose: true,
			startDate: new Date("01/01/1900"),
		    endDate: new Date("07/21/2006"),
		    startView:2,
			format: 'dd-mm-yyyy'
	    });


		$('textarea[data-limit-rows=true]').on('keypress', function (event) {
	        var textarea = $(this),
	            text = textarea.val(),
	            numberOfLines = (text.match(/\n/g) || []).length + 1,
	            maxRows = parseInt(textarea.attr('rows'));

	        if (event.which === 13 && numberOfLines === maxRows ) {
	          return false;
	        }
	    });

	    $('#kebangsaan').change(function() {
		  	var ktpEnabled = ($('#kebangsaan').val().toLowerCase() == 'indonesia')
		  	$("#ktp").prop('disabled', !ktpEnabled);
		});

	    function isEmail(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
		}

		function modalShow(titleHeader,message)
		{
			$('#myModal').modal('show');
			$(".modal-title").text(titleHeader);
			$(".modal-body").text(message); 
		}

		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
	                $('#myImg')
	                    .attr('src', e.target.result);
	                    // .width(150)
	                    // .height(200);
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
	    }

		$('#accepted1,#accepted2,#accepted3,#accepted4').click(function () {
	  		if ($('#accepted1:checked,#accepted2:checked,#accepted3:checked,#accepted4:checked').length == 4)
				$('#id_complete').removeAttr('disabled');
			else
				$('#id_complete').attr('disabled','disabled');
		});

	</script>
@endsection
