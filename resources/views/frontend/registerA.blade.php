@extends('frontend')

@section('title', 'Registration Page -  Undangan Khusus /  Special Invitation')


@section('css')
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">

	<!-- Select 2 -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">

	<style>
		label.cameraButton {
		display: inline-block;
		margin: 1em 0;

		/* Styles to make it look like a button */
		padding: 0.5em;
		border: 2px solid #666;
		border-color: #EEE #CCC #CCC #EEE;
		background-color: #DDD;
		}

		/* Look like a clicked/depressed button */
		label.cameraButton:active {
		border-color: #CCC #EEE #EEE #CCC;
		}

		/* This is the part that actually hides the 'Choose file' text box for camera inputs */
		label.cameraButton input[accept*="camera"] {
		display: none;
		}

		.btn.btn-primary[disabled] {
		    background-color: #888888;
		}

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}

		/*.datepicker table tr td :not(.disabled).day {
		   background-color: green;
		}*/
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi/
			<i style="color: gray">Registration</i>
			  <small>Untuk Tamu Undangan/ <i style="color: gray">Special Invitation</i></small>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a href="./">Registration</a></li>
          <li><a class="active">Form A</a></li>
        </ol>
	</section>
@endsection

@section('content')
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

			</div>
		</div>

		<!-- novalidate="novalidate" -->
		<form class="form" name="formRegister" id="formRegister" method="post" action="postRegistration" enctype="multipart/form-data" autocomplete="off">
		{{ csrf_field() }}
		<input type="hidden" name="form" id="form" value="A">
		<input type="hidden" name="program" id="program" value="0">
		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Data Diri / <i style="color: gray">Identity</i></h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		   
		    	<div class="form-group row">
		    		<!-- Foto -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">
		    			<label for="gambar" class="cameraButton">Take a picture
					    	<input type="file" name="gambar" id="gambar" accept="image/*;capture=camera" onchange="readURL(this);">
					 	</label>

					 	<div class="panel panel-default">
							<img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
			    			
			    		</div>
			    		<p align="center"><small>rotasi gambar akan diperbaiki ketika gambar diupload / image rotation will be fixed when it's uploaded</small></p>
		    		</div>
		    	</div>

		    	<div class="form-group row">
		    		<!-- NAMA -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">

      					<label for="nama">Nama / <i style="color: gray">Name</i></label>
      					<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama / Enter Your Name" maxlength="50" size="50" style="text-transform: uppercase;">
  					</div>
  				</div>

		    		<!-- <div class="clearfix"></div> -->

				<div class="form-group row">
		    		<!-- UMAT -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
		    			<label for="umat">Saya adalah / <i style="color: gray">I'm a</i></label>

							<div class="radio">
								<label><input type="radio" name="umat" value="0" >Bhikkhu / <i style="color: gray">Monk</i></label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="1">Samanera</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="2">Anagarini / <i style="color: gray">Atthasilani</i></label>
							</div>
							<div class="radio">
								<label><input type="radio" name="umat" value="3">Umat / <i style="color: gray">Lay People</i></label>
							</div>
						</div>

					<!-- JENIS KELAMIN -->
					<div class="col-xs-12 col-sm-6 col-lg-4">
		    			<label for="jk">Jenis Kelamin / <i style="color: gray">Gender</i></label>
        				<div class="radio">
							<label><input type="radio" name="jk" value="M">Pria / <i style="color: gray">Male</i></label>
						</div>

						<div class="radio">
							<label><input type="radio" name="jk" value="F">Wanita / <i style="color: gray">Female</i></label>
						</div>
						 <!-- <select class="form-control select_placeholder" name="jk" id="jk">
							<option></option>
							<option value="L">Pria / Male</option>
							<option value="P">Wanita / Female</option>
						</select> -->

					</div>
				</div>

				<hr>

				<div class="form-group row">
					<!-- Organisasi -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="organisasi">Organisasi / <i style="color: gray">Organization</i></label>
      					<input type="text" class="form-control" name="organisasi" id="organisasi" placeholder="boleh kosong" maxlength="40" size="40">
  					</div>

					<!-- Jabatan -->
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="jabatan">Jabatan / <i style="color: gray">Designation</i></label>
      					<input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="boleh kosong" maxlength="40" size="40">
  					</div>
				</div>

				<div class="form-group row">
					<!-- tanggal lahir  -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="tgl_lahir">Tanggal Lahir / <i style="color: gray">Date Of Birth (dd-mm-yyyy)</i></label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir">
						</div> 

  					</div>
				</div>

				<div class="form-group row">
					<!-- Asal Negara -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="kebangsaan">Asal Negara / <i style="color: gray">Nationality</i></label>
      					<select class="form-control" name="kebangsaan" id="kebangsaan">
							<option></option>
						</select>
						<!-- <select class="js-data-example-ajax"></select> -->
  					</div>

					<!-- KTP -->
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="ktp">KTP (<i style="color: gray">Indonesian Only</i>)</label>
      					<input type="number" class="form-control" name="ktp" id="ktp" maxlength="40" size="40" disabled="disabled" placeholder="kosong jika tidak memiliki KTP">
  					</div>

  					<!-- Passport -->
		    		<div class="col-xs-12 col-sm-6 col-sm-offset-6 col-lg-4 col-lg-offset-5">
      					<label for="passport">Passport / <i style="color: gray">Passport</i></label>
      					<input type="text" class="form-control" name="passport" id="passport"  maxlength="40" size="40" placeholder="boleh kosong">
  					</div>

				</div>

				<div class="form-group row">
					<!-- Alamat -->
		    		<div class="col-xs-12 col-lg-9">
      					<label for="alamat">Alamat / <i style="color: gray">Full Address</i></label>
      					<textarea id="alamat" name="alamat" class="form-control" rows="4" data-limit-rows="true" maxlength="120" placeholder="Enter..." style="resize:none;width: 100%;"></textarea>
  					</div>
				</div>

				<div class="form-group row">
					<div class="col-xs-12 col-sm-6 col-lg-5">
						<label for="phone">No Telp/ HP <i style="color: gray">Phone / Mobile Phone</i></label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="glyphicon glyphicon-phone"></i>
							</div>
	      					
							<input type="text" name="phone" id="phone" class="form-control" maxlength="20" size="20" placeholder="contoh/ex : +62812345678 / 021-1111111">
						</div>
  					</div>
  				</div>

				<div class="form-group row">
					<!-- Email  -->
		    		<div class="col-xs-12 col-lg-9">
      					<label for="email">Email / <i style="color: gray">Email</i></label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="glyphicon glyphicon-envelope"></i>
							</div>
							<input type="email" class="form-control" id="email" name="email" placeholder="Boleh kosong" maxlength="60" size="60">
						</div>
  					</div>
				</div>

				<!-- JENIS KELAMIN -->
				<div class="form-group row">
					<div class="col-xs-12 col-sm-6 col-lg-4">
		    			<label for="meal">Makanan / <i style="color: gray">Meal</i></label>
	    				<div class="radio">
							<label><input type="radio" name="meal" value="0">Vegetarian</label>
						</div>

						<div class="radio">
							<label><input type="radio" name="meal" value="1">Non Vegetarian</label> </div>
					</div>
				</div>
		  	</div>
		  <!-- /.box-body -->
		</div>


		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Detil Acara/<i style="color: gray">Event Schedule</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  		<div class="form-group row">
		    		<!-- UMAT -->
		    		<div class="col-xs-12 col-lg-9">
		    			<label for="program">Acara Yang akan diikuti / <i style="color: gray">I will attend</i> </label>
		    			<p>20-22 Juli 2018 Tipitaka Chanting &amp;&nbsp;Asalha Mahapuja</p>
					</div>
				</div>

				<div class="row">						
					<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
					  	<label>Panitia akan menyediakan akomodasi / <i style="color: gray">Accomodation</i></label>
					  	<ul>
					  		<li>Antar jemput di Bandara Adi Sucipto Yogyakarta. <i style="color: gray">/Transportation airport - hotel</i></li>
							<li>Penginapan 4 malam di Hotel di Magelang (Check-in 19 Juli 2018 setelah Jam 14.00 dan Check-out 23 Juli 2018 sebelum Jam 12.00 siang) <i style="color: gray"> / Hotel Jul 19-23, 2018</i></li>
							<li>Tiket masuk Borobudur sesuai jadwal acara. <i style="color: gray"> / Entry Fee for Borobudur</i></li>
							<li>Makan 3x sehari sesuai jadwal (sarapan di hotel, makan siang dan makan malam di area ITC )<i style="color: gray"> / Breakfast at Hotel, lunch and dinner at ITC venue </i></li>
							<li>Air minum di lokasi ITC. <i style="color: gray">/ Drinking water at ITC Venue</i></li>
							<li>Transportasi dengan bus AC dari hotel ke Borobudur PP. <i style="color: gray">/ Bus with AC from hotel - Borobudur, vice versa</i></li>
						</ul>
					</div>
				</div>

		  	</div>
		</div>

		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Kedatangan/<i style="color: gray">Arrival</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">

				<div class="form-group row">
					<!-- Kedatangan -->
					<!-- Nama kereta atau pesawat / Flight or Train detail -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="arrival_transport">No Pesawat/Kereta/Bus <i style="color: gray">Flight/Train/Bus No</i></label>
						<input type="text" name="arrival_transport" id="arrival_transport" class="form-control" maxlength="40" size="40" placeholder="contoh/ex : AirAsia A123, KA Pasudan">
						<!-- <select class="js-data-example-ajax"></select> -->
  					</div>

					
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="arrival_from">Asal Kota <i style="color: gray">Origin</i></label>
						<input type="text" name="arrival_from" id="arrival_from" class="form-control" maxlength="40" size="40" placeholder="">
						<!-- <select class="js-data-example-ajax"></select> -->
  					</div>

  					<div class="clearfix"></div>

					<!-- Arrival Date -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="arrival_date">Tanggal Tiba / <i style="color: gray">Arrival Date</i></label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" name="arrival_date" class="form-control datepicker" id="arrival_date">
						</div>
  					</div>

					<!-- Arrival Date -->
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="arrival_time">Perkiraan Jam Tiba(<i style="color: gray">Estimated Arrival Time</i>)</label>
      					<div class="bootstrap-timepicker">
          					<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</div>
								<input type="text" name="arrival_time" class="form-control timepicker" id="arrival_time">
							</div>
						</div>
  					</div>
				</div>

		  	</div>
		</div>

		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Keberangkatan / <i style="color: gray">Departure</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">

				<div class="form-group row">
					<!-- Kedatangan -->
					<!-- Nama kereta atau pesawat / Flight or Train detail -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="departure_transport">No Pesawat/Kereta/Bus <i style="color: gray">Flight/Train/Bus No</i></label>
						<input type="text" name="departure_transport" id="departure_transport" class="form-control" maxlength="40" size="40" placeholder="contoh/ex : AirAsia A123, KA Pasudan">
						<!-- <select class="js-data-example-ajax"></select> -->
  					</div>

					
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="departure_to">Kembali Ke Kota <i style="color: gray">Departure to</i></label>
						<input type="text" name="departure_to" id="departure_to" class="form-control" maxlength="40" size="40" placeholder="">
						<!-- <select class="js-data-example-ajax"></select> -->
  					</div>

  					<div class="clearfix"></div>

					<!-- Arrival Date -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="departure_date">Tanggal Berangkat / <i style="color: gray">Departure Date</i></label>
      					<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" id="departure_date" name="departure_date">
						</div>
  					</div>

					<!-- Arrival Date -->
		    		<div class="col-xs-12 col-sm-6 col-lg-4">
      					<label for="departure_time">Jam Berangkat(<i style="color: gray">Departure Time</i>)</label>
      					<div class="bootstrap-timepicker">
          					<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</div>
								<input type="text" class="form-control timepicker" id="arrival_time" name="departure_time">
							</div>
						</div>
  					</div>
				</div>

		  	</div>
		</div>


		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Kontak Darurat/<i style="color: gray">Emergency Contact</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  		<div class="form-group row">
					<!--  -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="emergency_cp">Nama / <i style="color: gray">Name</i></label>
      					<input type="text" class="form-control" id="emergency_cp" name="emergency_cp"  maxlength="40" size="40" placeholder="In case of emergency please contact">
      				</div>
				</div>

				<div class="form-group row">
					<!-- KTP -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="emergency_phone">No Telp / <i style="color: gray">Phone Number</i></label>
      					<input type="text" class="form-control" id="emergency_phone" name="emergency_phone" maxlength="35" size="35" placeholder="required">
  					</div>
  				</div>

  				<div class="form-group row">
  					<!-- Passport -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="emergency_email">Email / <i style="color: gray">Email</i></label>
      					<input type="email" class="form-control" id="emergency_email" name="emergency_email" maxlength="60" size="60" placeholder="boleh kosong / Optional">
  					</div>
  				</div>
		  	</div>
		</div>

		<div class="box box-default">

			<div class="box-header with-border">
				<h3 class="box-title">Pernyataan/<i style="color: gray">Agreement</i></small>
				</h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  		<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted1" style="font-weight: normal;"><input id="accepted1" type="checkbox">Berusia lebih dari 15 Tahun / <i style="color: gray">At least 15 years Old</i></label>
      				</div>
				</div>
				<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted2" style="font-weight: normal;"><input id="accepted2" type="checkbox">Mengikuti Tipitaka Chanting dan Asalha Maha Puja sesuai jadwal acara. / <i style="color: gray">Follow Tipitaka Chanting and Asalha Maha Puja Schedule</i></label>
      				</div>
				</div>
				<div class="form-group row">
		    		<div class="col-xs-12 col-lg-9">
                        
                        <label for="accepted3" style="font-weight: normal;"><input id="accepted3" type="checkbox">Pakaian putih (atas) hitam (bawah) atau putih-putih selama acara / <i style="color: gray">Wear black and white or all white</i></label>
      				</div>
				</div>


  				<div class="row" style="text-align:center;">
					<div class="col-xs-12">
						<input type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Register" disabled>
					</div>
				</div>


		  	</div>
		</div>

		</form>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
	<!-- datepicker -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	
	<!-- bootstrap time picker -->
	<script type="text/javascript" src="{{asset('AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

	<!-- Select2 -->
	<script type="text/javascript" src="{{asset('AdminLTE/bower_components/select2/dist/js/select2.min.js')}}"></script>

	<!-- jquery validation 1.17 -->
	<script type="text/javascript" src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>

	<!-- <script type="text/javascript" src="js/exif.js"></script> -->
@endsection

@section('script_body_main')
	<script type="text/javascript">

		// //alert('b_'+kode)
		$(function(){
		    $(".select_placeholder").select2({ width: '100%', placeholder:"Pilih / Please Select" });

			$.ajax({
				url: "country.json",
				cache: false,
				dataType: 'json',
				success: function(json){
					var dt = [];
					if( Object.prototype.toString.call( json ) === '[object Array]' ) {
						for (var i = 0; i < json.length; i++) {
							var obj = {};
							var name = json[i];
							obj['id'] = name;
							obj['text'] = name;
							dt.push(obj);
						}

						$('#kebangsaan').select2({
						  	data:dt, 
						  	width: '100%', 
						  	placeholder:"Pilih / Please Select",
						  	allowClear:true,
						});

						$("#kebangsaan").val('Indonesia');
						$("#kebangsaan").trigger('change');
					}
				}
			});

			$('.js-data-example-ajax').select2({
				ajax: {
					url: 'https://api.github.com/search/repositories',
					dataType: 'json'
					// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				},
			});

			$('.timepicker').timepicker({
		    	showInputs: false,
		    	maxHours:24,
		    	showMeridian:false,
		    	defaultTime:false,
		    });

		    $(".datepicker").datepicker().on('changeDate', function(ev) {
	            $(this).valid();  // triggers the validation test
	            // '$(this)' refers to '$("#datepicker")'
	        });
	        
	  //       $('#accepted1').attr('checked','checked');
	  //       $('#accepted2').attr('checked','checked');
	  //       $('#accepted3').attr('checked','checked');
			// $('#id_complete').removeAttr('disabled');
		});

		$( document ).ready( function () {
			$('form').validate( {
				ignore:[],
				rules: {
					// gambar: "required",
					nama: {
						required: true,
						minlength: 4
					},
					email: {email: true},
					umat: "required",
					jk: "required",
					tgl_lahir: "required",
					alamat: "required",
					meal: "required",
					phone: {
						required: true,
						minlength: 6,
						// pattern: "[0-9\-\(\)\s]+"
					},
					emergency_cp: "required",
					emergency_phone: "required",
				},
				messages: {
					// gambar: "Please take picture or upload image file",
					nama : {
						required: "Nama harus diisi / Please enter your complete name",
						minlength: "Nama harus setidaknya terdiri dari 4 karakter / Your name must consist of at least 4 characters"
					},
					email: "Please enter a valid email address",
					umat: "Pilih salah satu / Please pick one of the options",
					jk: "Pilih salah satu / Please pick one of the options",
					tgl_lahir: "Wajib di isi/ Required",
					alamat: "Wajib di isi/ Required",
					meal: "Pilih salah satu / Please pick one of the options",
					phone : {
						required: "harus diisi / required",
						minlength: "Minimal 6 digit / should be at least 6 digits"
					},
					emergency_cp: "Wajib di isi/ Required",
					emergency_phone: "Wajib di isi/ Required",
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox") {
						error.insertAfter( element.parent( "label" ) );
					} 
					else if (element.prop( "type" ) === "radio"){
						error.insertAfter( element.parents('.col-xs-12').find("label").first() );
					}
					else if ((element.hasClass( "datepicker")) || ((element.prop( "type" ) === "email")) || (element.attr('name') === "phone") || (element.attr('name') === "gambar")){
						error.appendTo(element.parents('.col-xs-12'));
					}
					else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-xs-12" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-xs-12" ).removeClass( "has-error" );
				},
				submitHandler: function(form) {
	    			if ($('#gambar')[0].files.length == 0)
					{
						modalShow("Submitting form","Bagian foto perlu dimasukkan / Please take a picture or upload from file");
					}
					else if ($('#kebangsaan').val() == "")
					{
						modalShow("Submitting form","Asal Negara wajib diisi / Please fill Nationality");
					}
					else if ( ($('#tgl_lahir').val()!= "") && ( isNaN(parseDMY(($('#tgl_lahir').val())))  ) )
					{
						modalShow("Submitting form","Format Tanggal lahir salah / Wrong date of birth format");
					}
					else if ( ($('#arrival_date').val()!= "") && ( isNaN(parseDMY(($('#arrival_date').val())))  ) )
					{
						modalShow("Submitting form","Format Tanggal Kedatangan salah / Wrong arrival date format");
					}
					else if ( ($('#departure_date').val()!= "") && ( isNaN(parseDMY(($('#departure_date').val())))  ) )
					{
						modalShow("Submitting form","Format Tanggal Berangkat salah / Wrong departure date format ");
					}
					else
					{
		    			modalShow("Submitting form","Silakan tunggu sebentar / Please wait a moment");
					    form.submit();
				    }
				},
			});
		});

		function parseDMY(value) {
		    var dt = value.split("-");
		    var d = parseInt(dt[0], 10),
		        m = parseInt(dt[1], 10),
		        y = parseInt(dt[2], 10);
		    return new Date(y, m - 1, d);
		}

		$('#tgl_lahir').datepicker({
			autoclose: true,
			startDate: new Date("01/01/1900"),
		    endDate: new Date("07/21/2006"),
		    startView:2,
			format: 'dd-mm-yyyy'
	    });

		$('#arrival_date').datepicker({
		    startDate: new Date("07/17/2018"),
		    endDate: new Date("07/22/2018"),
		    maxViewMode: 0,
		    format: 'dd-mm-yyyy',
		});

		$('#departure_date').datepicker({
		    startDate: new Date("07/22/2018"),
		    endDate: new Date("07/24/2018"),
		    maxViewMode: 0,
		    format: 'dd-mm-yyyy',
		});
		

		$('textarea[data-limit-rows=true]').on('keypress', function (event) {
	        var textarea = $(this),
	            text = textarea.val(),
	            numberOfLines = (text.match(/\n/g) || []).length + 1,
	            maxRows = parseInt(textarea.attr('rows'));

	        if (event.which === 13 && numberOfLines === maxRows ) {
	          return false;
	        }
	    });

	    $('#kebangsaan').change(function() {
		  	var ktpEnabled = ($('#kebangsaan').val().toLowerCase() == 'indonesia')
		  	$("#ktp").prop('disabled', !ktpEnabled);
		});
	    
	    function isEmail(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
		}

		function modalShow(titleHeader,message)
		{
			$('#myModal').modal('show');
			$(".modal-title").text(titleHeader);
			$(".modal-body").text(message); 
		}

		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
	                $('#myImg')
	                    .attr('src', e.target.result);
	                    // .width(150)
	                    // .height(200);
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
	    }

		$('#accepted1,#accepted2,#accepted3').click(function () {
	  		if ($('#accepted1:checked,#accepted2:checked,#accepted3:checked').length == 3)
				$('#id_complete').removeAttr('disabled');
			else
				$('#id_complete').attr('disabled','disabled');
		});

		

	</script>
@endsection
