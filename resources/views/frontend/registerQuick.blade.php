@extends('frontend')

@section('title', 'Registration Page -  Quick Register')


@section('css')
	<!-- Select 2 -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">

	<style>
		label.cameraButton {
		display: inline-block;
		margin: 1em 0;

		/* Styles to make it look like a button */
		padding: 0.5em;
		border: 2px solid #666;
		border-color: #EEE #CCC #CCC #EEE;
		background-color: #DDD;
		}

		/* Look like a clicked/depressed button */
		label.cameraButton:active {
		border-color: #CCC #EEE #EEE #CCC;
		}

		/* This is the part that actually hides the 'Choose file' text box for camera inputs */
		label.cameraButton input[accept*="camera"] {
		display: none;
		}

		.btn.btn-primary[disabled] {
		    background-color: #888888;
		}

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi/
			<i style="color: gray">Registration</i>
			  <small>Cepat/ <i style="color: gray">Quick</i></small>
			</h3>
			<p align="center"><small>Hanya jika butuh cepat</small></p>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a href="{{ url('/') }}">Registration</a></li>
          <li><a class="active">Quick Registration</a></li>
        </ol>
	</section>
@endsection

@section('content')
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

			</div>
		</div>

		<!-- novalidate="novalidate" -->
		<form class="form" name="formRegister" id="formRegister" method="post" action="{{ url('/') }}/postQuickRegistration" enctype="multipart/form-data" autocomplete="off">
		{{ csrf_field() }}

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Data Diri / <i style="color: gray">Identity</i></h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		   
		    	<div class="form-group row">
		    		<!-- FOTO -->
		    		<div class="col-xs-12">
		    			<label for="gambar" class="cameraButton">Take a picture
					    	<input type="file" name="gambar" id="gambar" accept="image/*;capture=camera" onchange="readURL(this);">
					 	</label>

					 	<div class="panel panel-default">
							<img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
			    			
			    		</div>
			    		
		    		</div>
		    	</div>

		    	
  				
  				<div class="form-group row">
  				    <!-- PRINTED NAME -->
		    		<div class="col-xs-12">
      					<label for="printed_name">Nama Tercetak</label>
      					<input type="text" class="form-control" id="printed_name" name="printed_name" placeholder="Maksimum 20 karakter / 20 chars max" maxlength="20" size="20" style="text-transform: uppercase;">
  					</div>
  				</div>
  				
  				<div class="form-group row">
		    		<!-- ALAMAT -->
		    		<div class="col-xs-12">
      					<label for="alamat">Alamat</label>
      					<input type="text" class="form-control" id="alamat" name="alamat" placeholder="Asal" maxlength="50" size="50" style="text-transform: uppercase;">
  					</div>
  				</div>

				<div class="form-group row">
					<!-- FORM -->
		    		<div class="col-xs-12">
      					<label for="Form">Form</label>
      					<select class="form-control" name="form" id="form">
							<option value="A" selected>A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
						</select>
  					</div>
				</div>

                <div class="row" style="text-align:center;">
					<div class="col-xs-12">
						<input type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Register">
					</div>
				</div>
		  	</div>
		  <!-- /.box-body -->
		</div>
		
		</form>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
	<!-- jquery validation 1.17 -->
	<script type="text/javascript" src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
@endsection

@section('script_body_main')
	<script type="text/javascript">
		$( document ).ready( function () {
			$('form').validate( {
				ignore:[],
				rules: {
					printed_name: {
						required: true,
						maxlength: 20
					},
					form: "required",
				},
				messages: {
					printed_name: {
						required: "Nama tercetak harus disi / Printed name must be filled",
						maxlength: "Nama tercetak maksimal 20 karakter / 20 Characters Max"
					},
					form: "Wajib di isi/ Required",
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox") {
						error.insertAfter( element.parent( "label" ) );
					} 
					else if (element.prop( "type" ) === "radio"){
						error.insertAfter( element.parents('.col-xs-12').find("label").first() );
					}
					else if ((element.hasClass( "datepicker")) || ((element.prop( "type" ) === "email")) || (element.attr('name') === "phone") || (element.attr('name') === "gambar")){
						error.appendTo(element.parents('.col-xs-12'));
					}
					else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-xs-12" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-xs-12" ).removeClass( "has-error" );
				},
				submitHandler: function(form) {
	    			if ($('#gambar')[0].files.length == 0)
					{
						modalShow("Submitting form","Bagian foto perlu dimasukkan / Please take a picture or upload from file");
					}
					else if ($('#kebangsaan').val() == "")
					{
						modalShow("Submitting form","Asal Negara wajib diisi / Please fill Nationality");
					}
					else
					{
		    			modalShow("Submitting form","Silakan tunggu sebentar / Please wait a moment");
					    form.submit();
				    }
				},
			});
			
			$("#nama").keyup(function(event) {
              var stt = $(this).val();
              $("#printed_name").val(stt.substr(0, 20));
            });
		});
		
		function modalShow(titleHeader,message)
		{
			$('#myModal').modal('show');
			$(".modal-title").text(titleHeader);
			$(".modal-body").text(message); 
		}

		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
	                $('#myImg')
	                    .attr('src', e.target.result);
	                    // .width(150)
	                    // .height(200);
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
	    }
	</script>
@endsection
