<?php
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
// 	$recruiter = false;
// 	if (property_exists ($data, 'uuid_recruiter' ))
// 		$recruiter = true;

    // echo json_encode($data);
    // return;
    
    $hasImage = file_exists(public_path('ImageThumb/').'/'.$data->ID_PESERTA.'.jpg');
?>

@extends('frontend')

@section('title', 'Photo Edit')


@section('css')
	<!-- Select 2 -->
	<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">

	<style>
		label.cameraButton {
		display: inline-block;
		margin: 1em 0;

		/* Styles to make it look like a button */
		padding: 0.5em;
		border: 2px solid #666;
		border-color: #EEE #CCC #CCC #EEE;
		background-color: #DDD;
		}

		/* Look like a clicked/depressed button */
		label.cameraButton:active {
		border-color: #CCC #EEE #EEE #CCC;
		}

		/* This is the part that actually hides the 'Choose file' text box for camera inputs */
		label.cameraButton input[accept*="camera"] {
		display: none;
		}

		.btn.btn-primary[disabled] {
		    background-color: #888888;
		}

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Photo Edit <small>Buat Ganti foto cepat</small>
			</h3>
			<p align="center"><small>Foto yang terdata langsung dianggap valid dan siap cetak</small></p>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a href="{{ url('/') }}">Registration</a></li>
          <li><a class="active">Photo Edit</a></li>
        </ol>
	</section>
@endsection

@section('content')
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

			</div>
		</div>

		<!-- novalidate="novalidate" -->
		<form class="form" name="formRegister" id="formRegister" method="post" action="{{ url('/') }}/postQuickPhoto" enctype="multipart/form-data" autocomplete="off">
		{{ csrf_field() }}
		<input type="hidden" name="uuid" value="{{$data->barcode}}">

		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Data Diri / <i style="color: gray">Identity</i></h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		   
		    	<div class="form-group row">
		    		<!-- Foto -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">
		    			<label for="gambar" class="cameraButton">Take a picture
					    	<input type="file" name="gambar" id="gambar" accept="image/*;capture=camera" onchange="readURL(this);">
					 	</label>
					 	
					 	<div class="panel panel-default">
							
			    			@if($hasImage) 
							  <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{URL::to('/').'/ImageThumb'.'/'.$data->ID_PESERTA.'.jpg?'.time()}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
							@else
							  <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 300px; max-height: 300px; margin: auto;"/>
							@endif
							
			    		</div>
			    		@if($hasImage)
			    			@if($data->photo_validated == 1)
								<p id="photo_editted" align="center" style="font-style: oblique;"><small>foto ini telah siap dicetak pada nametag hub panitia jika ingin mengganti foto sebelum acara / we will put this photo on your nametag, please contact us if you want to replace it</small></p>
							@elseif($data->photo_validated == -1)
								<p id="photo_editted" align="center" style="font-style: oblique; color:red;"><small>foto telah review panitia dan sebaiknya anda mengupload foto terbaru yang cukup jelas / please reupload your photo</small></p>
							@endif
						@else
			    			<p align="center"><small>rotasi gambar akan diperbaiki ketika gambar diupload / Image will be rotate when uploaded</small></p>
			    		@endif
		    		</div>
		    	</div>

		    	<div class="form-group row">
		    		<!-- NAMA -->
		    		<div class="col-xs-12 col-sm-12 col-lg-9">
      					<label for="nama">Nama</label>
      					<input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" placeholder="Masukkan Nama / Enter Your Name" maxlength="50" size="50" style="text-transform: uppercase;">
  					</div>
  				</div>

                <div class="form-group row">
  				    <!-- PRINTED NAME -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="printed_name">Nama Tercetak</label>
      					<input type="text" class="form-control" id="printed_name" name="printed_name" value="{{$data->printed_name}}" placeholder="hingga 20 karakter/up to 20 chars" maxlength="25" size="25" style="text-transform: uppercase;">
  					</div>
  				</div>

				<div class="form-group row">
					<!-- FORM -->
		    		<div class="col-xs-12 col-sm-6 col-lg-5">
      					<label for="form">Form</label>
      					{{$data->form}}
  					</div>
				</div>

                <div class="row" style="text-align:center;">
					<div class="col-xs-12 col-sm-12 col-lg-9">
						<input type="submit" id="id_complete" class="btn btn-primary btn-lg" value="Update Photo & Validate">
					</div>
				</div>
		  	</div>
		  <!-- /.box-body -->
		</div>
		
		</form>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
	<!-- jquery validation 1.17 -->
	<script type="text/javascript" src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
@endsection

@section('script_body_main')
	<script type="text/javascript">
		$( document ).ready( function () {
			$('form').validate( {
				ignore:[],
				rules: {
					// gambar: "required",
					nama: {
						required: true,
						minlength: 4
					},
					printed_name: {
						required: true,
						maxlength: 20
					},
				},
				messages: {
					// gambar: "Please take picture or upload image file",
					nama : {
						required: "Nama harus diisi / Please enter your complete name",
						minlength: "Nama harus setidaknya terdiri dari 4 karakter / Your name must consist of at least 4 characters"
					},
					printed_name: {
						required: "Nama tercetak harus disi / Printed name must be filled",
						maxlength: "Nama tercetak maksimal 20 karakter / 20 Characters Max"
					},
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox") {
						error.insertAfter( element.parent( "label" ) );
					} 
					else if (element.prop( "type" ) === "radio"){
						error.insertAfter( element.parents('.col-xs-12').find("label").first() );
					}
					else if ((element.hasClass( "datepicker")) || ((element.prop( "type" ) === "email")) || (element.attr('name') === "phone") || (element.attr('name') === "gambar")){
						error.appendTo(element.parents('.col-xs-12'));
					}
					else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-xs-12" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-xs-12" ).removeClass( "has-error" );
				},
				submitHandler: function(form) {
	    			if ($('#gambar')[0].files.length == 0)
					{
						modalShow("Submitting form","Bagian foto perlu dimasukkan / Please take a picture or upload from file");
					}
					else if ($('#kebangsaan').val() == "")
					{
						modalShow("Submitting form","Asal Negara wajib diisi / Please fill Nationality");
					}
					else
					{
		    			modalShow("Submitting form","Silakan tunggu sebentar / Please wait a moment");
					    form.submit();
				    }
				},
			});
			
			$("#nama").keyup(function(event) {
              var stt = $(this).val();
              $("#printed_name").val(stt.substr(0, 20));
            });
		});
		
		function modalShow(titleHeader,message)
		{
			$('#myModal').modal('show');
			$(".modal-title").text(titleHeader);
			$(".modal-body").text(message); 
		}

		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
	                $('#myImg')
	                    .attr('src', e.target.result);
	                    // .width(150)
	                    // .height(200);
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
	    }
	</script>
@endsection
