@extends('frontend')

@section('title', 'Registration Completed')

@section('css')
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Registrasi/
			<i style="color: gray">Registration</i>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Return To Main Site</a></li>
        </ol>
	</section>
@endsection

@section('content')
	<section class="content">
		<div class="box box-default" style="min-height: 500px;">
		  	<div class="box-body" style="margin-left: 10px;">
<<<<<<< HEAD:resources/views/frontend/successEdit.blade.php

		  		@isset($data->uuid)
		    		<p>Perubahan Data Berhasil.</p>
		    		<hr>
		    		<center><h3>{{strtoupper($data->uuid)}}</h3></center>
						<hr>
		    		<p>atau melalui link jika akan melakukan perubahan data lagi</p>
		    		<form>
							<div class="input-group">
								<input type="text" onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
								    value="https://register.asalhapuja.or.id/edit?uuid={{$data->uuid}}" id="copy-input">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="copy-button" data-toggle="tooltip" data-placement="button" title="Copy to Clipboard">
									Copy
								</button>
								</span>
							</div>
						</form>

						<p>Link diatas valid hingga {{$data->expiredDate}} WIB. Batas mengubah data sisa {{10-$data->ctredit}}</p>

						@if (strtotime($data->expiredDate)>time() && ($data->ctredit < 3))
						<div class="row" style="text-align: center;">
							<a href="edit?uuid={{$data->uuid}}">Klik disini untuk mengubah data</a>
						</div>
						@endif
					
					@endisset

=======
		  		@isset($data->uuid)
		    		<p>Edit Data Berhasil, mohon simpan link ini jika ingin melakukan perubahan data</p>
		    		<form>
					<div class="input-group">
						<input type="text" onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
						    value="http://register.asalhapuja.or.id/edit?uuid={{$data->uuid}}" id="copy-input">
						<span class="input-group-btn">
						<button class="btn btn-primary" type="button" id="copy-button" data-toggle="tooltip" data-placement="button" title="Copy to Clipboard">
							Copy
						</button>
						</span>
					</div>
					</form>

					<p>Link diatas valid hingga {{$data->expiredDate}} WIB. Batas mengubah data sisa {{3-$data->ctredit}}</p>

					@if (strtotime($data->expiredDate)>time() && ($data->ctredit < 3))
					<div class="row" style="text-align: center;">
						<a href="edit?uuid={{$data->uuid}}">Klik disini untuk mengubah data</a>
					</div>
					@endif
				@endisset
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b:resources/views/frontend/successEdit.blade.php
		  	</div>
		</div>

	</section>
@endsection

@section('script_body')
	<!-- <script type="text/javascript" src="js/exif.js"></script> -->
@endsection

@section('script_body_main')
	<script type="text/javascript">
		$(function() {
			// Initialize the tooltip.
			$('#copy-button').tooltip();

			// When the copy button is clicked, select the value of the text box, attempt
			// to execute the copy command, and trigger event to update tooltip message
			// to indicate whether the text was successfully copied.
			$('#copy-button').focus(function(){
				var input = document.querySelector('#copy-input');
				input.setSelectionRange(0, input.value.length + 1);
				try {
					var success = document.execCommand('copy');
					if (success) {
						$('#copy-button').trigger('copied', ['Copied!']);
					} else {
						$('#copy-button').trigger('copied', ['Copy with Ctrl-c']);
					}
				} catch (err) {
			  		$('#copy-button').trigger('copied', ['Copy with Ctrl-c']);
				}
			});

			// Handler for updating the tooltip message.
			$('#copy-button').bind('copied', function(event, message) {
				$(this).attr('title', message)
				    .tooltip('fixTitle')
				    .tooltip('show')
				    .attr('title', "Copy to Clipboard")
				    .tooltip('fixTitle');
			});

			var a = $('#copy-input').get(0);
			a.setSelectionRange(0, a.value.length - 1);


		});
	</script>
@endsection
