<<<<<<< HEAD
<?php
    //echo var_dump($data);
    //return 0;
?>
=======
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
@extends('frontend')

@section('title', 'Registration Page')

@section('css')
	<style>
	.row-striped:nth-of-type(odd){
	  background-color: #efefef;
	}

	.row-striped:nth-of-type(even){
	  background-color: #ffffff;
	}
	</style>
@endsection

@section('content-header')
	<section class="content-header">
		<div>
			<h3 align="center">Menu khusus Kepala Rombongan / Rekrut</i>
			</h3>
		</div>
		<ol class="breadcrumb">
          <li><a href="http://asalhapuja.or.id"><i class="fa fa-dashboard"></i>Main Site</a></li>
          <li><a class="active">Group Registration</a></li>
        </ol>
	</section>
@endsection

@section('content')

	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
<<<<<<< HEAD
    			<!-- Modal content-->
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal">&times;</button>
    					<h4 class="modal-title">Modal Header</h4>
    				</div>
    				<div class="modal-body">
    					<p>Some text in the modal.</p>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				</div>
    			</div>
=======

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
			</div>
		</div>
		
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Data Anda</h3>
			</div>
		  	<div class="box-body" style="margin-left: 10px;">
		  		<div class="row">

		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KELOMPOK</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p> {{$data->NAMA}}<p>
		    		</div>

		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KEPALA</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->CP}} / {{$data->HP}}</p>
		    		</div>

		    		@if($data->CP2!=null)
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KEPALA2</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->CP2}} / {{$data->HP2}}</p>
		    		</div>
		    		@endif

		    		@if($data->EMAIL!=null)
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>EMAIL</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->EMAIL}}</p>
		    		</div>
		    		@endif

		    		@if($data->QUOTA > 0)
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>KUOTA</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->QUOTA}}</p>
		    		</div>
		    		@endif

<<<<<<< HEAD
		    		<div class="col-xs-3 col-sm-3 col-lg-2">
		    			<label>FORM</label><span class="pull-right hidden-xs"><strong>:</strong></span> 
		    		</div>
		    		<div class="col-xs-9 col-sm-9 col-sm-10">
		    			<p>{{$data->FORM}}</p>
		    		</div>

		    	</div>
		    	<hr>

		    	<?php
		    		//var_dump(!(strpos($data->FORM,'C') === false));
		    	?>

		    	@if (!(strpos($data->FORM,'A') === false))
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Untuk peserta undangan kategori A tolong diperhatikan agar memenuhi syarat berikut :</strong>
					  	<ul>
					  		<li>Berusia lebih dari 15 Tahun</li>
							<li>Berpakaian putih (atas) hitam (bawah) atau putih-putih selama acara (untuk umat)</i></li>
						</ul>
					</div>
		    	</div>
		    	@endif

		    	@if (!(strpos($data->FORM,'B') === false))
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Untuk peserta kategori B tolong diperhatikan agar memenuhi syarat berikut :</strong>
					  	<ul>
					  		<li>Berusia lebih dari 15 Tahun</li>
							<li>Berpakaian putih (atas) hitam (bawah) atau putih-putih selama acara (untuk umat)</i></li>
							<li>Peserta harus mengerti ketentuan paket</li>
						</ul>
					</div>
		    	</div>
		    	@endif

		    	@if (!(strpos($data->FORM,'C') === false))
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Untuk peserta kategori C tolong diperhatikan agar memenuhi syarat berikut :</strong>
=======
		    	</div>
		    	<hr>
		    	@if($data->FORM == 'D')
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Tolong diperhatikan agar pesertanya memenuhi syarat berikut :</strong>
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
					  	<ul>
					  		<li>Berusia lebih dari 15 Tahun</li>
							<li>Wajib mengikuti Tipitaka Chanting dan Asalha Puja sesuai jadwal acara.</i></li>
							<li>Berpakaian putih (atas) hitam (bawah) atau putih-putih selama acara</i></li>
<<<<<<< HEAD
							<li>Anda sebagai kepala rombongan telah mengatur sendiri transportasi dan penginapan untuk peserta kelompok anda, panitia tidak akan menyiapkan penginapan dan transportasi</li>
						</ul>
					</div>
		    	</div>
		    	@endif

		    	@if (!(strpos($data->FORM,'D') === false))
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Untuk peserta kategori D tolong diperhatikan agar pesertanya memenuhi syarat berikut :</strong>
=======
							<li>Wajib melaksanakan atthasila pada tgl 20-22 Juli 2018.</li>
						</ul>
					</div>
		    	</div>
		    	@elseif($data->FORM == 'C')
		    	<div class="row">
		    		<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">
						<strong>Tolong diperhatikan agar pesertanya memenuhi syarat berikut :</strong>
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
					  	<ul>
					  		<li>Berusia lebih dari 15 Tahun</li>
							<li>Wajib mengikuti Tipitaka Chanting dan Asalha Puja sesuai jadwal acara.</i></li>
							<li>Berpakaian putih (atas) hitam (bawah) atau putih-putih selama acara</i></li>
<<<<<<< HEAD
							<li>Wajib melaksanakan atthasila pada tgl 20-22 Juli 2018.</li>
=======
							<li>Anda sebagai kepala rombongan telah mengatur sendiri transportasi dan penginapan untuk peserta kelompok anda, panitia tidak akan menyiapkan penginapan dan transportasi</li>
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
						</ul>
					</div>
		    	</div>
		    	@endif
<<<<<<< HEAD
		    	

=======
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
		  	</div>
		  <!-- /.box-body -->
		</div>

		<div class="box box-default">
			<div class="box-header with-border" style="margin-right: 10px;">
				<h3 class="box-title">Data Group Anda ({{sizeof($data->PESERTA)}}{{($data->QUOTA > 0)?"/".$data->QUOTA:""}})</h3>
<<<<<<< HEAD
				<span class="pull-right">
					@if ($data->ADD_VALID)
						@if (!(strpos($data->FORM,'A') === false))
						<span>
							<a target="_blank" href="{{ url('/') }}/registerGroup?uuid={{$data->UUID}}&action=add&type=A" class="btn btn-primary" role="button">
							<span class="glyphicon glyphicon-plus"></span> A</a>
						</span>
						@endif

						@if (!(strpos($data->FORM,'B') === false))
						<span>
							<a target="_blank" href="{{ url('/') }}/registerGroup?uuid={{$data->UUID}}&action=add&type=B" class="btn btn-primary" role="button">
							<span class="glyphicon glyphicon-plus"></span> B</a>
						</span>
						@endif

						@if (!(strpos($data->FORM,'C') === false))
						<span>
							<a target="_blank" href="{{ url('/') }}/registerGroup?uuid={{$data->UUID}}&action=add&type=C" class="btn btn-primary" role="button">
							<span class="glyphicon glyphicon-plus"></span> C</a>
						</span>
						@endif

						@if (!(strpos($data->FORM,'D') === false))
						<span>
							<a target="_blank" href="{{ url('/') }}/registerGroup?uuid={{$data->UUID}}&action=add&type=D" class="btn btn-primary" role="button">
							<span class="glyphicon glyphicon-plus"></span> D</a>
						</span>
						@endif
					@endif
				</span>
=======
				@if ((sizeof($data->PESERTA) < $data->QUOTA) && ($data->FORM == 'D') || ($data->FORM <> 'D'))
				<span class="pull-right"><a href="register{{$data->FORM}}?uuid={{$data->UUID}}" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-plus"></span> Add</a></span>
				@endif
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
			</div>
		  	<div class="box-body" style="margin-left: 10px; margin-right: 10px;">

		  		<?php $ct = 1;?>
		  		@foreach ($data->PESERTA as $peserta)
		  		<?php
  					$wa_phone = str_replace(array('-','+','(',')'), '', $peserta->PHONE);
  					if (substr($peserta->PHONE, 0, 1)=="0")
  						$wa_phone = "62".substr($wa_phone,1);
  				?>
		  		<div class="row row-striped">	
		  				<div class="hidden-xs hidden-sm hidden-md col-xs-12  col-lg-1">
		  					<label>{{$ct}}</label>
<<<<<<< HEAD
		  					<span class="pull-right"><span class="glyphicon glyphicon-file" style="color:gray;"></span><strong>{{$peserta->FORM }}</strong></span>
		  				</div>
		  				<div class="col-xs-8 col-lg-3">
		  					<p><span class="glyphicon glyphicon-user" style="margin-right: 5px"></span>{{$peserta->NAMA }}</p>
		  				</div>

		  				<div class="col-xs-2 hidden-lg" style="color:gray;">
		  					<span class="pull-right">
		  						<span class="glyphicon glyphicon-file"></span>
		  						<strong>{{$peserta->FORM }}</strong>
		  					</span>	  					
		  				</div>

		  				<div class="col-xs-2 hidden-lg">
		  						<label class="pull-right ">{{$ct}}</label>
		  				</div>

=======
		  				</div>
		  				<div class="col-xs-10 col-lg-3">
		  					<p><span class="glyphicon glyphicon-user" style="margin-right: 5px"></span>{{$peserta->NAMA }}</p>
		  				</div>
		  				<div class="col-xs-2 hidden-lg">
		  					<label class="pull-right ">{{$ct}}</label>
		  				</div>
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
		  				<div class="col-xs-6 col-lg-2" >
		  					<p><span class="glyphicon glyphicon-phone" style="margin-right: 5px"></span>{{ $peserta->PHONE }}</p>
		  				</div>
		  				<div class="col-xs-6  visible-xs visible-sm visible-md">

		  					<span class="pull-right">
				  				<span><a href="tel:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span></a></span>
				    			<span><a href="sms:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-envelope"></span></a></span>
				    			<span><a target="_blank" href="https://api.whatsapp.com/send?phone={{$wa_phone}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>	
							</span>
		  					
		  				</div>
		  				<div class="col-xs-10 col-lg-4">
		  					<p><span class="glyphicon glyphicon-envelope" style="margin-right: 5px"></span>{{$peserta->EMAIL }}</p>
		  				</div>

		  				<div class="col-xs-2 col-lg-2">
		  					<span class="visible-lg pull-left">
				  				<span><a href="tel:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span></a></span>
				    			<span><a href="sms:{{$peserta->PHONE}}" class="btn btn-warning btn-xs" role="button"><span class="glyphicon glyphicon-envelope"></span></a></span>
				    			<span><a target="_blank" href="https://api.whatsapp.com/send?phone={{$wa_phone}}" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-phone"></span> WA</a></span>	
							</span>
<<<<<<< HEAD
		  					<span class="pull-right"><a target="_blank" href="{{ url('/') }}/registerGroup?uuid={{$data->UUID}}&action=edit&p={{$peserta->BARCODE}}" class="btn btn-primary btn-xs" role="button">
=======
		  					<span class="pull-right"><a href="groupEdit?uuid={{$data->UUID}}&id_peserta={{$peserta->ID_PESERTA}}" class="btn btn-primary btn-xs" role="button">
>>>>>>> 53e9854e9fd822f6acbd6dac11ab0725afecb83b
		  						<span class="glyphicon glyphicon-pencil"></span> Edit</a>
		  					</span>
		  				</div>
					
		    	</div>
		    	<?php $ct++;?>
		    	@endforeach
		  	</div>
		  <!-- /.box-body -->
		</div>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
@endsection

@section('script_body_main')
@endsection
