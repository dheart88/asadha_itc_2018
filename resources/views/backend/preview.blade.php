<!-- <script src="http://localhost/jquery-1.8.3.min.js"></script> -->
<script type="text/javascript" src="jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="jquery-barcode.min.js"></script>

<?php 
	$i = 0;
?>
@foreach($data as $dt)
    <?php 
    	$barcode = 1000000+$dt->ID_PESERTA;
    	$barcode = substr($barcode, 1, 6);
    	//substr($dt->NAMA,0,19)
    ?>
	@if($i % 2 == 0)
	    <br>
	@endif
	<table style="width:125mm;height:95mm;table-layout:fixed;display:inline-block; border-width:1px; background-size: 125mm 95mm;"  cellpadding="0" cellspacing="0" border="0"  background="{{$dt->FORM}}.jpg?{{time()}}"  >
    	<tr>
    	  <th colspan="4" style="width:125mm; height:38mm;"></td>
    	</tr>
    	
    	<tr>
    	  <td rowspan="3" style="width:12mm; height:28mm;"></td>
    	  <td rowspan="3" style="width:27mm; height:35mm;">
    	      <img src="validated/{{$dt->ID_PESERTA}}.jpg?'{{time()}}" width="102" height="136">
    	  </td>
    	  <td colspan="2" style="height:8mm;"></td>
    	</tr>
    	
    	<tr>
    	  <td style="width:5mm; height:10mm;">
    	      &nbsp;
    	  </td>
    	  <!--<td style="height:10mm; font-style: bold; font-family: Montepetrum, Bahnschrift Condensed, Impact; font-size: 41px;">-->
    	  <!--    <div style ="width:80mm;"><b>{{$dt->PRINTED_NAME}}</b></div>-->
    	  <!--</td>-->
    	  <td></td>
    	</tr>
    	
    	<tr>
    	  <td style="width:5mm; height:10mm;">
    	      &nbsp;
    	  </td>	  
    	  <td style="width:80mm; height:10mm;">
    	    <div style="display:inline-block;" id="b_{{$barcode}}"></div>
    		<script type="text/javascript">
    			$("#b_{{$barcode}}").barcode("{{$barcode}}", "code128" ,{ barWidth: 1, barHeight: 25, bgColor:"#FFFFFFAA"});
    		</script>
    		@if ($dt-> MEAL == 0)
    		 <span style="font-family:Interstate Condensed; font-size: 10px; color:#00000088; margin-left:-20px; margin-top:-10px;">*</span>
    		@endif
    	  </td>
    	</tr>
    
    	<tr>
    	  <td style="width:12mm; height:9mm;"></td>
    	  <!--73421a-->
    	   <!--font-style: bold;-->
    	   <!--Bahnschrift Condensed,-->
    	   <!--, Impact-->
    	   <!--Swiss721BT-RomanCondensed-->
    	   <!--Interstate Condensed-->
    	   <!--Interstate Condensed, -->
    	  <td colspan="3" style="width:125mm; height:9mm; font-family:Arial Narrow; font-size: 32px; color:#000000">
    	    <b>{{$dt->PRINTED_NAME}}</b>
    	  </td>
    	</tr>
    	
    	<tr>
    	  <th colspan="4" style="width:125mm; height:11mm;"></td>
    	</tr>
    	
	</table>
	<?php 
	    $i++;
    ?>
@endforeach