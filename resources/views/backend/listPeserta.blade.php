<?php
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
    // echo json_encode($data);
    // return;
?>
@extends('frontend')

@section('title', 'Registration Page')

@section('css')
	<style>
	.row-striped:nth-of-type(odd){
	  background-color: #efefef;
	}

	.row-striped:nth-of-type(even){
	  background-color: #ffffff;
	}
	
	.big-checkbox {width: 30px; height: 30px;}
	</style>
@endsection

@section('content')
    
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
    			<!-- Modal content-->
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal">&times;</button>
    					<h4 class="modal-title">Modal Header</h4>
    				</div>
    				<div class="modal-body">
    					<p>Some text in the modal.</p>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				</div>
    			</div>
			</div>
		</div>
		
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">List Peserta & Absensi Kedatangan</small></h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  	    <form class="form" name="formSearch" method="post" onSubmit="search(); return false" autocomplete="off">
		  	    <div class="form-group row">
		  	        
		  	        <!-- NAMA PESERTA -->
					<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="name_lookup">Pencarian pada nama lengkap ataupun nama tercetak</label>
						<input type="text" name="name_lookup" id="name_lookup" class="form-control" maxlength="25" size="25" placeholder="Masukkan nama yang di cari">
					</div>
					
				    <!-- STATUS CETAK -->
		    <!--		<div class="col-xs-12 col-sm-9 col-lg-6">-->
      <!--					<label for="photo_status">Status Foto Dan Kartu</label>-->
      <!--					<select class="form-control" name="photo_status" id="photo_status">-->
						<!--	<option value="0">Foto Tidak Valid</option>-->
						<!--	<option value="1" >Kartu Belum DiCetak</option>-->
						<!--	<option value="1">Telah dicetak</option>-->
						<!--	<option value="-1" selected>Semua</option>-->
						<!--</select>-->
  				<!--	</div>-->
  					
  					<!-- FORM -->
		    		<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="form">Kategori</label>
      					<select class="form-control" name="form" id="form">
							<option value=""selected>Semuanya</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
						</select>
  					</div>
  					
  					<!-- ORDER BY -->
		    		<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="order_by">Urutkan</label>
      					<select class="form-control" name="order_by" id="order_by">
							<option value="0" selected>Nama Lengkap</option>
							<option value="1">Nama Tercetak</option>
							<option value="2">Kategori</option>
							<option value="3">Grup</option>
							<option value="4">ID</option>
						</select>
  					</div>
  					
  					<!-- Option Photo -->
		    	<!--	<div class="col-xs-12">-->
       <!--                 <label for="show_picture" style="font-weight: normal;">-->
       <!--                     <input id="show_picture" type="checkbox" name="data_option[]" value="photo" checked>Tampilkan gambar</input>-->
       <!--                 </label>-->
  					<!--</div>-->
  					<!--<div class="col-xs-12">-->
       <!--                 <label for="show_meal" style="font-weight: normal;">-->
       <!--                     <input id="show_meal" type="checkbox" name="data_option[]" value="meal" checked>Tampilkan makan</input>-->
       <!--                 </label>-->
  					<!--</div>-->
  					<!--<div class="col-xs-12">-->
       <!--                 <label for="show_contact" style="font-weight: normal;">-->
       <!--                     <input id="show_contact" type="checkbox" name="data_option[]" value="contact" checked>Tampilkan kontak</input>-->
       <!--                 </label>-->
  					<!--</div>-->
  				</div>
  				
  				<div class="row" style="text-align:right;">
					<div class="col-xs-12">
    		  	        <button type="submit" class="btn btn-primary btn-md">
                           <span class="glyphicon glyphicon-search"></span> Search
                        </button>
					</div>
				</div>
				</form>
		  	</div>
		</div>
		
		<!--action="{{ url('/') }}/postPrintList"-->
		<form class="form" name="formPrintList" id="formPrintList" method="post"  enctype="multipart/form-data" autocomplete="off">
         {{ csrf_field() }}
		<div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Result (<span id="result_num"></span>)</h3>
                
                <span class="pull-right">
                <span>
    					<a target="_blank" href="{{ url('/') }}/quickRegister" class="btn btn-primary" role="button">
    					<span class="glyphicon glyphicon-plus"></span> Quick Add</a>
    				</span>
    				<span>
    					<a target="_blank" href="{{ url('/') }}/registerGroup?uuid=ASALHAPU&action=add&type=A" class="btn btn-primary" role="button">
    					<span class="glyphicon glyphicon-plus"></span> A</a>
    				</span>
    				<span>
    					<a target="_blank" href="{{ url('/') }}/registerGroup?uuid=ASALHAPU&action=add&type=B" class="btn btn-primary" role="button">
    					<span class="glyphicon glyphicon-plus"></span> B</a>
    				</span>
    				<span>
    					<a target="_blank" href="{{ url('/') }}/registerGroup?uuid=ASALHAPU&action=add&type=C" class="btn btn-primary" role="button">
    					<span class="glyphicon glyphicon-plus"></span> C</a>
    				</span>
    				<span>
    					<a target="_blank" href="{{ url('/') }}/registerGroup?uuid=ASALHAPU&action=add&type=D" class="btn btn-primary" role="button">
    					<span class="glyphicon glyphicon-plus"></span> D</a>
    				</span>
				</span>
				
            </div>
            <div class="box-body" style="margin-left: 10px; margin-right: 10px;">
                <div id='result'></div>
		  	</div>
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>

	<!-- /.box -->
	</section>
@endsection

@section('script_body')
@endsection

@section('script_body_main')
	<script type="text/javascript">

		// //alert('b_'+kode)
		$(function(){
		    $('.overlay').show();
		    var arr = [];
		    arr.push({name:"_token",value:"{{csrf_token()}}"});
		    arr.push({name:"name_lookup",value:""});
		    arr.push({name:"form",value:""});
		    arr.push({name:"order_by",value:0});
			$.ajax({
			    type:'POST',
				url: "{{ url('/')}}/listPesertaData",
				cache: false,
				dataType: 'html',
				data: arr,
				success: function(msg){
				    $('.overlay').hide();
                    $("#result").html(msg);
                    $("#result_num").html($('#sizeData').val());
				}
			});
		});
		
		function search()
		{
		    $('.overlay').show();
		    var arr = [];
		    
		    arr.push({name:"_token",value:"{{csrf_token()}}"});
		    arr.push({name:"name_lookup",value:$('#name_lookup').val()});
		    arr.push({name:"form",value:$('#form').val()});
		    arr.push({name:"order_by",value:$('#order_by').val()});
		    
			$.ajax({
			    type:'POST',
				url: "{{ url('/')}}/listPesertaData",
				cache: false,
				dataType: 'html',
				data: arr,
				success: function(msg){
				    $('.overlay').hide();
				    $("#result").html(msg);
				    $('#name_lookup').val('');
				    $("#result_num").html($('#sizeData').val());
				}
			});
		}
        
        
	</script>
@endsection
