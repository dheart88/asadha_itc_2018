<?php
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
    // echo json_encode($data);
    // return;
?>
@extends('frontend')

@section('title', 'Registration Page')

@section('css')
	<style>
	.row-striped:nth-of-type(odd){
	  background-color: #efefef;
	}

	.row-striped:nth-of-type(even){
	  background-color: #ffffff;
	}
	
	.big-checkbox {width: 30px; height: 30px;}
	</style>
@endsection

@section('content')
    
	<section class="content">

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
    			<!-- Modal content-->
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal">&times;</button>
    					<h4 class="modal-title">Modal Header</h4>
    				</div>
    				<div class="modal-body">
    					<p>Some text in the modal.</p>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				</div>
    			</div>
			</div>
		</div>
		
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Kondisi Pencarian</small></h3>
			</div>

		  	<div class="box-body" style="margin-left: 10px;">
		  	    <form class="form" name="formSearch" method="post" onSubmit="search(); return false" autocomplete="off">
		  	    <div class="form-group row">
		  	        
		  	        <!-- NAMA PESERTA -->
					<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="name_lookup">Pencarian pada nama lengkap ataupun nama tercetak</label>
						<input type="text" name="name_lookup" id="name_lookup" class="form-control" maxlength="25" size="25" placeholder="Masukkan nama yang di cari">
					</div>
					
				    <!-- STATUS CETAK -->
		    		<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="printed_status">Status Cetak</label>
      					<select class="form-control" name="printed_status" id="printed_status">
							<option value="0" selected>Belum dicetak</option>
							<option value="1">Telah dicetak</option>
							<option value="-1">Semua kartu</option>
						</select>
  					</div>
  					
  					<!-- FORM -->
		    		<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="form">Kategori</label>
      					<select class="form-control" name="form" id="form">
							<option value=""selected>Semuanya</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
						</select>
  					</div>
  					
  					<!-- ORDER BY -->
		    		<div class="col-xs-12 col-sm-9 col-lg-6">
      					<label for="order_by">Urutkan</label>
      					<select class="form-control" name="order_by" id="order_by">
							<option value="0" selected>Nama Lengkap</option>
							<option value="1">Nama Tercetak</option>
							<option value="2">Kategori</option>
							<option value="3">Grup</option>
							<option value="4">ID</option>
						</select>
  					</div>
  					
  				</div>
  				
  				<div class="row" style="text-align:right;">
					<div class="col-xs-12">
    		  	        <button type="submit" class="btn btn-primary btn-md">
                           <span class="glyphicon glyphicon-search"></span> Search
                        </button>
					</div>
				</div>
				</form>
		  	</div>
		</div>
		
		<form class="form" name="formPrintList" id="formPrintList" method="post" action="{{ url('/') }}/postPrintList" enctype="multipart/form-data" autocomplete="off">
         {{ csrf_field() }}
		<div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Result (<span id="result_num"></span>)</h3>
            </div>
            <div class="box-body" style="margin-left: 10px; margin-right: 10px;">
                <div id='result'></div>
		  	</div>
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>

	<!-- /.box -->
	</section>
@endsection

@section('footer')
    <footer class="main-footer navbar-fixed-bottom">
        <div class="row" style="text-align:center;">
        	<div class="hidden-xs visible-sm visible-md visible-lg col-sm-12">
        	    <span class="pull-left">
        	        <button type="button" class="btn btn-sm" onclick="selectAll();">All</button>
        	        <button type="button" class="btn btn-sm" onclick="selectNone();">None</button>
        	        <button class="btn btn-info btn-md" type="submit" formaction="{{ url('/') }}/postPrintSetBelumCetak">Set Blm Cetak</button>
        	        <button class="btn btn-info btn-md" type="submit" formaction="{{ url('/') }}/postPrintSetSudahCetak">Set Tercetak</button>
        	    </span>
        	    
        	    <span class="pull-right">
        	        <!--<button class="btn btn-primary btn-md" onclick="print(); return;">Cetak</button>-->
        	        <input type="submit" formtarget="_blank"  id="id_complete" class="btn btn-primary btn-md" value="Cetak">
        	    </span>
        	</div>
        	
        	<div class="visible-xs hidden-sm hidden-md hidden-lg col-xs-12">
        	    <span class="pull-left">
        	        <button type="button" class="btn btn-xs" onclick="selectAll();">All</button>
        	        <button type="button" class="btn btn-xs" onclick="selectNone();">None</button>
        	        <button class="btn btn-info btn-xs" type="submit" formaction="{{ url('/') }}/postPrintSetBelumCetak">Set Blm Cetak</button>
        	        <button class="btn btn-info btn-xs" type="submit" formaction="{{ url('/') }}/postPrintSetSudahCetak">Set Tercetak</button>
        	    </span>
        	    
        	    <span class="pull-right">
        	        <!--<button class="btn btn-primary btn-md" onclick="print(); return;">Cetak</button>-->
        	        <input type="submit" formtarget="_blank"  id="id_complete" class="btn btn-primary btn-md" value="C E T A K">
        	    </span>
        	</div>
        </div>
	</footer>
	</form>
@endsection


@section('script_body')
@endsection

@section('script_body_main')
	<script type="text/javascript">

		// //alert('b_'+kode)
		$(function(){
		    $('.overlay').show();
		    var arr = [];
		    arr.push({name:"_token",value:"{{csrf_token()}}"});
		    arr.push({name:"name_lookup",value:""});
		    arr.push({name:"printed_status",value:0});
		    arr.push({name:"form",value:""});
		    arr.push({name:"order_by",value:0});
			$.ajax({
			    type:'POST',
				url: "{{ url('/')}}/listPrintData",
				cache: false,
				dataType: 'html',
				data: arr,
				success: function(msg){
				    $('.overlay').hide();
                    $("#result").html(msg);
                    fix_input_box();
                    $("#result_num").html($('#sizeData').val());
				}
			});
		});
		
		function toggle(id)
		{
		  //  var val = $(id).is(':checked');
		  //  $(id).prop('checked', !val);
		    $(id).trigger('click');
		}
		
		function fix_input_box()
		{
		    $("input:checkbox").change( function(){
        	    if($(this).is(':checked')) {
        	        $(this).parent().parent().addClass("bg-yellow");
            	} else {
            		$(this).parent().parent().removeClass("bg-yellow");
            	}
            	
            // 	
            
        	});
		}
		
		function search()
		{
		    $('.overlay').show();
		    var arr = [];
		    
		    arr.push({name:"_token",value:"{{csrf_token()}}"});
		    arr.push({name:"name_lookup",value:$('#name_lookup').val()});
		    arr.push({name:"printed_status",value:$('#printed_status').val()});
		    arr.push({name:"form",value:$('#form').val()});
		    arr.push({name:"order_by",value:$('#order_by').val()});
		    
			$.ajax({
			    type:'POST',
				url: "{{ url('/')}}/listPrintData",
				cache: false,
				dataType: 'html',
				data: arr,
				success: function(msg){
				    $('.overlay').hide();
				    $("#result").html(msg);
				    $('#name_lookup').val('');
				    fix_input_box();
				    $("#result_num").html($('#sizeData').val());
				}
			});
		}
		
		function print()
		{
		  //  alert("sini");
		}
		
		function selectAll()
		{     
          var checkboxes = $('input:checkbox');
          checkboxes.prop('checked', true);
          checkboxes.trigger('change');
        }
        
        function selectNone()
		{     
          var checkboxes = $('input:checkbox');
          checkboxes.prop('checked', false);
          checkboxes.trigger('change');
        }
        
        
	</script>
@endsection
