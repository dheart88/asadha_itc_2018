    <?php $ct = 1;?>
   
    <input type="hidden" id="sizeData" value={{sizeof($data)}} />
    @if (sizeof($data)>0)
      	@foreach ($data as $dt)
      	<?php
		    $hasImage = file_exists(public_path('ImageThumb/').'/'.$dt->ID_PESERTA.'.jpg') 
		?>
      	<div class="row row-striped">
            <div class="col-xs-1 col-sm-1 col-md-1">
                <label>{{$ct}}</label>
                <!--<input type="checkbox" class="big-checkbox check_me" id="cb_{{$dt->ID_PESERTA}}" name="list_id[]" value="{{$dt->ID_PESERTA}}">    -->
            </div>
              <div class="col-xs-3 col-sm-2 col-md-2">
              @if($hasImage) 
                <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{URL::to('/').'/ImageThumb'.'/'.$dt->ID_PESERTA.'.jpg?'.time()}}" style="display: block; max-width: 75px; max-height: 100px; margin: auto;"/>
              @else
                <img id="myImg" class="img-responsive" crossorigin="anonymous" src="{{asset('placeholder.jpg')}}" style="display: block; max-width: 75px; max-height: 100px; margin: auto;"/>
              @endif
              </div>
              <div class="col-xs-8 col-sm-9 col-md-9">
              <!--<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">-->
                <div class="row" >
                      <div class="col-xs-12">
                          <span class="pull-left"><strong>{{$dt->NAMA}}</strong></span>
                          <span class="pull-right">ID:{{$dt->ID_PESERTA}}</span>
                      </div>
                      <div class="col-xs-12"><p><i style="color: gray">{{$dt->PRINTED_NAME}}</i></p></div>
                      <div class="col-xs-12">
                          @if($dt->PHOTO_VALIDATED == 1)
                             <span class="label label-success"><i class="fa fa-check"></i> Siap Cetak</span>
                          @else
                             <span class="label label-warning"><i class="fa fa-close"></i> Tidak Valid</span>
                             
                          @endif
                          <span class="pull-right"><a target="_blank" href="{{ url('/') }}/quickPhoto?uuid={{$dt->BARCODE}}" class="btn btn-primary btn-xs" role="button">
                          <span class="glyphicon glyphicon-camera"></span> Ambil Foto</a>
                        </span>
                          
                          @if($dt->PHOTO_PRINTED == 1)
                             <span class="label label-info"><i class="fa fa-check"></i>Telah tercetak</span>
                          @endif
                       </div>
                        <div class="col-xs-12">
                            <span class="glyphicon glyphicon-file" style="color:gray;"></span>
                            <font style="color: gray">{{$dt->FORM}}</font>
                        </div>
                        <div class="col-xs-12">
                            <span class="glyphicon glyphicon-tag" style="color:gray;"></span>
                            <font style="color: gray">{{$dt->GRUP}}</font>
                        </div>
                        <div class="col-xs-12">
                            <span class="glyphicon glyphicon-tag" style="color:gray;"></span>
                            <font style="color: gray">{{$dt->MEAL == 0? "VEGE":"NON VEGE"}}</font>
                        </div>
                        @if($dt->FORM == 'C')
                        <div class="col-xs-12">
                            <span class="glyphicon glyphicon-tag" style="color:gray;"></span>
                            {{$dt->field_1 == 1?"19M":""}} |
                            {{$dt->field_2 == 1?"20P":""}} |
                            {{$dt->field_3 == 1?"20S":""}} |
                            {{$dt->field_4 == 1?"20M":""}} |
                            {{$dt->field_5 == 1?"21S":""}} |
                            {{$dt->field_6 == 1?"21M":""}} |
                            {{$dt->field_7 == 1?"21S":""}} 
                        </div>
                        @endif
    	        </div>
            </div>
    	<?php $ct++;?>
    	</div>
    	@endforeach
	@else
	    <STRONG>TAK ADA DATA DENGAN PENCARIAN TERSEBUT</STRONG>
	@endif