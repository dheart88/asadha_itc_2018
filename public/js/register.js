$(function(){
  $(".select_placeholder").select2({ width: '100%', placeholder:"Pilih / Please Select" });

  $('.timepicker').timepicker({
    showInputs: false,
    maxHours:24,
    showMeridian:false,
    defaultTime:false,
  });
    
  $('#tgl_lahir').datepicker({
    autoclose: true,
    startDate: new Date("01/01/1900"),
    endDate: new Date("06/12/2007"),
    startView:2,
    format: 'dd-mm-yyyy'
  });

  $('#arrival_date').datepicker({
      startDate: new Date("07/11/2019"),
      endDate: new Date("07/11/2019"),
      format: 'dd-mm-yyyy',
  }).val('11-07-2019').attr('disabled','disabled');

  $('#departure_date').datepicker({
      startDate: new Date("07/15/2019"),
      endDate: new Date("07/15/2019"),
      format: 'dd-mm-yyyy',
  }).val('15-07-2019').attr('disabled','disabled');

  $('textarea[data-limit-rows=true]').on('keypress', function (event) {
    var textarea = $(this),
        text = textarea.val(),
        numberOfLines = (text.match(/\n/g) || []).length + 1,
        maxRows = parseInt(textarea.attr('rows'));

    if (event.which === 13 && numberOfLines === maxRows ) {
      return false;
    }
  });

  
  // Disable scroll when focused on a number input.
  $('form').on('focus', 'input[type=number]', function(e) {
    $(this).on('wheel', function(e) {
        e.preventDefault();
    });
});

  // Restore scroll on number inputs.
  $('form').on('blur', 'input[type=number]', function(e) {
      $(this).off('wheel');
  });

  // Disable up and down keys.
  $('form').on('keydown', 'input[type=number]', function(e) {
      if ( e.which == 38 || e.which == 40 )
          e.preventDefault();
  });  

});

function checkUniqueID(){
  //validate KTP
  var obj = {};
  obj.ktp = $('#ktp').val();
  obj.passport = $('#passport').val();
  obj.uuid = $('#uuid').val();

  return $.ajax({
    url: '/checkUniqueID',
    type: 'POST',
    headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
    contentType: 'application/json',
    data: JSON.stringify(obj)
  });
}

function parseDMY(value) {
  var dt = value.split("-");
  var d = parseInt(dt[0], 10),
      m = parseInt(dt[1], 10),
      y = parseInt(dt[2], 10);
  return new Date(y, m - 1, d);
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function modalShow(titleHeader,message)
{
  $('#myModal').modal('show');
  $(".modal-title").text(titleHeader);
  $(".modal-body").text(message); 
}

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#myImg')
              .attr('src', e.target.result);
              // .width(150)
              // .height(200);
      };

      reader.readAsDataURL(input.files[0]);
  }
}

function previewReceipt(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#receiptImage').attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
  }
}

$('.accepted').click(function () {
  if ($('.accepted:checked').length == $('.accepted').length)
    $('#id_complete').removeAttr('disabled');
  else
    $('#id_complete').attr('disabled','disabled');
});

// $(".datepicker").datepicker().on('changeDate', function(ev) {
//   $(this).valid();  // triggers the validation test
//   // '$(this)' refers to '$("#datepicker")'
// });